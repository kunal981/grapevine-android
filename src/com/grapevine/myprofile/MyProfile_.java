package com.grapevine.myprofile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cuelogic.grapevine.LocationChangeHandler;
import com.cuelogic.grapevine.LoginRegister_;
import com.cuelogic.grapevine.R;
import com.cuelogic.grapevine.util.AppConstant.Config;
import com.grapevine.interest.InterestAdapterHeader_;
import com.grapevine.interest.InterestAdapter_;
import com.grapevine.recommended.Recommended_;
import com.grapevine.ui.MySpinnerAdapter;
import com.grapevine.ui.SegmentedGroup;
import com.grapevine.ui.SponserImageView;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class MyProfile_ extends Activity implements OnClickListener,
		RadioGroup.OnCheckedChangeListener {

	private LinearLayout overviewLinear, interestLinear, locationLinear;
	private EditText myEmail, myFirstName, myLastName, myAge;
	// private TextView registeredEventCount, likedEventCount,
	// bookmarkedEventCount;
	private Spinner gender;
	private ArrayList<String> genderArray;
	private Button updateDetails, updateLocation, updateInterests;
	// private ProgressDialog pd;
	// private ImageView fbProfileImageView;
	// For Interest
	ListView interestsCommunitiesLv;
	ArrayList<HashMap<String, String>> interestList, communitiesList;
	InterestAdapter_ interestAdapter, communitiesAdapter;
	private LayoutInflater mInflater;
	SocketConnection sc;
	BaseUrl bu;
	RadioButton radioButton1, radioButton2, radioButton3;
	SegmentedGroup segmented4;

	// For Location
	private Spinner communitySpinner, neighborhoodSpinner;
	private HashMap<String, String> hashmapAllCommunity = new HashMap<String, String>();
	private HashMap<String, String> hashmapAllNeighborhood;
	private ArrayList<String> communities;
	private ArrayList<String> neighbourhoods;
	private int Position, CommunityIdtoMatch;
	private String toUpdateCommunityId;
	MySpinnerAdapter spinnerArrayAdapter;
	SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_profile_);

		preferences = getSharedPreferences("GrapevineData",
				Context.MODE_PRIVATE);
		BaseUrl.TAB4_STACK.add(this);
		((Button) findViewById(R.id.btLogout)).setOnClickListener(this);
		((Button) findViewById(R.id.btBack)).setOnClickListener(this);
		((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
				.ConfigureTabBarPane(false, false, false, true);
		segmented4 = (SegmentedGroup) findViewById(R.id.segmented4);
		segmented4.setTintColor(getResources().getColor(
				R.color.radio_button_selected_color));
		segmented4.setOnCheckedChangeListener(this);
		radioButton1 = (RadioButton) findViewById(R.id.button41);
		// radioButton1.setChecked(true);
		radioButton2 = (RadioButton) findViewById(R.id.button42);
		radioButton3 = (RadioButton) findViewById(R.id.button43);
		((SponserImageView) findViewById(R.id.id_img_sponser_1))
				.setSponserImage(Integer.valueOf(BaseUrl.CommunityID));

		sc = new SocketConnection(this);
		bu = new BaseUrl();
		InitUI();
		SetOverviewData();
		new AsynchInterest().execute();
		LocationChangeHandler.setInstance(new LocationChangeHandler(
				getApplicationContext()));
		LocationChangeHandler.getInstance().beginUpdate();

		String userkey = preferences.getString("facebookID", "");
		// if (userkey.length() > 0) {
		// AsynFBPhoto asynfb = new AsynFBPhoto(userkey);
		// asynfb.execute();
		// }
	}

	public void InitUI() {
		overviewLinear = (LinearLayout) findViewById(R.id.overviewLinear);
		interestLinear = (LinearLayout) findViewById(R.id.interestLinear);
		locationLinear = (LinearLayout) findViewById(R.id.locationLinear);

		interestLinear.setVisibility(View.GONE);
		locationLinear.setVisibility(View.GONE);
		myEmail = (EditText) findViewById(R.id.myEmailEdt);
		myFirstName = (EditText) findViewById(R.id.myFirstNameEdt);
		myLastName = (EditText) findViewById(R.id.myLastNameEdt);
		myAge = (EditText) findViewById(R.id.myAgeEdt);
		genderArray = new ArrayList<String>();
		genderArray.add("N/A");
		genderArray.add("Male");
		genderArray.add("Female");
		gender = (Spinner) findViewById(R.id.GenderSpinner);

		MySpinnerAdapter spinnerArrayAdapter = new MySpinnerAdapter(
				MyProfile_.this, R.layout.spinner_layout, genderArray,
				"PtSansWebRegular.TTF");
		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The
																							// drop
																							// down
																							// vieww
		gender.setAdapter(spinnerArrayAdapter);
		updateDetails = (Button) findViewById(R.id.UpdateBtn);
		updateDetails.setOnClickListener(this);

		interestsCommunitiesLv = (ListView) findViewById(R.id.InterestCommunitiesListview);
		mInflater = LayoutInflater.from(MyProfile_.this);
		View headerView = mInflater.inflate(R.layout.layout_text_list_header,
				null);
		interestsCommunitiesLv.addHeaderView(headerView);
		interestList = new ArrayList<HashMap<String, String>>();
		communitiesList = new ArrayList<HashMap<String, String>>();
		interestAdapter = new InterestAdapter_(this, interestList, "INTERESTS");
		communitiesAdapter = new InterestAdapter_(this, communitiesList,
				"COMMUNITIES");
		updateLocation = (Button) findViewById(R.id.updateLocationBtn);
		updateInterests = (Button) findViewById(R.id.UpdateInterest);

		communitySpinner = (Spinner) findViewById(R.id.communitySpinner);
		neighborhoodSpinner = (Spinner) findViewById(R.id.neighborhoodSpinner);
		updateLocation.setOnClickListener(this);
		updateInterests.setOnClickListener(this);
		communities = new ArrayList<String>();
		for (int i = 0; i < bu.parentCommunityArray.length(); i++) {
			try {
				JSONObject jobjCommunity = bu.parentCommunityArray
						.getJSONObject(i);
				hashmapAllCommunity.put(jobjCommunity.getString("id"),
						jobjCommunity.getString("value"));
				communities.add(jobjCommunity.getString("value"));
				if (jobjCommunity.getString("id").equalsIgnoreCase(
						bu.CommunityID))
					Position = i;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// ArrayAdapter<String> communityspinnerArrayAdapter = new
		// ArrayAdapter<String>(
		// this, android.R.layout.simple_spinner_item, communities);
		MySpinnerAdapter communityspinnerArrayAdapter = new MySpinnerAdapter(
				this, R.layout.spinner_layout, communities,
				"PtSansWebRegular.TTF");
		communityspinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The
		communitySpinner.setAdapter(communityspinnerArrayAdapter);
		communitySpinner.setSelection(Position);
		hashmapAllNeighborhood = new HashMap<String, String>();
		neighbourhoods = new ArrayList<String>();
		String key_you_look_for = null;
		Iterator<Map.Entry<String, String>> iter = hashmapAllCommunity
				.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, String> entry = iter.next();
			if (entry.getValue().equalsIgnoreCase(
					communitySpinner.getSelectedItem().toString())) {
				key_you_look_for = entry.getKey();
				toUpdateCommunityId = entry.getKey();
			}
		}
		for (int i = 0; i < bu.neighborhoodCommunityAllArray.length(); i++) {
			try {
				JSONObject jobjNeighborhood = bu.neighborhoodCommunityAllArray
						.getJSONObject(i);
				if (jobjNeighborhood.getString("parent_community")
						.equalsIgnoreCase(key_you_look_for)) {
					hashmapAllNeighborhood.put(
							jobjNeighborhood.getString("id"),
							jobjNeighborhood.getString("value"));
					neighbourhoods.add(jobjNeighborhood.getString("value"));

					MySpinnerAdapter neighborhoodspinnerArrayAdapter = new MySpinnerAdapter(
							this, R.layout.spinner_layout, neighbourhoods,
							"PtSansWebRegular.TTF");
					neighborhoodspinnerArrayAdapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The
																										// drop
																										// down
																										// vieww
					neighborhoodSpinner
							.setAdapter(neighborhoodspinnerArrayAdapter);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		communitySpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						hashmapAllNeighborhood = new HashMap<String, String>();
						neighbourhoods = new ArrayList<String>();
						String key_you_look_for = null;
						Iterator<Map.Entry<String, String>> iter = hashmapAllCommunity
								.entrySet().iterator();
						while (iter.hasNext()) {
							Map.Entry<String, String> entry = iter.next();
							if (entry.getValue().equalsIgnoreCase(
									communitySpinner.getSelectedItem()
											.toString())) {
								key_you_look_for = entry.getKey();
								toUpdateCommunityId = entry.getKey();
							}
						}
						for (int i = 0; i < bu.neighborhoodCommunityAllArray
								.length(); i++) {
							try {
								JSONObject jobjNeighborhood = bu.neighborhoodCommunityAllArray
										.getJSONObject(i);
								if (jobjNeighborhood.getString(
										"parent_community").equalsIgnoreCase(
										key_you_look_for)) {
									hashmapAllNeighborhood.put(
											jobjNeighborhood.getString("id"),
											jobjNeighborhood.getString("value"));
									neighbourhoods.add(jobjNeighborhood
											.getString("value"));
									MySpinnerAdapter neighborhoodspinnerArrayAdapter = new MySpinnerAdapter(
											MyProfile_.this,
											R.layout.spinner_layout,
											neighbourhoods,
											"PtSansWebRegular.TTF");
									neighborhoodspinnerArrayAdapter
											.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The
																														// drop
																														// down
																														// vieww
									neighborhoodSpinner
											.setAdapter(neighborhoodspinnerArrayAdapter);
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.UpdateBtn:
			new AsynchUpdateProfile(true, "").execute();
			break;

		case R.id.updateLocationBtn:
			Iterator<Map.Entry<String, String>> iter = hashmapAllNeighborhood
					.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<String, String> entry = iter.next();
				if (entry.getValue().equalsIgnoreCase(
						neighborhoodSpinner.getSelectedItem().toString())) {
					String key_you_look_for = entry.getKey();
					new AsynchUpdateProfile(false, key_you_look_for).execute();
					break;
				}
			}

			break;
		case R.id.UpdateInterest:
			new AsynchUpdateUser().execute();
			break;
		case R.id.btBack:
			onBackPressed();
			break;

		case R.id.btLogout:
			clearALLData();
			Intent loginAgain = new Intent(MyProfile_.this,
					com.cuelogic.grapevine.LoginRegister_.class);
			startActivity(loginAgain);
			finish();
			overridePendingTransition(R.anim.rotate_out, R.anim.rotate_in);
			break;

		default:
			break;
		}
	}

	public Void SetOverviewData() {
		try {
			JSONObject userInfo = BaseUrl.UserInfo;
			myEmail.setText(userInfo.getString("email"));
			myFirstName.setText(userInfo.getString("first_name"));
			myLastName.setText(userInfo.getString("last_name"));
			myAge.setText(userInfo.getString("age"));
			if (userInfo.getString("gender").equalsIgnoreCase("male"))
				gender.setSelection(1);
			else if (userInfo.getString("gender").equalsIgnoreCase("female"))
				gender.setSelection(2);
			else
				gender.setSelection(0);
			// if (BaseUrl.registeredEventList.size() > 0) {
			// eventName.setText(BaseUrl.registeredEventList.get(0).get(
			// "title"));
			// String DateTime = getDate(BaseUrl.registeredEventList.get(0)
			// .get("start_time"));
			// eventDate.setText(DateTime);
			// eventAddress.setText(BaseUrl.registeredEventList.get(0).get(
			// "city")
			// + ", "
			// + BaseUrl.registeredEventList.get(0).get("state")
			// + " "
			// + BaseUrl.registeredEventList.get(0).get("postalcode"));
			//
			// registeredEventCount.setText(BaseUrl.registeredEventList.size()
			// + "");
			// } else {
			// eventName.setText("");
			// eventDate.setText("");
			// eventAddress.setText("");
			// registeredEventCount.setText("0");
			// }
			// bookmarkedEventCount.setText(BaseUrl.bookmarkedEventList.size()
			// + "");
			// likedEventCount.setText(BaseUrl.likedEventList.size() + "");
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return null;

	}

	private String getDate(String timeStampStr) {

		try {
			SimpleDateFormat writeFormat = new SimpleDateFormat(
					"MMM dd, yyyy hh:mm aaa");
			Date netDate = (new Date(Long.parseLong(timeStampStr) * 1000));
			return writeFormat.format(netDate);
		} catch (Exception ex) {
			return null;
		}
	}

	public class AsynchUpdateProfile extends AsyncTask<Void, Void, String> {
		SocketConnection sc;

		boolean updateProfile = false;
		String s, neighborhoodId, lat, lon;

		public AsynchUpdateProfile(boolean flag, String neighborhoodId) {
			this.updateProfile = flag;
			this.neighborhoodId = neighborhoodId;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			sc = new SocketConnection(MyProfile_.this);

			// pd = ProgressDialog.show(MyProfile_.this, "", "Please Wait..");
			DesignUtil.showProgressDialog(MyProfile_.this);
		}

		@Override
		protected String doInBackground(Void... params) {

			JSONObject profileUpdateJson = new JSONObject();

			try {
				if (updateProfile) {
					profileUpdateJson.put("first_name", myFirstName.getText()
							.toString().trim());
					profileUpdateJson.put("last_name", myLastName.getText()
							.toString().trim());
					if (gender.getSelectedItem().toString()
							.equalsIgnoreCase("male"))
						profileUpdateJson.put("gender", "male");
					else if (gender.getSelectedItem().toString()
							.equalsIgnoreCase("female"))
						profileUpdateJson.put("gender", "female");
					profileUpdateJson.put("age", myAge.getText().toString()
							.trim());
					profileUpdateJson.put("email", myEmail.getText().toString()
							.trim());
					s = sc.SendPUTdata(
							bu.Development_Uri
									+ bu.getMethodName("RECOMMENDED")
									+ bu.USER_KEY + bu.Api_key,
							profileUpdateJson);
				} else {
					if (LocationChangeHandler.getLocation() != null) {
						lat = LocationChangeHandler.getLocation().getLatitude()
								+ "";
						lon = LocationChangeHandler.getLocation()
								.getLongitude() + "";
					} else {
						lat = 35.74184050952353 + "";
						lon = -118.8128372137487 + "";
					}
					profileUpdateJson.put("lon", lon);
					profileUpdateJson.put("lat", lat);
					profileUpdateJson.put("neighborhood_id", neighborhoodId);
					s = sc.SendPUTdata(
							bu.Development_Uri
									+ bu.getMethodName("RECOMMENDED")
									+ bu.USER_KEY + bu.Api_key,
							profileUpdateJson);
				}

				return s;
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			// pd.dismiss();
			DesignUtil.hideProgressDialog();
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(overviewLinear.getWindowToken(), 0);
			System.out.println("Response is " + result);
			if (result != null) {
				Toast.makeText(MyProfile_.this, "Updated successfully",
						Toast.LENGTH_SHORT).show();
				bu.CommunityID = toUpdateCommunityId;
				int sponserId = Integer.valueOf(bu.CommunityID);
				preferences.edit()
						.putInt(Config.KEY_PREF_SPONSER_ID, sponserId).commit();
				((SponserImageView) findViewById(R.id.id_img_sponser_1))
						.setSponserImage(Integer.valueOf(BaseUrl.CommunityID));

			} else {
				Toast.makeText(MyProfile_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}

		}

		private String getTimeStamp() {
			Long tsLong = System.currentTimeMillis() / 1000;
			return tsLong.toString();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		LocationChangeHandler.getInstance().stopUpdates();
		if (BaseUrl.TAB1_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB1_STACK.size(); i++) {
				Activity act = BaseUrl.TAB1_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB2_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB2_STACK.size(); i++) {
				Activity act = BaseUrl.TAB2_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB3_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB3_STACK.size(); i++) {
				Activity act = BaseUrl.TAB3_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB4_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB4_STACK.size(); i++) {
				Activity act = BaseUrl.TAB4_STACK.get(i);
				act.finish();
			}
		}
		BaseUrl.TAB4_LAST_ACTIVITY = this.getClass();
		Intent i = new Intent(this, Recommended_.class);
		startActivity(i);
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		finish();
	}

	/*
	 * A thread which runs in background to parse all the Interest and
	 * Communities.
	 */
	public class AsynchInterest extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// pd = ProgressDialog.show(MyProfile.this,"","Please Wait..");
			DesignUtil.showProgressDialog(MyProfile_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				JSONArray arrayOfInterests = bu.AllInterestArray;
				for (int i = 0; i < arrayOfInterests.length(); i++) {
					JSONObject obj = arrayOfInterests.getJSONObject(i);
					HashMap<String, String> hm = new HashMap<String, String>();
					hm.put("ID", obj.getString("id"));
					hm.put("Value", obj.getString("value"));
					interestList.add(hm);
				}
				JSONArray arrayOfCommunities = bu.AllCommunitiesArray;
				for (int i = 0; i < arrayOfCommunities.length(); i++) {
					JSONObject obj = arrayOfCommunities.getJSONObject(i);
					HashMap<String, String> hm = new HashMap<String, String>();
					hm.put("ID", obj.getString("id"));
					hm.put("Value", obj.getString("value"));
					communitiesList.add(hm);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd.dismiss();
			DesignUtil.hideProgressDialog();
			InterestAdapterHeader_ adapter = new InterestAdapterHeader_(
					MyProfile_.this, "INTERESTS", "COMMUNITIES");
			adapter.addSection("COMMUNITIES", communitiesAdapter);
			adapter.addSection("INTERESTS", interestAdapter);
			interestsCommunitiesLv.setAdapter(adapter);
		}

	}

	@Override
	protected void onStop() {
		super.onStop();
		// new AsynchUpdateUser().execute();
	}

	public class AsynchUpdateUser extends AsyncTask<Void, Void, String> {
		String s;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			DesignUtil.showProgressDialog(MyProfile_.this);
		}

		@Override
		protected String doInBackground(Void... params) {

			try {
				JSONObject updateInterestJson = new JSONObject();
				JSONArray arrayInterest = new JSONArray();
				for (int i = 0; i < BaseUrl.checkedINTEREST.size(); i++) {
					arrayInterest.put(BaseUrl.checkedINTEREST.get(i));
				}
				updateInterestJson.put("interests", arrayInterest);
				JSONArray arrayCommunity = new JSONArray();
				for (int i = 0; i < BaseUrl.checkedCOMMUNITIES.size(); i++) {
					arrayCommunity.put(BaseUrl.checkedCOMMUNITIES.get(i));
				}
				updateInterestJson.put("communities", arrayCommunity);
				System.out.println("" + updateInterestJson);
				s = sc.SendPUTdata(
						bu.Development_Uri
								+ bu.getMethodName("UPDATE_INTEREST_COMMUNITIES")
								+ bu.USER_KEY, updateInterestJson);
			} catch (JSONException e) {
			}
			return s;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			DesignUtil.hideProgressDialog();
			if (result != null) {
				Toast.makeText(MyProfile_.this, "Updated Succesfully",
						Toast.LENGTH_SHORT).show();

			} else {
				Toast.makeText(MyProfile_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void PutRegisteredEventinHashmap(
			ArrayList<HashMap<String, String>> arrayToAdd, JSONArray jsonarray) {

		for (int i = 0; i < jsonarray.length(); i++) {
			try {
				JSONObject arrayElement = jsonarray.getJSONObject(i);
				if (Long.parseLong(arrayElement.getString("start_time")) <= System
						.currentTimeMillis() / 1000) {
					HashMap<String, String> hash = new HashMap<String, String>();
					hash.put("address_line_1",
							arrayElement.getString("address_line_1"));
					hash.put("city", arrayElement.getString("city"));
					hash.put("description",
							arrayElement.getString("description"));
					hash.put("distance", arrayElement.getString("distance"));
					hash.put("end_time", arrayElement.getString("end_time"));
					hash.put("start_time", arrayElement.getString("start_time"));
					hash.put("event_url", arrayElement.getString("event_url"));
					hash.put("public_url", arrayElement.getString("public_url"));
					hash.put("id", arrayElement.getString("id"));
					hash.put("image_url", arrayElement.getString("image_url"));
					hash.put("latitude", arrayElement.getString("latitude"));
					hash.put("location", arrayElement.getString("location"));
					hash.put("longitude", arrayElement.getString("longitude"));
					hash.put("postalcode", arrayElement.getString("postalcode"));
					hash.put("registration_deadline",
							arrayElement.getString("registration_deadline"));
					hash.put("registration_url",
							arrayElement.getString("registration_url"));
					hash.put("state", arrayElement.getString("state"));
					hash.put("teaser", arrayElement.getString("teaser"));
					hash.put("title", arrayElement.getString("title"));
					hash.put("user_feedback",
							arrayElement.getString("user_feedback"));
					hash.put("user_like", arrayElement.getString("user_like"));
					hash.put("user_not_like",
							arrayElement.getString("user_not_like"));
					hash.put("user_registered",
							arrayElement.getString("user_registered"));
					hash.put("user_score", arrayElement.getString("user_score"));
					JSONObject orgnazationObj = arrayElement
							.getJSONObject("organization");
					hash.put("orgnization", orgnazationObj.getString("name"));
					hash.put("orgnizationid", orgnazationObj.getString("id"));
					arrayToAdd.add(hash);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	public void clearALLData() {
		if (BaseUrl.TAB1_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB1_STACK.size(); i++) {
				Activity act = BaseUrl.TAB1_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB2_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB2_STACK.size(); i++) {
				Activity act = BaseUrl.TAB2_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB3_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB3_STACK.size(); i++) {
				Activity act = BaseUrl.TAB3_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB4_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB4_STACK.size(); i++) {
				Activity act = BaseUrl.TAB4_STACK.get(i);
				act.finish();
			}
		}
		BaseUrl.TAB1_LAST_ACTIVITY = com.grapevine.recommended.Recommended_.class;
		BaseUrl.TAB2_LAST_ACTIVITY = com.grapevine.search.SearchScreen_.class;
		BaseUrl.TAB3_LAST_ACTIVITY = com.grapevine.myevent.MyEvents_.class;
		BaseUrl.TAB4_LAST_ACTIVITY = com.grapevine.myprofile.MyProfile_.class;
		BaseUrl.USER_KEY = "";
		BaseUrl.UserInfo = null;
		BaseUrl.bookmarkedEventId.clear();
		BaseUrl.registeredEventId.clear();
		BaseUrl.bookmarkedEventList.clear();
		BaseUrl.registeredEventList.clear();
		BaseUrl.likedEventList.clear();
		BaseUrl.TAB1_STACK.clear();
		BaseUrl.TAB2_STACK.clear();
		BaseUrl.TAB3_STACK.clear();
		BaseUrl.TAB4_STACK.clear();
		Editor editor = getSharedPreferences(bu.KEY,
				Context.MODE_WORLD_WRITEABLE).edit();
		editor.putString("USER_KEY", "");
		editor.commit();

	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		switch (checkedId) {
		case R.id.button41:
			overviewLinear.setVisibility(View.VISIBLE);
			interestLinear.setVisibility(View.GONE);
			locationLinear.setVisibility(View.GONE);

			return;
		case R.id.button42:
			overviewLinear.setVisibility(View.GONE);
			interestLinear.setVisibility(View.VISIBLE);
			locationLinear.setVisibility(View.GONE);

			return;
		case R.id.button43:
			overviewLinear.setVisibility(View.GONE);
			interestLinear.setVisibility(View.GONE);
			locationLinear.setVisibility(View.VISIBLE);

			return;

		}

	}
}

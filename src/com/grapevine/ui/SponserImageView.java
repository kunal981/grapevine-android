package com.grapevine.ui;

import com.cuelogic.grapevine.R;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class SponserImageView extends ImageView {

	public SponserImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public SponserImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SponserImageView(Context context) {
		super(context);
	}

	public void setSponserImage(int id) {

		int imageResourceId = getSponserImage(id);
		setImageResource(imageResourceId);
		setVisibility(View.VISIBLE);

	}

	int arraryResourceid[] = { R.drawable.sponser_1, R.drawable.no_sponser_img,
			R.drawable.sponser_3, R.drawable.sponser_4, R.drawable.sponser_5 };

	public int getSponserImage(int key) {

		int id = 0;
		switch (key) {
		case 34:
			Log.d("Sponser", "Greater Atlanta");
			id = arraryResourceid[0];

			break;
		case 33:
			Log.d("Sponser", "Greater Los Angeles");
			id = arraryResourceid[1];

			break;

		case 32:
			Log.d("Sponser", "Greater Portland");
			id = arraryResourceid[2];

			break;
		case 25:
			Log.d("Sponser", "Greater Rhode Island");
			id = arraryResourceid[3];

			break;
		case 27:
			Log.d("Sponser", "New York");
			id = arraryResourceid[4];

			break;

		default:
			id = arraryResourceid[0];
			break;
		}

		return id;

	}

}

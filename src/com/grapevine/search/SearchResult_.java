package com.grapevine.search;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.cuelogic.grapevine.LocationChangeHandler;
import com.cuelogic.grapevine.R;
import com.grapevine.ui.SponserImageView;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class SearchResult_ extends Activity implements OnClickListener {

	private EditText searchedText;
	private ListView searchedEvents;
	// LinearLayout footerRel;
	LinearLayout noItemFoundLinear;
	private ProgressDialog pd;
	SocketConnection sc;
	BaseUrl bu;
	ArrayList<HashMap<String, String>> searchedEventList;
	private LayoutInflater mInflater;
	static SearchResultAdapter_ searchResultAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_result_);
		Intent intentDetails = getIntent();
		((com.grapevine.views.Header_) findViewById(R.id.header))
				.ConfigureHeaderPane(false, true);
		((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
				.ConfigureTabBarPane(false, true, false, false);
		mInflater = LayoutInflater.from(SearchResult_.this);
		View footerView = mInflater.inflate(R.layout.footer_, null);
		SponserImageView imageview = (SponserImageView) footerView
				.findViewById(R.id.id_img_sponser_1);
		imageview.setSponserImage(Integer.valueOf(BaseUrl.CommunityID));
		((TextView) findViewById(R.id.txt_headertitle))
				.setText("SEARCH RESULTS");

		noItemFoundLinear = (LinearLayout) findViewById(R.id.NoSearchFoundLayout);
		// footerRel = (LinearLayout)findViewById(R.id.footerLinear);
		// footerRel.setVisibility(View.GONE);
		noItemFoundLinear.setVisibility(View.GONE);
		searchedText = (EditText) findViewById(R.id.searchAllOpportitiesEditText);
		((Button) findViewById(R.id.btBack))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						onBackPressed();
					}
				});
		if (intentDetails.hasExtra("filterTerm")) {
			searchedText.setText(intentDetails.getStringExtra("filterTerm"));
		}
		searchedEvents = (ListView) findViewById(R.id.searchListview);
		searchedEvents.addFooterView(footerView);
		searchedEventList = new ArrayList<HashMap<String, String>>();
		searchResultAdapter = new SearchResultAdapter_(this, searchedEventList);
		sc = new SocketConnection(this);
		bu = new BaseUrl();
		turnGPSOn();
		LocationChangeHandler.setInstance(new LocationChangeHandler(
				getApplicationContext()));
		LocationChangeHandler.getInstance().beginUpdate();
		new AsynchSearchResultList().execute();
		// findViewById(R.id.btBack).setOnClickListener(this);
		searchedText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
				if ((arg2 != null && (arg2.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (arg1 == EditorInfo.IME_ACTION_DONE)) {
					new AsynchSearchResultList().execute();
				}
				return false;
			}
		});
		findViewById(R.id.tabSearchLinearLayout).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent startSearchScreen = new Intent(
								SearchResult_.this, SearchScreen_.class);
						startActivity(startSearchScreen);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);
						finish();
					}
				});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btBack:
			Intent i = new Intent(this, SearchScreen_.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();
			break;
		}

	}

	/*
	 * A function used to turn-on device GPS if it is off.
	 */
	public void turnGPSOn() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", true);
		this.sendBroadcast(intent);
		String provider = Settings.Secure.getString(this.getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) { // if gps is disabled

			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			this.sendBroadcast(poke);

		}
	}

	/*
	 * A function used to turn-off device GPS.
	 */
	public void turnGPSOff() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", false);
		this.sendBroadcast(intent);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		LocationChangeHandler.getInstance().stopUpdates();
		turnGPSOff();
	}

	// A service is called to get Events for Search Result screen.
	public class AsynchSearchResultList extends AsyncTask<Void, Void, String> {
		String s, lat, lon, Json_to_parse;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = ProgressDialog.show(SearchResult_.this, "", "Please Wait..");
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				if (LocationChangeHandler.getLocation() != null) {
					lat = LocationChangeHandler.getLocation().getLatitude()
							+ "";
					lon = LocationChangeHandler.getLocation().getLongitude()
							+ "";
				} else {
					lat = 35.74184050952353 + "";
					lon = -118.8128372137487 + "";
				}
				InputStream ip = sc.getHttpStream(BaseUrl.Development_Uri
						+ bu.getMethodName("RECOMMENDED") + bu.USER_KEY
						+ bu.getMethodName("RECOMMENDED_EVENTS") + bu.Api_key
						+ "&type=all&lat=" + lat + "&filter_term="
						+ searchedText.getText().toString().trim() + "&lon="
						+ lon + "&filter_community_id=" + bu.CommunityID);
				Json_to_parse = sc.convertStreamToString(ip);
				parseJson(Json_to_parse);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// return Json_to_parse;
			return Json_to_parse;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
			LocationChangeHandler.getInstance().stopUpdates();
			turnGPSOff();
			if (result != null) {
				// searchedEvents.postInvalidate();
				// searchResultAdapter.notifyAll();
				// recommendations.notifyAll();

				if (searchedEventList.size() == 0) {
					// footerRel.setVisibility(View.VISIBLE);
					noItemFoundLinear.setVisibility(View.VISIBLE);
					searchedEvents.setVisibility(View.GONE);
				} else {
					// footerRel.setVisibility(View.GONE);
					noItemFoundLinear.setVisibility(View.GONE);
					searchedEvents.setVisibility(View.VISIBLE);
					searchedEvents.setAdapter(searchResultAdapter);
					searchedEvents.postInvalidate();
					searchResultAdapter.notifyDataSetChanged();
				}
			} else {
				Toast.makeText(SearchResult_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	public void parseJson(String jsonString) {
		try {
			searchedEventList.clear();
			JSONObject recommendedJsonObject = new JSONObject(jsonString);
			// jsonFeatured = (JSONObject) jsonObject.get("sponsored");
			// JSONObject jsonRecommended = (JSONObject)
			// jsonObject.get("recommended");
			JSONArray arrayallEvents = recommendedJsonObject
					.getJSONArray("all");

			if (arrayallEvents.length() > 0) {
				// searchedEventList.clear();
				PutEventinHashmap(searchedEventList, arrayallEvents);
			}

		} catch (JSONException e) {
		} catch (Exception e) {
		}
	}

	public void PutEventinHashmap(
			ArrayList<HashMap<String, String>> arrayToAdd, JSONArray jsonarray) {

		for (int i = 0; i < jsonarray.length(); i++) {
			try {
				JSONObject arrayElement = jsonarray.getJSONObject(i);
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("address_line_1",
						arrayElement.getString("address_line_1"));
				hash.put("city", arrayElement.getString("city"));
				hash.put("description", arrayElement.getString("description"));
				hash.put("distance", arrayElement.getString("distance"));
				hash.put("end_time", arrayElement.getString("end_time"));
				hash.put("start_time", arrayElement.getString("start_time"));
				hash.put("event_url", arrayElement.getString("event_url"));
				hash.put("public_url", arrayElement.getString("public_url"));
				hash.put("id", arrayElement.getString("id"));
				hash.put("image_url", arrayElement.getString("image_url"));
				hash.put("latitude", arrayElement.getString("latitude"));
				hash.put("location", arrayElement.getString("location"));
				hash.put("longitude", arrayElement.getString("longitude"));
				hash.put("postalcode", arrayElement.getString("postalcode"));
				hash.put("registration_deadline",
						arrayElement.getString("registration_deadline"));
				hash.put("registration_url",
						arrayElement.getString("registration_url"));
				hash.put("state", arrayElement.getString("state"));
				hash.put("teaser", arrayElement.getString("teaser"));
				hash.put("title", arrayElement.getString("title"));
				hash.put("user_feedback",
						arrayElement.getString("user_feedback"));
				hash.put("user_like", arrayElement.getString("user_like"));
				hash.put("user_not_like",
						arrayElement.getString("user_not_like"));
				hash.put("user_registered",
						arrayElement.getString("user_registered"));
				hash.put("user_score", arrayElement.getString("user_score"));
				JSONObject orgnazationObj = arrayElement
						.getJSONObject("organization");
				hash.put("orgnization", orgnazationObj.getString("name"));
				hash.put("orgnizationid", orgnazationObj.getString("id"));

				if (arrayElement.getString("user_like").equalsIgnoreCase("1")) {
					boolean isFound = false;
					for (int k = 0; k < BaseUrl.likedEventList.size(); k++) {
						HashMap<String, String> hm = BaseUrl.likedEventList
								.get(k);
						if (hm.get("id").equalsIgnoreCase(
								arrayElement.getString("id")))
							isFound = true;
					}
					if (!isFound)
						BaseUrl.likedEventList.add(hash);
				}
				arrayToAdd.add(hash);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent i = new Intent(this, SearchScreen_.class);
		startActivity(i);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}

}

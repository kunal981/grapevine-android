package com.grapevine.recommended;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.cuelogic.grapevine.R;
import com.grapevine.map.MapActivity_;
import com.grapevine.organization.OrganizationDetail_;
import com.grapevine.ui.SponserImageView;
import com.grapevine.views.BetterPopupWindow;
import com.grapevine.views.FacebookPost;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.socketconnection.BaseUrl;
import com.twitter.android.TwitterDirectPost;

public class EventDetail_ extends Activity implements OnClickListener {

	private ImageView eventImage;
	private TextView eventTitle, eventOrganizer, eventDate, eventDescription,
			eventAddress;
	private Button back, map;
	private LinearLayout eventAddToCalendarLayout, bookmarkLayout,
			eventRegisterLayout, shareImageLayout;
	private Button eventAddToCalendar, bookmark, shareImage, eventRegister;
	HashMap<String, String> eventDetailhashMap;
	public Bitmap bm = null;
	public int calID[];
	public String FROM = "";
	private LayoutInflater mInflater;

	DisplayImageOptions options;

	// AsynchUpdateAction updateActionAsynch;
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_details_);
		initImageLoader();
		BaseUrl.TAB1_STACK.add(this);
		eventImage = (ImageView) findViewById(R.id.eventImage);
		eventAddress = (TextView) findViewById(R.id.eventAddress);
		shareImage = (Button) findViewById(R.id.shareIV);
		shareImageLayout = (LinearLayout) findViewById(R.id.shareIVContainer);
		shareImageLayout.setOnClickListener(this);
		eventTitle = (TextView) findViewById(R.id.eventTitle);
		eventOrganizer = (TextView) findViewById(R.id.eventOrganizationName);
		eventOrganizer.setOnClickListener(this);
		eventDate = (TextView) findViewById(R.id.eventDate);
		eventDescription = (TextView) findViewById(R.id.eventDescription);
		eventRegister = (Button) findViewById(R.id.registerEvent);
		eventRegisterLayout = (LinearLayout) findViewById(R.id.registerEventContainer);
		eventRegisterLayout.setOnClickListener(this);
		eventAddToCalendar = (Button) findViewById(R.id.addToCalendarEvent);
		eventAddToCalendarLayout = (LinearLayout) findViewById(R.id.addToCalendarEventContainer);
		eventAddToCalendarLayout.setOnClickListener(this);
		back = (Button) findViewById(R.id.btBack);
		back.setOnClickListener(this);
		map = (Button) findViewById(R.id.mapBtn);
		map.setOnClickListener(this);
		bookmark = (Button) findViewById(R.id.bookmarkIB);
		bookmarkLayout = (LinearLayout) findViewById(R.id.bookmarkIBContainer);

		bookmarkLayout.setOnClickListener(this);
		Intent intent = getIntent();
		eventDetailhashMap = (HashMap<String, String>) intent
				.getSerializableExtra("EventDetail");

		((SponserImageView) findViewById(R.id.id_img_sponser_1))
				.setSponserImage(Integer.valueOf(BaseUrl.CommunityID));

		// ((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
		// .ConfigureTabBarPane(true, false, false, false);

		InitUI();
		// If SDK version is less than 14 then call this method to get all
		// Calenders
		if (Build.VERSION.SDK_INT < 14) {
			getCalendars();
		}

		if (intent.hasExtra("IsRegistered")) {
			showAlertDialog("Register?",
					"Did you complete registration for this event?");
		}
	}

	private void initImageLoader() {
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.no_image_icon)
				.showImageOnFail(R.drawable.no_image_icon).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1)).build();
	}

	/*
	 * Function is used to set the Data on Event Details screen
	 */
	public void InitUI() {
		// eventDistance.setText(eventDetailhashMap.get("distance") +
		// " miles away");
		Log.e("Image", eventDetailhashMap.get("image_url"));
		ImageLoader.getInstance().displayImage(
				eventDetailhashMap.get("image_url"), eventImage, options);
		eventTitle.setText(eventDetailhashMap.get("title"));
		if (!eventDetailhashMap.get("orgnization").equalsIgnoreCase("null"))
			eventOrganizer.setText(eventDetailhashMap.get("orgnization"));

		if (eventDetailhashMap.get("start_time").length() != 0) {
			String DateTime = getDate(eventDetailhashMap.get("start_time"));
			eventDate.setText(DateTime);
		} else {
			eventDate.setText("Timing information not available right now.");
		}
		if (eventDetailhashMap.get("city").length() > 0
				&& eventDetailhashMap.get("state").length() > 0) {
			eventAddress.setText(eventDetailhashMap.get("location") + ", "
					+ eventDetailhashMap.get("address_line_1") + ", "
					+ eventDetailhashMap.get("city") + ", "
					+ eventDetailhashMap.get("state") + " "
					+ eventDetailhashMap.get("postalcode"));
		} else {
			eventAddress.setVisibility(View.GONE);
		}

		eventDescription.setText(eventDetailhashMap.get("description"));
		if (eventDetailhashMap.get("user_registered").equalsIgnoreCase("1"))
			eventRegister.setSelected(true);
		if (BaseUrl.bookmarkedEventId.contains(eventDetailhashMap.get("id")))
			bookmark.setSelected(true);
		else
			bookmark.setSelected(false);

		if (!eventDetailhashMap.get("latitude").contains("null")
				&& !eventDetailhashMap.get("longitude").contains("null")) {
			eventAddress.setOnClickListener(this);
		} else {
			map.setVisibility(View.GONE);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bookmarkIBContainer:
			if (v.isSelected()) {
				v.setSelected(false);
				BaseUrl.bookmarkedEventId.remove(eventDetailhashMap.get("id"));
				BaseUrl.bookmarkedEventList.remove(eventDetailhashMap);
				new AsynchUpdateAction(eventDetailhashMap.get("id"),
						"unbookmark", EventDetail_.this).execute();
			} else {
				v.setSelected(true);
				boolean isFound = false;
				for (int k = 0; k < BaseUrl.bookmarkedEventList.size(); k++) {
					HashMap<String, String> hm = BaseUrl.bookmarkedEventList
							.get(k);
					if (hm.get("id").equalsIgnoreCase(
							eventDetailhashMap.get("id")))
						isFound = true;
				}
				if (!isFound) {
					BaseUrl.bookmarkedEventId.add(eventDetailhashMap.get("id"));
					BaseUrl.bookmarkedEventList.add(eventDetailhashMap);
				}
				new AsynchUpdateAction(eventDetailhashMap.get("id"),
						"bookmark", EventDetail_.this).execute();
			}
			break;
		case R.id.btBack:

			Intent i = new Intent(this, Recommended_.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();

			break;

		case R.id.mapBtn:
			if (!eventDetailhashMap.get("latitude").contains("null")
					&& !eventDetailhashMap.get("longitude").contains("null")) {
				Intent mapActivity = new Intent(this, MapActivity_.class);
				mapActivity.putExtra("LATITUDE",
						eventDetailhashMap.get("latitude"));
				mapActivity.putExtra("LONGITUDE",
						eventDetailhashMap.get("longitude"));
				this.startActivity(mapActivity);
			} else {
				Toast.makeText(this, "No latitude/longitude found",
						Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.eventAddress:
			if (!eventDetailhashMap.get("latitude").contains("null")
					&& !eventDetailhashMap.get("longitude").contains("null")) {
				Intent mapActivity = new Intent(this, MapActivity_.class);
				mapActivity.putExtra("LATITUDE",
						eventDetailhashMap.get("latitude"));
				mapActivity.putExtra("LONGITUDE",
						eventDetailhashMap.get("longitude"));
				this.startActivity(mapActivity);
			}
			break;

		case R.id.addToCalendarEventContainer:
			// Since SDK version > 4.0 has different APIs we have to call
			// different methods to add
			// an event depending upon SDK version
			if (Build.VERSION.SDK_INT >= 14)
				addEventForHighSDK();
			else
				addEvent();
			break;

		case R.id.registerEventContainer:
			if (!v.isSelected()) {
				Intent registerEventIntent = new Intent(this,
						RegisterForEvent_.class);
				// registerEventIntent.putExtra("url", url);
				registerEventIntent.putExtra("url",
						eventDetailhashMap.get("registration_url"));
				registerEventIntent.putExtra("from", "recommended");
				registerEventIntent.putExtra("EventDetail", eventDetailhashMap);

				// startActivityForResult(registerEventIntent, 1);
				BaseUrl.registeredEventId.add(eventDetailhashMap.get("id"));
				boolean isFound = false;
				for (int k = 0; k < BaseUrl.registeredEventList.size(); k++) {
					HashMap<String, String> hm = BaseUrl.registeredEventList
							.get(k);
					if (hm.get("id").equalsIgnoreCase(
							eventDetailhashMap.get("id")))
						isFound = true;
				}
				if (!isFound)
					BaseUrl.registeredEventList.add(eventDetailhashMap);

				startActivity(registerEventIntent);
				// finish();
				overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			} else {
				showAlertDialog("Register?",
						"Did you complete registration for this event?");
			}
			break;

		case R.id.shareIVContainer:
			DemoPopupWindow dw = new DemoPopupWindow(v, EventDetail_.this);
			dw.showLikeQuickAction(0, 0);
			// Toast.makeText(EventDetail_.this, "Under development",
			// Toast.LENGTH_SHORT).show();
			break;

		case R.id.eventOrganizationName:
			Intent callOrganizationDetail = new Intent(EventDetail_.this,
					OrganizationDetail_.class);
			callOrganizationDetail.putExtra("organizationId",
					eventDetailhashMap.get("orgnizationid"));
			callOrganizationDetail.putExtra("from", "EventDetail");
			callOrganizationDetail.putExtra("organizationName",
					eventDetailhashMap.get("title"));
			startActivity(callOrganizationDetail);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			break;

		default:
			break;
		}

	}

	private String getDate(String timeStampStr) {

		try {
			SimpleDateFormat writeFormat = new SimpleDateFormat(
					"MMM dd, yyyy hh:mm aaa");
			Date netDate = (new Date(Long.parseLong(timeStampStr) * 1000));
			return writeFormat.format(netDate);
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		Activity act = BaseUrl.TAB1_STACK.get(BaseUrl.TAB1_STACK.size() - 2);
		Intent i;
		if (act.getClass().equals(this.getClass())) {
			i = new Intent(this, Recommended_.class);
		} else {
			i = new Intent(this, act.getClass());
		}
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(i);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();

	}

	/* retrieve a list of available calendars */
	private MyCalendar m_calendars[];
	private String m_selectedCalendarId = "0";

	private void getCalendars() {
		String[] l_projection = new String[] { "_id", "displayName" };
		Uri l_calendars;
		if (Build.VERSION.SDK_INT >= 8) {
			l_calendars = Uri.parse("content://com.android.calendar/calendars");
		} else {
			l_calendars = Uri.parse("content://calendar/calendars");
		}
		Cursor l_managedCursor = this.managedQuery(l_calendars, l_projection,
				null, null, null); // all calendars
		// Cursor l_managedCursor = this.managedQuery(l_calendars, l_projection,
		// "selected=1", null, null); //active calendars
		if (l_managedCursor.moveToFirst()) {
			m_calendars = new MyCalendar[l_managedCursor.getCount()];
			String l_calName;
			String l_calId;
			int l_cnt = 0;
			int l_nameCol = l_managedCursor.getColumnIndex(l_projection[1]);
			int l_idCol = l_managedCursor.getColumnIndex(l_projection[0]);
			do {
				l_calName = l_managedCursor.getString(l_nameCol);
				l_calId = l_managedCursor.getString(l_idCol);
				m_calendars[l_cnt] = new MyCalendar(l_calName, l_calId);
				++l_cnt;
			} while (l_managedCursor.moveToNext());
		}
	}

	class MyCalendar {
		public String name;
		public String id;

		public MyCalendar(String _name, String _id) {
			name = _name;
			id = _id;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	/* add an event to calendar */
	private void addEvent() {
		ContentValues l_event = new ContentValues();
		long t, t1 = 0;
		String c = eventDetailhashMap.get("end_time");
		if (!c.contains("null")) {
			t = Long.parseLong(c);
			t1 = t * 1000;
		} else {
			t = System.currentTimeMillis();
			t1 = t * 1000;
		}
		l_event.put("calendar_id", "1");
		l_event.put("title", eventDetailhashMap.get("title"));
		l_event.put("description", eventDetailhashMap.get("description"));
		l_event.put("eventLocation", eventDetailhashMap.get("location"));
		l_event.put("dtstart", t1);
		l_event.put("dtend", t1);
		l_event.put("allDay", 0);
		// status: 0~ tentative; 1~ confirmed; 2~ canceled
		l_event.put("eventStatus", 1);
		// 0~ default; 1~ confidential; 2~ private; 3~ public
		l_event.put("visibility", 0);
		// 0~ opaque, no timing conflict is allowed; 1~ transparency, allow
		// overlap of scheduling
		l_event.put("transparency", 0);
		// 0~ false; 1~ true
		l_event.put("hasAlarm", 1);
		Uri l_eventUri;
		if (Build.VERSION.SDK_INT >= 8) {
			l_eventUri = Uri.parse("content://com.android.calendar/events");
		} else {
			l_eventUri = Uri.parse("content://calendar/events");
		}
		Uri l_uri = this.getContentResolver().insert(l_eventUri, l_event);
		Toast toast = Toast.makeText(EventDetail_.this,
				"Event added to the calendar.", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		Log.v("++++++test", l_uri.toString());
	}

	// For android OS 4.0 and above
	public void addEventForHighSDK() {
		Uri uri = Uri.parse("content://com.android.calendar/calendars");
		String[] projection = new String[] { CalendarContract.Calendars._ID,
				CalendarContract.Calendars.ACCOUNT_NAME,
				CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
				CalendarContract.Calendars.NAME,
				CalendarContract.Calendars.CALENDAR_COLOR,

		};

		Cursor calendarCursor = managedQuery(uri, projection, null, null, null);
		if (calendarCursor.moveToFirst()) {
			m_calendars = new MyCalendar[calendarCursor.getCount()];
			calID = new int[calendarCursor.getCount()];
			String l_calName;
			String l_calId;
			int l_cnt = 0;
			int l_nameCol = calendarCursor.getColumnIndex(projection[1]);
			int l_idCol = calendarCursor.getColumnIndex(projection[0]);
			do {
				l_calName = calendarCursor.getString(l_nameCol);
				l_calId = calendarCursor.getString(l_idCol);
				m_calendars[l_cnt] = new MyCalendar(l_calName, l_calId);
				calID[l_cnt] = Integer.parseInt(l_calId);
				++l_cnt;
			} while (calendarCursor.moveToNext());
		}
		// Construct event details
		long startMillis, t = 0;
		long endMillis, t1 = 0;
		String c = eventDetailhashMap.get("start_time");
		if (!c.contains("null")) {
			t = Long.parseLong(c);
			t1 = t * 1000;
		} else {
			t = System.currentTimeMillis();
			t1 = t * 1000;
		}
		TimeZone timeZone = TimeZone.getDefault();

		// Insert Event
		ContentResolver cr = getContentResolver();
		ContentValues values = new ContentValues();
		for (int i = 0; i < calID.length; i++) {
			values.put(CalendarContract.Events.DTSTART, t1);
			values.put(CalendarContract.Events.DTEND, t1);
			values.put(CalendarContract.Events.TITLE,
					eventDetailhashMap.get("title"));
			values.put(CalendarContract.Events.DESCRIPTION,
					eventDetailhashMap.get("description"));
			values.put(CalendarContract.Events.CALENDAR_ID, calID[i]);
			values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
			Uri l_eventUri;
			l_eventUri = Uri.parse("content://com.android.calendar/events");
			Uri uri1 = cr.insert(l_eventUri, values);
			Toast toast = Toast.makeText(EventDetail_.this,
					"Event added to the calendar.", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			break;
		}
	}

	// @Override
	// protected void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// // TODO Auto-generated method stub
	// overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	// super.onActivityResult(requestCode, resultCode, data);
	// if(requestCode == 1){
	// showAlertDialog("Register?",
	// "Did you complete registration for this event?");
	//
	// }
	// }
	@SuppressWarnings("deprecation")
	public void showAlertDialog(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message)
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								new AsynchUpdateAction(eventDetailhashMap
										.get("id"), "register",
										EventDetail_.this).execute();
								dialog.cancel();
								eventRegister.setSelected(true);
								BaseUrl.registeredEventId
										.add(eventDetailhashMap.get("id"));
								boolean isFound = false;
								for (int k = 0; k < BaseUrl.registeredEventList
										.size(); k++) {
									HashMap<String, String> hm = BaseUrl.registeredEventList
											.get(k);
									if (hm.get("id").equalsIgnoreCase(
											eventDetailhashMap.get("id")))
										isFound = true;
								}
								if (!isFound)
									BaseUrl.registeredEventList
											.add(eventDetailhashMap);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						new AsynchUpdateAction(eventDetailhashMap.get("id"),
								"unregister", EventDetail_.this).execute();
						dialog.cancel();
						eventRegister.setSelected(false);
						BaseUrl.registeredEventId.remove(eventDetailhashMap
								.get("id"));
						BaseUrl.registeredEventList.remove(eventDetailhashMap);
					}
				});
		builder.show();
	}

	// ************** Class for pop-up window **********************
	/**
	 * The Class DemoPopupWindow.
	 */
	private class DemoPopupWindow extends BetterPopupWindow implements
			OnClickListener {

		/**
		 * Instantiates a new demo popup window.
		 *
		 * @param anchor
		 *            the anchor
		 * @param cnt
		 *            the cnt
		 */
		public DemoPopupWindow(View anchor, Context cnt) {
			super(anchor);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cellalert24.Views.BetterPopupWindow#onCreate()
		 */
		@Override
		protected void onCreate() {
			// inflate layout
			LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			ViewGroup root = (ViewGroup) inflater.inflate(
					R.layout.share_choose_popup, null);

			// setup button events
			for (int i = 0, icount = root.getChildCount(); i < icount; i++) {
				View v = root.getChildAt(i);

				if (v instanceof TableRow) {
					TableRow row = (TableRow) v;

					for (int j = 0, jcount = row.getChildCount(); j < jcount; j++) {
						View item = row.getChildAt(j);
						if (item instanceof Button) {
							Button b = (Button) item;
							b.setOnClickListener(this);
						}
					}
				}
			}

			this.setContentView(root);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			this.dismiss();
			Intent intent;
			switch (v.getId()) {
			case R.id.shareFacebookBtn:
				FacebookPost fb = new FacebookPost(EventDetail_.this,
						eventDetailhashMap.get("image_url"),
						eventDetailhashMap.get("title"),
						eventDetailhashMap.get("description"));
				break;

			case R.id.shareTwitterBtn:
				new TwitterDirectPost(EventDetail_.this,
						eventDetailhashMap.get("public_url"));
				break;

			case R.id.shareEmailBtn:

				String eventUrl = eventDetailhashMap.get("event_url");
				String body = "<div style=\"margin:10px auto;padding:10px;border:2px solid #ccc; font-family:Arial;font-size:12px;\"><div style=\"margin-bottom: 10px; padding-bottom: 7px;\"><img src=\"http://www.grape-vine.com/images/logo.png\" alt=\"GrapeVine\" /></div><div style=\"background-color:#f6f6f6;padding:10px;moz-border:5px;border:1px solid #e5e5e5;\"><br /> Hi,<br /><br />I thought you might like this link I found on <a href=\"http://www.grape-vine.com\">GrapeVine</a>. <a href=\""
						+ eventUrl
						+ "\" target=\"_blank\">Click here</a> to check it out!<br /><br/>Or, find out what else they've heard you might like at <a href=\"http://www.grape-vine.com\">GrapeVine:</a> Your trusted source for recommended events in your community!<br /><br />Sincerely,<br /><br/><br/>GrapeVine is currently being offered in Rhode Island, Columbus, OH, and New York City. If you want more information about bringing GrapeVine to a city near you,please email us at <a href=\"mailto:info@grape-vine.com\">info@grape-vine.com</a></div></div>";
				Intent intent1 = new Intent(Intent.ACTION_SENDTO); // it's not
																	// ACTION_SEND
				intent1.setType("text/html");
				// Uri uri = Uri.fromFile(new
				// File("file:///android_asset/friend_share.htm"));
				// intent1.putExtra(android.content.Intent.EXTRA_STREAM, uri);
				intent1.putExtra(Intent.EXTRA_SUBJECT,
						"Check out this event from Grape-vine");
				intent1.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(body));
				intent1.setData(Uri.parse("mailto:")); // or just "mailto:" for
														// blank
				intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will
																	// make such
																	// that when
																	// user
																	// returns
																	// to your
																	// app, your
																	// app is
																	// displayed,
																	// instead
																	// of the
																	// email
																	// app.
				startActivity(intent1);
				break;

			case R.id.cancelBtn:
				dismiss();
				break;
			}
		}
	}
	// String s =
	// "<div style=\"margin:10px auto;padding:10px;border:2px solid #ccc; font-family:Arial;font-size:12px;\"><div style=\"margin-bottom: 10px; padding-bottom: 7px;\"><img src=\"http://www.grape-vine.com/images/logo.png\" alt=\"GrapeVine\" /></div><div style=\"background-color:#f6f6f6;padding:10px;moz-border:5px;border:1px solid #e5e5e5;\"><br /> Hi,<br /><br />I thought you might like this link I found on <a href=\"http://www.grape-vine.com\">GrapeVine</a>. <a href=\"%@\" target=\"_blank\">Click here</a> to check it out!<br /><br/>Or, find out what else they've heard you might like at <a href=\"http://www.grape-vine.com\">GrapeVine:</a> Your trusted source for recommended events in your community!<br /><br />Sincerely,<br /> %@ %@<br/><br/>GrapeVine is currently being offered in Rhode Island, Columbus, OH, and New York City. If you want more information about bringing GrapeVine to a city near you,please email us at <a href=\"mailto:info@grape-vine.com\">info@grape-vine.com</a></div></div>";

}

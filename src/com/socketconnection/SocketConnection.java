package com.socketconnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class SocketConnection {
	DefaultHttpClient httpClient;
	HttpContext localContext;
	private String ret;
	Context context;
	HttpResponse response = null;
	HttpPost httpPost = null;
	HttpGet httpGet = null;

	public HttpClient getNewHttpClient() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(
					params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

	public SocketConnection(Context ctx) {
		HttpParams myParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(myParams, 100000000);
		HttpConnectionParams.setSoTimeout(myParams, 100000000);
		// httpClient = new DefaultHttpClient(myParams);
		httpClient = (DefaultHttpClient) getNewHttpClient();
		localContext = new BasicHttpContext();
		this.context = ctx;
	}

	public void clearCookies() {
		httpClient.getCookieStore().clear();
	}

	public void abort() {
		try {
			if (httpClient != null) {
				httpPost.abort();
			}
		} catch (Exception e) {
		}
	}

	public String sendPost(String url, String data) {
		System.out.println("" + url);
		return sendPost(url, data, null);

	}

	public String sendJSONPost(String url, JSONObject data) {
		return sendPost(url, data.toString(), "application/json");
	}

	public String sendPost(String url, String data, String contentType) {
		ret = null;
		httpPost = new HttpPost(url);
		response = null;
		StringEntity tmp = null;
		httpPost.setHeader("Accept", "application/json");

		if (contentType != null) {
			httpPost.setHeader("Content-Type", contentType);
		} else {
			httpPost.setHeader("Content-Type", "application/json");
		}
		try {
			tmp = new StringEntity(data);

		} catch (UnsupportedEncodingException e) {
		}

		httpPost.setEntity(tmp);

		try {
			response = httpClient.execute(httpPost, localContext);
			if (response != null) {
				ret = EntityUtils.toString(response.getEntity());
			}
		} catch (Exception e) {
			System.out.println("The exception is " + e);
			// Toast.makeText(context, "Please check your Internet Connection",
			// Toast.LENGTH_SHORT).show();
			// return null;
		}
		return ret;
	}

	public static final int HTTP_SESSION_TIME_OUT = (int) (30 * 1000);

	public String getResponse(String url) throws IOException {
		InputStream iStream = null;
		String jString = null;

		HttpResponse response = null;
		// Making HTTP request
		try {
			// defaultHttpClient
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams,
					HTTP_SESSION_TIME_OUT);
			HttpConnectionParams
					.setSoTimeout(httpParams, HTTP_SESSION_TIME_OUT);
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			iStream = entity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Exception", "" + e.getMessage());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					iStream, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "");
			}
			jString = sb.toString();
			Log.d("" + "Response", jString);
		} catch (Exception e) {
			Log.e("" + "Buffer Error",
					"Error converting result " + e.toString());
		} finally {
			if (iStream != null) {
				iStream.close();
			}

		}
		return jString;

	}

	public InputStream getHttpStream(String urlString) throws IOException {
		InputStream in = null;
		int response = -1;

		System.out.println("urlString ... " + urlString);
		//
		URL url = new URL(urlString.toString());
		HttpURLConnection httpconn = null;

		if (url.getProtocol().toLowerCase().equals("https")) {
			trustAllHosts();
			HttpsURLConnection https = (HttpsURLConnection) url
					.openConnection();
			https.setHostnameVerifier(DO_NOT_VERIFY);
			httpconn = https;
		} else {
			httpconn = (HttpURLConnection) url.openConnection();
		}
		try {
			httpconn.setAllowUserInteraction(false);
			httpconn.setInstanceFollowRedirects(true);
			httpconn.setRequestMethod("GET");
			httpconn.connect();

			// int len = httpconn.getContentLength();
			// System.out.println("len .."+len);

			response = httpconn.getResponseCode();

			System.out.println("response ... " + response);
			if (response == httpconn.HTTP_OK) {
				in = httpconn.getInputStream();

			}

		} catch (Exception e) {
			throw new IOException("Error connecting");

		}

		return in;
	}

	public String convertStreamToString(InputStream is) throws IOException {
		System.out.println("is ... " + is);
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[5012];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

	public String SendPUTdata(String urlTO, JSONObject jobj) {
		URI url = URI.create(urlTO);
		HttpPut p = new HttpPut(url);
		System.out.println("" + url);
		System.out.println("" + jobj.toString());
		try {

			StringEntity s = new StringEntity(jobj.toString());
			p.setHeader("Accept", "application/json");
			p.setHeader("Content-Type", "application/json");
			p.setEntity(s);
			HttpResponse response = httpClient.execute(p);
			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append((line + "\n"));
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			System.out.println("" + sb.toString());
			return sb.toString();
		} catch (Exception e) {
			System.out.println("" + e);
		}
		return null;
	}

	// For Get method to avoid client server authentication.
	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	private static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			public void checkClientTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}

			public void checkServerTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
package com.cuelogic.grapevine;

/* A class which is use to see Login/Register screen.
 * @author Cuelogic Technologies
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cuelogic.grapevine.util.AppConstant.Config;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.SessionEvents;
import com.facebook.android.SessionEvents.AuthListener;
import com.facebook.android.SessionStore;
import com.facebook.android.Util;
import com.grapevine.interest.Intersest_;
import com.grapevine.recommended.Recommended_;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class LoginRegister_ extends Activity implements OnClickListener {
	SocketConnection sc;
	BaseUrl bu;

	private EditText email, password;
	private Button login;
	private Button btnloginFB;
	private TextView textForgotPassword, textRegister, errorTextview,
			privacyTerms, termsOfUse, aboutUs;
	private ProgressDialog pd, pd1;
	public Facebook mFacebook;
	public static AsyncFacebookRunner mAsyncRunner;
	private SharedPreferences shared;
	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "read_stream", "email" };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_register_);
		email = (EditText) findViewById(R.id.emailEdit);
		password = (EditText) findViewById(R.id.passwordEdit);
		login = (Button) findViewById(R.id.loginBtn);
		btnloginFB = (Button) findViewById(R.id.LoginFB_button);
		btnloginFB.setOnClickListener(this);

		textRegister = (TextView) findViewById(R.id.txt_no_account);
		textForgotPassword = (TextView) findViewById(R.id.text_forgot_passoword);
		errorTextview = (TextView) findViewById(R.id.ErrorMessage);
		privacyTerms = (TextView) findViewById(R.id.privacyTermsTV);
		termsOfUse = (TextView) findViewById(R.id.termsOfUseTV);
		aboutUs = (TextView) findViewById(R.id.aboutUsTV);
		privacyTerms.setOnClickListener(this);
		termsOfUse.setOnClickListener(this);
		aboutUs.setOnClickListener(this);
		login.setOnClickListener(this);
		textForgotPassword.setOnClickListener(this);
		textRegister.setOnClickListener(this);
		sc = new SocketConnection(this);
		bu = new BaseUrl();

		mFacebook = new Facebook(bu.APP_ID);
		SessionStore.restore(mFacebook, this);
		mAsyncRunner = new AsyncFacebookRunner(mFacebook);
		SessionEvents.addAuthListener(new SampleAuthListener());
		if (mFacebook.isSessionValid()) {
			logoutFB(this);
		}

		turnGPSOn();
		LocationChangeHandler.setInstance(new LocationChangeHandler(
				getApplicationContext()));
		LocationChangeHandler.getInstance().beginUpdate();
	}

	/**
	 * Logout facebook.
	 */
	public static void logoutFB(Context ctx) {
		mAsyncRunner.logout(((Activity) ctx), new RequestListener() {

			@Override
			public void onComplete(String response) {

				System.out.println("fblogout");

			}

			@Override
			public void onIOException(IOException e) {

			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {

			}

			@Override
			public void onMalformedURLException(MalformedURLException e) {

			}

			@Override
			public void onFacebookError(FacebookError e) {

			}
		});
	}

	/**
	 * The listener interface for receiving loginDialog events. The class that
	 * is interested in processing a loginDialog event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addLoginDialogListener<code> method. When
	 * the loginDialog event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see LoginDialogEvent
	 */
	public final class LoginDialogListener implements DialogListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.facebook.android.Facebook.DialogListener#onComplete(android.os
		 * .Bundle)
		 */
		public void onComplete(Bundle values) {
			try {

				// The user has logged in, so now you can query and use their
				// Facebook info
				new AsynchGetFBdetails().execute();
			} catch (Exception error) {
				System.out.println("execption -----" + error.getMessage());
				afterLogin();
				CallLoginUsingFacebook();
			}
			// catch (FacebookError error) {
			// System.out.println("execption -----"+error.getMessage());
			// }
		}

		public void onFacebookError(FacebookError error) {
			Log.e("FacebookError", error + "");
		}

		public void onError(DialogError error) {

		}

		public void onCancel() {

		}

	}

	/*
	 * Stores access token and token expires details in shared preference.
	 */
	public void afterLogin() {
		Editor editor = getSharedPreferences(bu.KEY,
				Context.MODE_WORLD_WRITEABLE).edit();
		editor.putString("access_token", mFacebook.getAccessToken());
		editor.putLong("access_expires", mFacebook.getAccessExpires());

		editor.commit();
	}

	/*
	 * A user login via Facebook service is called.
	 */
	public void CallLoginUsingFacebook() {
		System.out.println("Came here from successful login of facebook");
		new AsynchFBLogin().execute();

	}

	/**
	 * The listener interface for receiving sampleAuth events. The class that is
	 * interested in processing a sampleAuth event implements this interface,
	 * and the object created with that class is registered with a component
	 * using the component's <code>addSampleAuthListener<code> method. When
	 * the sampleAuth event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SampleAuthEvent
	 */
	public class SampleAuthListener implements AuthListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.facebook.android.SessionEvents.AuthListener#onAuthSucceed()
		 */
		public void onAuthSucceed() {
			mAsyncRunner.request("me", new SampleRequestListener());
			SharedPreferences.Editor editor = shared.edit();
			editor.putString("fbvariable", "true");
			editor.commit();
			finish();

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.facebook.android.SessionEvents.AuthListener#onAuthFail(java.lang
		 * .String)
		 */
		public void onAuthFail(String error) {
			finish();
		}
	}

	/**
	 * The listener interface for receiving sampleRequest events. The class that
	 * is interested in processing a sampleRequest event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addSampleRequestListener<code> method. When
	 * the sampleRequest event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SampleRequestEvent
	 */
	public class SampleRequestListener extends BaseRequestListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.facebook.android.AsyncFacebookRunner.RequestListener#onComplete
		 * (java.lang.String)
		 */
		public void onComplete(final String response) {
			try {
				Log.d("Facebook-Example", "Response: " + response.toString());
				JSONObject FBResponseJson = Util.parseJson(response);
				Editor editor = getSharedPreferences(bu.KEY,
						Context.MODE_PRIVATE).edit();
				editor.putString("facebookID", FBResponseJson.getString("id"));
				editor.putString("firstName",
						FBResponseJson.getString("first_name"));
				editor.putString("lastName",
						FBResponseJson.getString("last_name"));
				editor.putString("emailID", FBResponseJson.getString("email"));
				editor.putString("username", FBResponseJson.getString("name"));
				editor.putString("link", FBResponseJson.getString("link"));
				editor.putString("gender", FBResponseJson.getString("gender"));
				editor.putString("local", FBResponseJson.getString("locale"));
				editor.putString("timezone",
						FBResponseJson.getString("timezone"));
				try {
					editor.putString("verified",
							FBResponseJson.getString("verified"));
				} catch (Exception e) {

					editor.putString("verified", "");
				}

				editor.putString("updatedTime",
						FBResponseJson.getString("updated_time"));

				if (FBResponseJson.has("location")) {
					JSONObject json_location = FBResponseJson
							.getJSONObject("location");
					String location = json_location.getString("name");
					String[] location_of_user = new String[2];
					if (location.contains(","))
						location_of_user = location.split(",");
					else
						location_of_user[0] = location;
					location_of_user[1] = "";

					editor.putString("city", location_of_user[0]);
					editor.putString("state", location_of_user[1]);
				}
				editor.commit();
				// CallLoginUsingFacebook();
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.loginBtn:
			if (email.getText().length() > 0 && password.getText().length() > 0) {
				if (checkValidEmail(email.getText().toString())) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(password.getWindowToken(), 0);
					new AsynchLogin().execute();
				} else {
					errorTextview.setVisibility(View.VISIBLE);
					email.setText("");
					password.setText("");
				}
			} else {
				errorTextview.setVisibility(View.VISIBLE);
				email.setText("");
				password.setText("");
			}
			break;
		case R.id.txt_no_account:
			// Toast.makeText(this, "Comming soon", Toast.LENGTH_SHORT).show();
			Intent callRegisterationActivity = new Intent(this,
					RegisterActivity.class);
			startActivity(callRegisterationActivity);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			finish();
			break;
		case R.id.text_forgot_passoword:
			Intent callForgotPassword = new Intent(this, ForgotPassword_.class);
			startActivity(callForgotPassword);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			finish();
			break;
		case R.id.LoginFB_button:
			mFacebook.authorize(LoginRegister_.this, PERMISSIONS,
					new LoginDialogListener());
			break;
		case R.id.privacyTermsTV:
			Intent call_privacy = new Intent(LoginRegister_.this,
					PrivacyPolicy.class);
			call_privacy.putExtra("WEBVIEW", "privacy");

			startActivity(call_privacy);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			finish();
			break;
		case R.id.termsOfUseTV:
			Intent call_terms = new Intent(LoginRegister_.this,
					PrivacyPolicy.class);
			call_terms.putExtra("WEBVIEW", "terms");

			startActivity(call_terms);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			finish();
			break;
		case R.id.aboutUsTV:
			Intent call_about = new Intent(LoginRegister_.this,
					PrivacyPolicy.class);
			call_about.putExtra("WEBVIEW", "aboutus");

			startActivity(call_about);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			finish();
			break;
		default:
			break;
		}
	}

	/*
	 * An Email validation function which checks the entered email is a valid
	 * email or not. returns true if valid , false if invalid
	 */
	protected boolean checkValidEmail(String email) {
		boolean isValid = false;
		// String PATTERN =
		// "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(email);
		isValid = matcher.matches();
		return isValid;
	}

	/*
	 * A function used to turn-on device GPS if it is off.
	 */
	public void turnGPSOn() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", true);
		this.sendBroadcast(intent);
		String provider = Settings.Secure.getString(this.getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) { // if gps is disabled

			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			this.sendBroadcast(poke);

		}
	}

	/*
	 * A function used to turn-off device GPS.
	 */
	public void turnGPSOff() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", false);
		this.sendBroadcast(intent);
	}

	/*
	 * A user Login API is called.
	 */
	public class AsynchLogin extends AsyncTask<Void, Void, String> {
		String s;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// pd = ProgressDialog.show(LoginRegister_.this, "",
			// "Please Wait..");
			DesignUtil.showProgressDialog(LoginRegister_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				JSONObject loginJson = new JSONObject();
				try {
					loginJson.put("type", "existing");
					loginJson.put("password", password.getText().toString()
							.trim());
					loginJson.put("email", email.getText().toString().trim());
					if (LocationChangeHandler.getLocation() != null) {
						loginJson.put("lat", LocationChangeHandler
								.getLocation().getLatitude() + "");
						loginJson.put("lon", LocationChangeHandler
								.getLocation().getLongitude() + "");
					} else {
						loginJson.put("lat", "35.74184050952353");
						loginJson.put("lon", "-118.8128372137487");
					}
					s = sc.sendJSONPost(
							BaseUrl.Development_Uri + bu.getMethodName("LOGIN")
									+ BaseUrl.Api_key, loginJson);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return s;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd.dismiss();
			DesignUtil.hideProgressDialog();
			LocationChangeHandler.getInstance().stopUpdates();
			turnGPSOff();
			if (s != null) {
				try {
					JSONObject loginRespone = new JSONObject(result);
					bu.UserInfo = loginRespone;
					bu.USER_KEY = loginRespone.getString("user_key");
					JSONArray bookmarkedEventArray = loginRespone
							.getJSONArray("event_bookmarks");
					if (bookmarkedEventArray.length() > 0) {
						for (int i = 0; i < bookmarkedEventArray.length(); i++) {
							BaseUrl.bookmarkedEventId.add(bookmarkedEventArray
									.getString(i));
						}
					}
					// Put all the Communities,Neighborhoods and Selected
					// community Location at one place.

					bu.parentCommunityArray = new JSONArray();
					bu.parentCommunityArray = loginRespone
							.getJSONArray("communities_all");
					bu.neighborhoodCommunityAllArray = new JSONArray();
					bu.neighborhoodCommunityAllArray = loginRespone
							.getJSONArray("neighborhoods_all");
					bu.CommunityID = loginRespone.getString("community_id");
					bu.AllInterestArray = new JSONArray();
					bu.AllInterestArray = loginRespone
							.getJSONArray("interests_all");
					bu.AllOrganizationArray = new JSONArray();
					bu.AllOrganizationArray = loginRespone
							.getJSONArray("organizations_all");
					bu.AllCommunitiesArray = new JSONArray();
					bu.AllCommunitiesArray = loginRespone
							.getJSONArray("target_communities_all");
					System.out.println("" + bu.parentCommunityArray + ""
							+ bu.neighborhoodCommunityAllArray);
					// This will take all previously selected Interest and
					// communities
					bu.checkedINTEREST = new ArrayList<String>();
					if (!loginRespone.isNull("interests")) {
						JSONArray userSelectedInterest = loginRespone
								.getJSONArray("interests");
						if (userSelectedInterest.length() > 0) {
							for (int i = 0; i < userSelectedInterest.length(); i++) {
								bu.checkedINTEREST.add(userSelectedInterest
										.getString(i));
							}
						}
					}
					bu.checkedCOMMUNITIES = new ArrayList<String>();
					if (!loginRespone.isNull("target_communities")) {
						JSONArray userSelectedCommunities = loginRespone
								.getJSONArray("target_communities");
						if (userSelectedCommunities.length() > 0) {
							for (int i = 0; i < userSelectedCommunities
									.length(); i++) {
								bu.checkedCOMMUNITIES
										.add(userSelectedCommunities
												.getString(i));
							}
						}
					}
					Editor editor = getSharedPreferences(bu.KEY,
							Context.MODE_PRIVATE).edit();
					editor.putString("USER_KEY",
							loginRespone.getString("user_key"));
					editor.commit();
					int KEY_STATUS = loginRespone.getInt("key_status");
					int is_new = loginRespone.getInt("is_new");
					errorTextview.setVisibility(View.GONE);
					email.setText("");
					password.setText("");
					int sponserId = Integer.valueOf(bu.CommunityID);
					getSharedPreferences(bu.KEY, Context.MODE_PRIVATE).edit()
							.putInt(Config.KEY_PREF_SPONSER_ID, sponserId)
							.commit();
					Intent i = new Intent(getApplicationContext(),
							Recommended_.class);
					startActivity(i);
					overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
					finish();

					// Intent call_loginRegister = new
					// Intent(LoginRegister_.this,
					// Intersest_.class);
					// call_loginRegister.putExtra("allInterest",
					// bu.AllInterestArray.toString());
					// call_loginRegister.putExtra("allCommunities",
					// bu.AllCommunitiesArray.toString());
					// startActivity(call_loginRegister);
					// finish();

				} catch (JSONException e) {
					e.printStackTrace();
					// Toast.makeText(LoginRegister.this, "Login Unsuccessful",
					// Toast.LENGTH_SHORT).show();
					errorTextview.setVisibility(View.VISIBLE);
					errorTextview.setText("Invalid Email/Password");
					return;

				}
			} else {
				Toast.makeText(LoginRegister_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
			System.out.println("" + result);
		}

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		LocationChangeHandler.getInstance().stopUpdates();
		turnGPSOff();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		mFacebook.authorizeCallback(requestCode, resultCode, data);
	}

	/*
	 * A thread which is called store details of user from Facebook.
	 */
	public class AsynchGetFBdetails extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			DesignUtil.showProgressDialog(LoginRegister_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			JSONObject facebookResponseJson;
			try {
				facebookResponseJson = Util.parseJson(mFacebook.request("me"));
				Log.e("Facebook", facebookResponseJson + "");
				SessionStore.save(mFacebook, LoginRegister_.this);
				Editor editor = getSharedPreferences(bu.KEY,
						Context.MODE_WORLD_WRITEABLE).edit();

				editor.putString("facebookID",
						facebookResponseJson.getString("id"));
				editor.putString("firstName",
						facebookResponseJson.getString("first_name"));
				editor.putString("lastName",
						facebookResponseJson.getString("last_name"));
				editor.putString("emailID",
						facebookResponseJson.getString("email"));
				editor.putString("username",
						facebookResponseJson.getString("name"));
				editor.putString("link", facebookResponseJson.getString("link"));
				editor.putString("gender",
						facebookResponseJson.getString("gender"));
				editor.putString("local",
						facebookResponseJson.getString("locale"));
				editor.putString("timezone",
						facebookResponseJson.getString("timezone"));
				if (facebookResponseJson.has("location")) {
					JSONObject json_location = facebookResponseJson
							.getJSONObject("location");
					String location = json_location.getString("name");
					String[] location_of_user = new String[2];
					if (location.contains(","))
						location_of_user = location.split(",");
					else
						location_of_user[0] = location;
					location_of_user[1] = "";

					editor.putString("city", location_of_user[0]);
					editor.putString("state", location_of_user[1]);
				}
				editor.commit();
				System.out.println("response from server for facebook "
						+ facebookResponseJson);

				SessionStore.save(mFacebook, LoginRegister_.this);
				// SessionEvents.onLoginSuccess();

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FacebookError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// System.out.println("response from server for facebook "+json);

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd1.dismiss();
			DesignUtil.hideProgressDialog();
			afterLogin();
			CallLoginUsingFacebook();
		}
	}

	/*
	 * A service called to Login via Facebook.
	 */
	public class AsynchFBLogin extends AsyncTask<Void, Void, String> {
		String s;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// pd = ProgressDialog.show(LoginRegister_.this, "",
			// "Please Wait..");
			DesignUtil.showProgressDialog(LoginRegister_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				JSONObject facebookLoginjson = new JSONObject();
				try {
					facebookLoginjson.put("type", "facebook");
					facebookLoginjson.put(
							"access_token",
							getSharedPreferences(bu.KEY,
									Context.MODE_WORLD_WRITEABLE).getString(
									"access_token", "").toString());
					s = sc.sendJSONPost(
							BaseUrl.Development_Uri
									+ bu.getMethodName("LOGINFB")
									+ BaseUrl.Api_key, facebookLoginjson);

				} catch (JSONException e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return s;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd.dismiss();
			DesignUtil.hideProgressDialog();

			if (s != null) {
				try {
					JSONObject loginFBRespone = new JSONObject(result);
					bu.UserInfo = loginFBRespone;
					bu.USER_KEY = loginFBRespone.getString("user_key");
					Editor editor = getSharedPreferences(bu.KEY,
							Context.MODE_WORLD_WRITEABLE).edit();
					editor.putString("USER_KEY",
							loginFBRespone.getString("user_key"));
					editor.commit();
					JSONArray bookmarkedEventArray = loginFBRespone
							.getJSONArray("event_bookmarks");
					if (bookmarkedEventArray.length() > 0) {
						for (int i = 0; i < bookmarkedEventArray.length(); i++) {
							BaseUrl.bookmarkedEventId.add(bookmarkedEventArray
									.getString(i));
						}
					}
					int KEY_STATUS = loginFBRespone.getInt("key_status");
					int is_new = loginFBRespone.getInt("is_new");
					if (is_new == 1) {
						String InterestALL = loginFBRespone
								.getString("interests_all");
						String CommunitiesALL = loginFBRespone
								.getString("target_communities_all");
						Intent call_loginRegister = new Intent(
								LoginRegister_.this, Intersest_.class);
						call_loginRegister.putExtra("allInterest", InterestALL);
						call_loginRegister.putExtra("allCommunities",
								CommunitiesALL);
						startActivity(call_loginRegister);
						// finish();
					}
					// Put all the Communities,Neighborhoods and Selected
					// community Location at one place.
					bu.parentCommunityArray = new JSONArray();
					bu.parentCommunityArray = loginFBRespone
							.getJSONArray("communities_all");
					bu.neighborhoodCommunityAllArray = new JSONArray();
					bu.neighborhoodCommunityAllArray = loginFBRespone
							.getJSONArray("neighborhoods_all");
					bu.CommunityID = loginFBRespone.getString("community_id");
					bu.AllInterestArray = new JSONArray();
					bu.AllInterestArray = loginFBRespone
							.getJSONArray("interests_all");
					bu.AllOrganizationArray = new JSONArray();
					bu.AllOrganizationArray = loginFBRespone
							.getJSONArray("organizations_all");
					bu.AllCommunitiesArray = new JSONArray();
					bu.AllCommunitiesArray = loginFBRespone
							.getJSONArray("target_communities_all");
					// This will take all previously selected Interest and
					// communities
					bu.checkedINTEREST = new ArrayList<String>();
					JSONArray userSelectedInterest = loginFBRespone
							.getJSONArray("interests");
					if (userSelectedInterest.length() > 0) {
						for (int i = 0; i < userSelectedInterest.length(); i++) {
							bu.checkedINTEREST.add(userSelectedInterest
									.getString(i));
						}
					}
					bu.checkedCOMMUNITIES = new ArrayList<String>();
					JSONArray userSelectedCommunities = loginFBRespone
							.getJSONArray("target_communities");
					if (userSelectedCommunities.length() > 0) {
						for (int i = 0; i < userSelectedCommunities.length(); i++) {
							bu.checkedCOMMUNITIES.add(userSelectedCommunities
									.getString(i));
						}
					}
					// Toast.makeText(MainActivity.this, "Login Successful",
					// Toast.LENGTH_SHORT).show();
					int sponserId = Integer.valueOf(bu.CommunityID);
					getSharedPreferences(bu.KEY, Context.MODE_PRIVATE).edit()
							.putInt(Config.KEY_PREF_SPONSER_ID, sponserId)
							.commit();

					Intent i = new Intent(getApplicationContext(),
							Recommended_.class);
					startActivity(i);
					finish();
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(LoginRegister_.this, "Login Unsuccessful",
							Toast.LENGTH_SHORT).show();
					return;
				}
			} else {
				Toast.makeText(LoginRegister_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
		}

	}
}

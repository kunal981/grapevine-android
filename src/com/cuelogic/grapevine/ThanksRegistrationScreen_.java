package com.cuelogic.grapevine;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class ThanksRegistrationScreen_ extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration_done_);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent callLoginRegister = new Intent(ThanksRegistrationScreen_.this,
				LoginRegister_.class);
		startActivity(callLoginRegister);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}
}

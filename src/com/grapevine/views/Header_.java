package com.grapevine.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

import com.cuelogic.grapevine.R;

public class Header_ extends FrameLayout implements OnClickListener {
	private OnClickListener clickListener;
	private View btBack;
	private View btLogout;

	public Header_(Context context) {
		super(context);
		if (!isInEditMode())
			init();
	}

	public Header_(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode())
			init();
	}

	@Override
	public void onClick(View v) {
		if (clickListener != null)
			clickListener.onClick(v);

		switch (v.getId()) {
		case R.id.btBack:
			break;
		case R.id.btLogout:
			break;

		default:
			break;
		}
	}

	@Override
	public void setOnClickListener(OnClickListener l) {
		clickListener = l;
	}

	private void init() {
		LayoutInflater layoutInflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.header_, this);
		btBack = findViewById(R.id.btBack);
		btBack.setOnClickListener(this);
		btLogout = findViewById(R.id.btLogout);
		btLogout.setOnClickListener(this);
	}

	/**
	 * Configure header pane.
	 *
	 * @param isHome
	 *            the is home
	 * @param isBack
	 *            the is back
	 * @return the header
	 */
	public Header_ ConfigureHeaderPane(boolean isLogout, boolean isBack) {
		if (isLogout)
			btLogout.setVisibility(View.VISIBLE);
		else
			btLogout.setVisibility(View.GONE);

		if (isBack)
			btBack.setVisibility(View.VISIBLE);
		else
			btBack.setVisibility(View.GONE);

		return this;

	}
}

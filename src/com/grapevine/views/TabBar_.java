package com.grapevine.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;

import com.cuelogic.grapevine.R;
import com.socketconnection.BaseUrl;

public class TabBar_ extends FrameLayout implements OnClickListener {
	private OnClickListener clickListener;
	private Button recommendedIV, searchIV, myeventsIV, myprofileIV;
	private View recommendedLinearLayout;
	private View searchLinearLayout;
	private View myEventsLinearLayout;
	private View myProfileLinearLayout;
	private Context context;

	public TabBar_(Context context) {
		super(context);
		this.context = context;
		if (!isInEditMode())
			init();

	}

	public TabBar_(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		if (!isInEditMode())
			init();
	}

	@Override
	public void onClick(View v) {
		if (clickListener != null)
			clickListener.onClick(v);
		String s = context.getClass().toString();

		switch (v.getId()) {

		case R.id.tabRecommendedLinearLayout:
			if (s.contains("com.grapevine.myevent")) {
				BaseUrl.TAB3_LAST_ACTIVITY = context.getClass();
			} else if (s.contains("com.grapevine.myprofile")) {
				BaseUrl.TAB4_LAST_ACTIVITY = context.getClass();
			} else {
				BaseUrl.TAB2_LAST_ACTIVITY = context.getClass();
			}
			// BaseUrl.TAB1_LAST_ACTIVITY =
			// com.grapevine.recommended.Recommended_.class;
			Intent startRecommendScreen = new Intent(context,
					BaseUrl.TAB1_LAST_ACTIVITY);
			// startRecommendScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
			// | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			((Activity) context).startActivity(startRecommendScreen);
			((Activity) context).overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			((Activity) context).finish();
			break;
		case R.id.tabSearchLinearLayout:
			// if (s.contains("com.grapevine.myevent")) {
			// BaseUrl.TAB3_LAST_ACTIVITY = context.getClass();
			// } else if (s.contains("com.grapevine.myprofile")) {
			// BaseUrl.TAB4_LAST_ACTIVITY = context.getClass();
			// } else {
			// BaseUrl.TAB1_LAST_ACTIVITY = context.getClass();
			// }
			BaseUrl.TAB2_LAST_ACTIVITY = com.grapevine.search.SearchScreen_.class;
			Intent startSearchScreen = new Intent(context,
					BaseUrl.TAB2_LAST_ACTIVITY);
			((Activity) context).startActivity(startSearchScreen);
			((Activity) context).overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);

			((Activity) context).finish();
			// Toast.makeText(context, "Under development", Toast.LENGTH_SHORT)
			// .show();
			break;
		case R.id.tabMyEventsLinearLayout:
			if (s.contains("com.grapevine.recommended")) {
				BaseUrl.TAB1_LAST_ACTIVITY = context.getClass();
			} else if (s.contains("com.grapevine.myprofile")) {
				BaseUrl.TAB4_LAST_ACTIVITY = context.getClass();
			} else {
				BaseUrl.TAB2_LAST_ACTIVITY = context.getClass();
			}
			// BaseUrl.TAB3_LAST_ACTIVITY =
			// com.grapevine.myevent.MyEvents_.class;
			Intent startMyEventScreen = new Intent(context,
					BaseUrl.TAB3_LAST_ACTIVITY);
			// startMyEventScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
			// | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			((Activity) context).startActivity(startMyEventScreen);
			((Activity) context).overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			((Activity) context).finish();

			break;
		case R.id.tabMyProfileLinearLayout:
			// if (s.contains("com.grapevine.myevent")) {
			// BaseUrl.TAB3_LAST_ACTIVITY = context.getClass();
			// } else if (s.contains("com.grapevine.recommended")) {
			// BaseUrl.TAB1_LAST_ACTIVITY = context.getClass();
			// } else {
			// BaseUrl.TAB2_LAST_ACTIVITY = context.getClass();
			// }
			BaseUrl.TAB4_LAST_ACTIVITY = com.grapevine.myprofile.MyProfile_.class;
			Intent startMyProfileScreen = new Intent(context,
					BaseUrl.TAB4_LAST_ACTIVITY);
			startMyProfileScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			((Activity) context).startActivity(startMyProfileScreen);
			((Activity) context).overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			((Activity) context).finish();

			// Toast.makeText(context, "Under development", Toast.LENGTH_SHORT)
			// .show();
			break;
		}
	}

	@Override
	public void setOnClickListener(OnClickListener l) {
		clickListener = l;
	}

	private void init() {
		LayoutInflater layoutInflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.tab_bar_, this);
		recommendedLinearLayout = findViewById(R.id.tabRecommendedLinearLayout);
		recommendedIV = (Button) findViewById(R.id.recommnededIV);
		recommendedLinearLayout.setOnClickListener(this);
		searchLinearLayout = findViewById(R.id.tabSearchLinearLayout);
		searchIV = (Button) findViewById(R.id.searchIV);
		searchLinearLayout.setOnClickListener(this);
		myEventsLinearLayout = findViewById(R.id.tabMyEventsLinearLayout);
		myeventsIV = (Button) (findViewById(R.id.eventsIV));
		myEventsLinearLayout.setOnClickListener(this);
		myProfileLinearLayout = findViewById(R.id.tabMyProfileLinearLayout);
		myprofileIV = (Button) findViewById(R.id.profileIV);
		myProfileLinearLayout.setOnClickListener(this);
	}

	/**
	 * Configure Tabbar pane.
	 * 
	 * @return the header
	 */
	public TabBar_ ConfigureTabBarPane(boolean isRecommended, boolean isSearch,
			boolean isMyEvents, boolean isMyProfile) {

		if (isRecommended) {
			recommendedLinearLayout.setBackgroundColor(context.getResources()
					.getColor(R.color.bg_tab_purple_selected));
		} else {
			recommendedLinearLayout.setBackgroundColor(context.getResources()
					.getColor(R.color.bg_tab_purple));

		}
		if (isSearch) {
			searchLinearLayout.setBackgroundColor(context.getResources()
					.getColor(R.color.bg_tab_purple_selected));
		} else {
			searchLinearLayout.setBackgroundColor(context.getResources()
					.getColor(R.color.bg_tab_purple));
		}

		if (isMyEvents) {
			myEventsLinearLayout.setBackgroundColor(context.getResources()
					.getColor(R.color.bg_tab_purple_selected));
		} else {
			myEventsLinearLayout.setBackgroundColor(context.getResources()
					.getColor(R.color.bg_tab_purple));
		}

		if (isMyProfile) {
			myProfileLinearLayout.setBackgroundColor(context.getResources()
					.getColor(R.color.bg_tab_purple_selected));
		} else {
			myProfileLinearLayout.setBackgroundColor(context.getResources()
					.getColor(R.color.bg_tab_purple));
		}

		return this;

	}
}

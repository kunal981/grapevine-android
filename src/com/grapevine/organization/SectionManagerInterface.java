package com.grapevine.organization;


public abstract class SectionManagerInterface
{
		
	public abstract int getTotalSections();
	public abstract int getElementCountInSection(int section);
	public abstract Object getSection(int section);
	public abstract Object getElementOfSectionAtPosition(int section,int position);
	public abstract boolean configure();
	public abstract int getSectionByIndexIdentifier(Object identifier);
}

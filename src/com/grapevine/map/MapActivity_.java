package com.grapevine.map;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.cuelogic.grapevine.LocationChangeHandler;
import com.cuelogic.grapevine.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * The Class MapActivity.
 */
public class MapActivity_ extends FragmentActivity implements OnClickListener {

	/** The map view. */

	private GoogleMap map;


	Button getDirection, hideMap;
	String LAT, LONG;
	LatLng latLng;

	/* (non-Javadoc)
	* @see com.google.android.maps.MapActivity#onCreate(android.os.Bundle)
	*/
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.map_store_);
		turnGPSOn();
		LocationChangeHandler.setInstance(new LocationChangeHandler(
				getApplicationContext()));
		LocationChangeHandler.getInstance().beginUpdate();
		getDirection = (Button) findViewById(R.id.getDirectionButton);
		getDirection.setOnClickListener(this);
		hideMap = (Button) findViewById(R.id.hideMapButton);
		hideMap.setOnClickListener(this);
		Bundle bundle = getIntent().getExtras();
		LAT = bundle.getString("LATITUDE");
		LONG = bundle.getString("LONGITUDE");
		double lat = Double.parseDouble(LAT);
		double lng = Double.parseDouble(LONG);

		// LAT = "19.15382310";
		// LONG = "72.875178";
		latLng = new LatLng(lat, lng);
		Log.e("Location", latLng + "");
		map = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		map.addMarker(new MarkerOptions()
				.position(latLng)
				.title("Location")
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.pointer_big)));

		// Move the camera instantly to hamburg with a zoom of 15.
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));

		// Zoom in, animating the camera.
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,
				12);
		map.animateCamera(cameraUpdate);

	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.getDirectionButton:
			String currentLat,
			currentLong;
			if (LocationChangeHandler.getLocation() != null) {

				currentLat = LocationChangeHandler.getLocation().getLatitude()
						+ "";
				currentLong = LocationChangeHandler.getLocation()
						.getLongitude() + "";
				// currentLat = "35.74184050952353";
				// currentLong = "-118.8128372137487";

			} else {
				currentLat = "35.74184050952353";
				currentLong = "-118.8128372137487";
			}
			final Intent intent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://maps.google.com/maps?" + "saddr="
							+ currentLat + "," + currentLong + "" + "&daddr="
							+ LAT + "," + LONG + ""));
			intent.setClassName("com.google.android.apps.maps",
					"com.google.android.maps.MapsActivity");
			startActivity(intent);
			break;

		case R.id.hideMapButton:
			finish();
			break;
		default:
			break;
		}

	}

	/*
	* A function used to turn-on device GPS if it is off.
	*/
	public void turnGPSOn() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", true);
		this.sendBroadcast(intent);
		String provider = Settings.Secure.getString(this.getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) { // if gps is disabled

			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			this.sendBroadcast(poke);

		}
	}

	/*
	* A function used to turn-off device GPS.
	*/
	public void turnGPSOff() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", false);
		this.sendBroadcast(intent);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		LocationChangeHandler.getInstance().stopUpdates();
		turnGPSOff();
	}

}
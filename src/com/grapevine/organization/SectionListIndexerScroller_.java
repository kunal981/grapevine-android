package com.grapevine.organization;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cuelogic.grapevine.R;

public class SectionListIndexerScroller_ extends RelativeLayout implements
		OnTouchListener {
	private SectionIndexChangeInterface sectionIndexChangeInterface;

	public static interface SectionIndexChangeInterface {
		public boolean onSectionIndexChange(
				SectionListIndexerScroller_ sectionListIndexerScroller,
				int newIndex);
	}

	private int indexIndicatorSize = 100;
	private int fingerSliderSize = 50;
	private PopupWindow popupWindow;
	private LinearLayout alphabateWrap;
	private LinearLayout fingerSlider;
	private TextView txtIndexTitle;
	private String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#";

	private int oldSecIndex = 0;

	public SectionListIndexerScroller_(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		alphabateWrap = new LinearLayout(getContext());
		alphabateWrap.setOrientation(LinearLayout.VERTICAL);
		addView(alphabateWrap, new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

		indexIndicatorSize = this
				.getPXfromDIP(indexIndicatorSize, getContext());
		fingerSliderSize = this.getPXfromDIP(fingerSliderSize, getContext());

		fingerSlider = new LinearLayout(getContext());
		// fingerSlider.setBackgroundColor(Color.GREEN);
		fingerSlider.setBackgroundResource(R.drawable.drag);
		fingerSlider.getBackground().setAlpha(100);
		fingerSlider.setVisibility(GONE);
		addView(fingerSlider, new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, fingerSliderSize));

		if (isInEditMode())
			return;

		setOnTouchListener(this);

		for (int i = 0; i < sections.length(); i++) {
			TextView tView = new TextView(getContext());
			tView.setTextSize(9);
			tView.setTextColor(getResources().getColor(
					R.color.textColorExpandableList));
			// tView.setTextColor(getResources().getColor(R.drawable.drag));
			tView.setGravity(Gravity.CENTER);
			tView.setText(sections.charAt(i) + "");
			alphabateWrap.addView(tView, new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			showIndexIndicator((int) event.getY(), event);
			break;

		case MotionEvent.ACTION_MOVE:
			fingerSlider.setVisibility(View.VISIBLE);
			setIndexIndicator((int) event.getY());
			break;

		case MotionEvent.ACTION_UP:
			popupWindow.dismiss();
			fingerSlider.setVisibility(View.GONE);
			break;

		default:
			break;
		}

		return true;
	}

	private void setIndexIndicator(int yPointer) {
		if (yPointer > alphabateWrap.getBottom() || yPointer < 0)
			return;

		View container = (View) getParent();
		popupWindow.update(getLeft() - (popupWindow.getWidth()), yPointer
				+ container.getTop() + popupWindow.getWidth() / 2, -1, -1);

		int perSecHeight = alphabateWrap.getHeight() / sections.length();
		int newSecIndex = yPointer / perSecHeight;

		if (newSecIndex < 0)
			newSecIndex = 0;
		if (newSecIndex >= sections.length())
			newSecIndex = sections.length() - 1;

		RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) fingerSlider
				.getLayoutParams();
		layoutParams.topMargin = yPointer - layoutParams.height / 2;
		fingerSlider.setLayoutParams(layoutParams);
		txtIndexTitle.setText("" + sections.charAt(newSecIndex));

		if (oldSecIndex != newSecIndex) {
			sectionIndexChangeInterface.onSectionIndexChange(this, newSecIndex);
			oldSecIndex = newSecIndex;
		}

	}

	private void showIndexIndicator(int yPointer, MotionEvent event) {

		if (popupWindow == null) {
			popupWindow = new PopupWindow(this);
			popupWindow.setBackgroundDrawable(getContext().getResources()
					.getDrawable(android.R.drawable.alert_dark_frame));
			popupWindow.setHeight(indexIndicatorSize);
			popupWindow.setWidth(indexIndicatorSize);
			txtIndexTitle = new TextView(getContext());
			txtIndexTitle.setTextSize(indexIndicatorSize / 3);
			txtIndexTitle.setLayoutParams(new WindowManager.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			txtIndexTitle.setGravity(Gravity.CENTER);
			popupWindow.setContentView(txtIndexTitle);
		}
		View container = (View) getParent();
		popupWindow.showAtLocation(container, Gravity.LEFT | Gravity.TOP,
				container.getWidth() / 2 - indexIndicatorSize / 2, yPointer);
		fingerSlider.setVisibility(VISIBLE);

		setIndexIndicator(yPointer);
	}

	public Object getIdentifierOfSection(int index) {
		return sections.charAt(index);

	}

	public final SectionIndexChangeInterface getSectionIndexChangeInterface() {
		return sectionIndexChangeInterface;
	}

	public final void setSectionIndexChangeInterface(
			SectionIndexChangeInterface sectionIndexChangeInterface) {
		this.sectionIndexChangeInterface = sectionIndexChangeInterface;
	}

	public static final int getPXfromDIP(int dimInDip, Context ctx) {
		float d = ctx.getResources().getDisplayMetrics().density;
		return (int) (dimInDip * d);
	}

}

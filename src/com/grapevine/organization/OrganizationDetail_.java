package com.grapevine.organization;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.cuelogic.grapevine.R;
import com.grapevine.recommended.EventDetail_;
import com.grapevine.search.SearchResultAdapter_;
import com.grapevine.ui.SegmentedGroup;
import com.grapevine.util.DesignUtil;
import com.grapevine.views.BetterPopupWindow;
import com.grapevine.views.FacebookPost;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;
import com.twitter.android.TwitterDirectPost;

public class OrganizationDetail_ extends Activity implements OnClickListener,
		RadioGroup.OnCheckedChangeListener {

	// private ToggleButton eventProfileToggle, soonestClosetToggle;
	private LinearLayout profileLayoutContainer, eventLayoutContainer,
			eventContactContainer, contactLayout, detailLayout, myEventLayout,
			shareLayout, noItemFoundLinear;
	private TextView organizationNameTitle, organizationNameTv, addressLine1Tv,
			stateTv, organizationDetailsTv, addressLine1Contact,
			addressLine2Contact;
	private TextView contactTitle, contactName, contactEmail, contactPhone;
	private ImageView organizationIv, organizationEventIv;
	private ProgressDialog pd, pd2;
	private Button btn_details, btn_events, btn_contact, btn_share;
	SocketConnection sc;
	BaseUrl bu;
	String fetchOrganizationId, from;
	String ImageUrl;
	ArrayList<HashMap<String, String>> organizationEventList;
	private LayoutInflater mInflater;
	SearchResultAdapter_ searchResultAdapter;
	private ListView organizationEventListView;
	// private LinearLayout profileTab, eventTab, mainTab;
	Bitmap bm;

	DisplayImageOptions options;
	SegmentedGroup segmentedGroup;
	RadioButton radioButtonCurrent, radioButtonPast;
	String webSiteURLString, facebookUrlString, twitterUrlString;
	Button buttonWeb, buttonFb, buttonTwit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.organization_);
		initImageLoader();
		// ((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
		// .ConfigureTabBarPane(false, true, false, false);
		findViewById(R.id.btBack).setOnClickListener(this);

		profileLayoutContainer = (LinearLayout) findViewById(R.id.profileLayout);
		eventLayoutContainer = (LinearLayout) findViewById(R.id.eventLayout);
		eventContactContainer = (LinearLayout) findViewById(R.id.eventContact);
		profileLayoutContainer.setVisibility(View.VISIBLE);
		eventLayoutContainer.setVisibility(View.GONE);
		eventContactContainer.setVisibility(View.GONE);
		shareLayout = (LinearLayout) findViewById(R.id.shareLinearLayout);
		shareLayout.setOnClickListener(this);
		contactLayout = (LinearLayout) findViewById(R.id.contactLinearLayout);
		contactLayout.setOnClickListener(this);
		detailLayout = (LinearLayout) findViewById(R.id.detailLinearLayout);
		detailLayout.setSelected(true);
		detailLayout.setOnClickListener(this);
		myEventLayout = (LinearLayout) findViewById(R.id.myEventLinearLayout);
		myEventLayout.setOnClickListener(this);
		btn_details = (Button) findViewById(R.id.recommnededIV);
		btn_events = (Button) findViewById(R.id.searchIV);
		btn_contact = (Button) findViewById(R.id.eventsIV);
		btn_share = (Button) findViewById(R.id.profileIV);

		organizationNameTitle = (TextView) findViewById(R.id.organizationNameTitle);
		organizationNameTv = (TextView) findViewById(R.id.organizationNameTv);
		addressLine1Tv = (TextView) findViewById(R.id.addressLineTV);
		addressLine1Contact = (TextView) findViewById(R.id.txt_address);
		addressLine2Contact = (TextView) findViewById(R.id.txt_streetaddress);
		stateTv = (TextView) findViewById(R.id.stateTV);
		organizationDetailsTv = (TextView) findViewById(R.id.organizationDetailsTv);
		organizationIv = (ImageView) findViewById(R.id.organizationIV);
		contactTitle = (TextView) findViewById(R.id.txt_title);
		contactName = (TextView) findViewById(R.id.organizationContactPersonTv);
		contactEmail = (TextView) findViewById(R.id.organizationContactEmailTv);
		contactPhone = (TextView) findViewById(R.id.organizationContactPhoneTv);

		segmentedGroup = (SegmentedGroup) findViewById(R.id.segmented_button);
		segmentedGroup.setTintColor(getResources().getColor(
				R.color.radio_button_selected_color));
		segmentedGroup.setOnCheckedChangeListener(this);
		radioButtonCurrent = (RadioButton) findViewById(R.id.btn_current);
		radioButtonPast = (RadioButton) findViewById(R.id.btn_past);

		buttonFb = (Button) findViewById(R.id.btn_facebook);
		buttonTwit = (Button) findViewById(R.id.btn_twitter);
		buttonWeb = (Button) findViewById(R.id.btn_user);
		buttonFb.setOnClickListener(this);
		buttonTwit.setOnClickListener(this);
		buttonWeb.setOnClickListener(this);

		sc = new SocketConnection(this);
		bu = new BaseUrl();
		Intent getIntent = getIntent();
		fetchOrganizationId = getIntent.getStringExtra("organizationId");
		from = getIntent.getStringExtra("from");
		String organizationName = getIntent.getStringExtra("organizationName");
		organizationNameTitle.setText(organizationName);
		new AsynchOrganizationDetails(fetchOrganizationId).execute();
		// Below data is to load All events Of the specific organization
		mInflater = LayoutInflater.from(OrganizationDetail_.this);
		View footerView = mInflater.inflate(R.layout.footer_, null);
		noItemFoundLinear = (LinearLayout) findViewById(R.id.NoSearchFoundLayout);
		noItemFoundLinear.setVisibility(View.GONE);
		organizationEventListView = (ListView) findViewById(R.id.organizationEventListview);
		organizationEventListView.addFooterView(footerView);
		organizationEventList = new ArrayList<HashMap<String, String>>();
		searchResultAdapter = new SearchResultAdapter_(this,
				organizationEventList);
		new AsynchOrganizationEvent().execute();
		// findViewById(R.id.tabSearchLinearLayout).setOnClickListener(
		// new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// Intent startSearchScreen = new Intent(
		// OrganizationDetail_.this, SearchScreen.class);
		//
		// startActivity(startSearchScreen);
		// overridePendingTransition(R.anim.slide_in_left,
		// R.anim.slide_out_right);
		// finish();
		// }
		// });
	}

	private void initImageLoader() {
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.no_image_icon)
				.showImageOnFail(R.drawable.no_image_icon).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1)).build();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_user:
			if (webSiteURLString != null && !webSiteURLString.trim().equals("")) {
				Intent browserIntent1 = new Intent(Intent.ACTION_VIEW,
						Uri.parse(webSiteURLString.trim()));
				startActivity(browserIntent1);
			} else {
				Toast.makeText(this, "No URL matches", Toast.LENGTH_SHORT)
						.show();
			}
			break;
		case R.id.btn_twitter:
			if (twitterUrlString != null && !twitterUrlString.trim().equals("")) {
				Intent browserIntent1 = new Intent(Intent.ACTION_VIEW,
						Uri.parse(twitterUrlString.trim()));
				startActivity(browserIntent1);
			} else {
				Toast.makeText(this, "No URL matches", Toast.LENGTH_SHORT)
						.show();
			}
			break;
		case R.id.btn_facebook:
			if (facebookUrlString != null
					&& !facebookUrlString.trim().equals("")) {
				Intent browserIntent1 = new Intent(Intent.ACTION_VIEW,
						Uri.parse(facebookUrlString.trim()));
				startActivity(browserIntent1);
			} else {
				Toast.makeText(this, "No URL matches", Toast.LENGTH_SHORT)
						.show();
			}
			break;
		case R.id.shareLinearLayout:
			DemoPopupWindow dw = new DemoPopupWindow(v,
					OrganizationDetail_.this);
			dw.showLikeQuickAction(0, 0);
			break;
		case R.id.contactLinearLayout:
			if (!contactLayout.isSelected()) {
				profileLayoutContainer.setVisibility(View.GONE);
				eventLayoutContainer.setVisibility(View.GONE);
				eventContactContainer.setVisibility(View.VISIBLE);
				detailLayout.setSelected(false);
				myEventLayout.setSelected(false);
				contactLayout.setSelected(true);
			}
			break;
		case R.id.detailLinearLayout:
			if (!detailLayout.isSelected()) {
				profileLayoutContainer.setVisibility(View.VISIBLE);
				eventLayoutContainer.setVisibility(View.GONE);
				eventContactContainer.setVisibility(View.GONE);
				detailLayout.setSelected(true);
				myEventLayout.setSelected(false);
				contactLayout.setSelected(false);
			}
			break;
		case R.id.myEventLinearLayout:
			if (!myEventLayout.isSelected()) {
				profileLayoutContainer.setVisibility(View.GONE);
				eventLayoutContainer.setVisibility(View.VISIBLE);
				eventContactContainer.setVisibility(View.GONE);
				detailLayout.setSelected(false);
				myEventLayout.setSelected(true);
				contactLayout.setSelected(false);
			}
			break;

		case R.id.btBack:
			// if (from.equalsIgnoreCase("SearchOrganization")) {
			// Intent i = new Intent(this, SearchOrganization.class);
			// startActivity(i);
			// } else if (from.equalsIgnoreCase("SearchEventDetail")) {
			// Intent i = new Intent(this, SearchEventDetails.class);
			// startActivity(i);
			// } else if (from.equalsIgnoreCase("EventDetail")) {
			// Intent i = new Intent(this, EventDetail_.class);
			// startActivity(i);
			// } else if (from.equalsIgnoreCase("MyEventDetail")) {
			// Intent i = new Intent(this, MyEventDetail.class);
			// startActivity(i);
			// }
			// overridePendingTransition(R.anim.slide_in_left,
			// R.anim.slide_out_right);
			// finish();
			onBackPressed();
			break;
		default:
			break;
		}
	}

	public class AsynchOrganizationDetails extends
			AsyncTask<Void, Void, String> {
		String Json_to_parse, organizationID;

		public AsynchOrganizationDetails(String id) {
			this.organizationID = id;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// pd = ProgressDialog.show(OrganizationDetail_.this, "",
			// "Please Wait..");
			DesignUtil.showProgressDialog(OrganizationDetail_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			InputStream ip;
			try {
				ip = sc.getHttpStream(BaseUrl.Development_Uri
						+ bu.getMethodName("ORGANIZATIONS") + organizationID
						+ bu.Api_key);
				Json_to_parse = sc.convertStreamToString(ip);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return Json_to_parse;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd.dismiss();
			DesignUtil.hideProgressDialog();
			if (result != null) {
				try {
					JSONObject organizationDetail = new JSONObject(result);
					organizationNameTitle.setText(organizationDetail
							.getString("name"));
					organizationNameTv.setText(organizationDetail
							.getString("name"));
					addressLine1Tv.setText(organizationDetail
							.getString("address_line_1"));
					addressLine1Contact.setText(organizationDetail
							.getString("address_line_1"));
					stateTv.setText(organizationDetail.getString("city") + ", "
							+ organizationDetail.getString("state") + " "
							+ organizationDetail.getString("postalcode"));
					addressLine2Contact.setText(organizationDetail
							.getString("city")
							+ ", "
							+ organizationDetail.getString("state")
							+ " "
							+ organizationDetail.getString("postalcode"));

					if (!organizationDetail.getString("website_url").toString()
							.equalsIgnoreCase("null"))
						webSiteURLString = organizationDetail
								.getString("website_url");
					if (!organizationDetail.getString("facebook_url")
							.toString().equalsIgnoreCase("null"))
						facebookUrlString = organizationDetail
								.getString("facebook_url");
					if (!organizationDetail.getString("twitter_hashtag")
							.toString().equalsIgnoreCase("null"))
						twitterUrlString = String
								.valueOf("http://www.twitter.com/"
										+ organizationDetail
												.getString("twitter_hashtag"));

					organizationDetailsTv.setText(organizationDetail
							.getString("description"));
					contactName.setText(organizationDetail.getString("name"));
					contactTitle.setText(organizationDetail.getString("name"));
					contactEmail.setText(organizationDetail.getString("email"));
					contactPhone.setText(organizationDetail.getString("phone"));
					ImageUrl = organizationDetail.getString("logo_url");
					// new AsynchOrganizationImage(ImageUrl).execute();

					ImageLoader.getInstance().displayImage(ImageUrl,
							organizationIv, options);
				} catch (Exception e) {
					// TODO: handle exception
				}
			} else {
				Toast.makeText(OrganizationDetail_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public class AsynchOrganizationEvent extends AsyncTask<Void, Void, String> {
		String s, Json_to_parse;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// pd2 = ProgressDialog.show(OrganizationDetail_.this, "",
			// "Please Wait..");
			DesignUtil.showProgressDialog(OrganizationDetail_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			// if(bu.RecommendedEvents.length()>0){
			// PutEventinHashmap(organizationEventList,bu.RecommendedEvents);
			// }
			// if(bu.FeaturedEvents.length()>0){
			// PutEventinHashmap(organizationEventList,bu.FeaturedEvents);
			// }
			// return null;
			InputStream ip;
			try {
				ip = sc.getHttpStream(BaseUrl.Development_Uri + "/users/"
						+ bu.USER_KEY + bu.getMethodName("EVENTS") + bu.Api_key
						+ "&type=all&filter_organization_id="
						+ fetchOrganizationId);
				Json_to_parse = sc.convertStreamToString(ip);
				parseJsonforOrgnizationEvents(Json_to_parse);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return Json_to_parse;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd2.dismiss();
			DesignUtil.hideProgressDialog();
			HashSet hs = new HashSet();
			hs.addAll(organizationEventList);
			organizationEventList.clear();
			organizationEventList.addAll(hs);
			setListView(true);
		}

	}

	public void parseJsonforOrgnizationEvents(String jsonString) {
		try {
			JSONObject recommendedJsonObject = new JSONObject(jsonString);
			JSONArray orgEventArray = recommendedJsonObject.getJSONArray("all");
			if (orgEventArray.length() > 0)
				PutEventinHashmap(organizationEventList, orgEventArray);

		} catch (JSONException e) {

		} catch (Exception e) {
		}
	}

	public void PutEventinHashmap(
			ArrayList<HashMap<String, String>> arrayToAdd, JSONArray jsonarray) {
		for (int i = 0; i < jsonarray.length(); i++) {
			try {
				JSONObject arrayElement = jsonarray.getJSONObject(i);
				JSONObject orgnazationObj = arrayElement
						.getJSONObject("organization");
				if (orgnazationObj.getString("id").equalsIgnoreCase(
						fetchOrganizationId)) {
					HashMap<String, String> hash = new HashMap<String, String>();
					hash.put("orgnization", orgnazationObj.getString("name"));
					hash.put("orgnizationid", orgnazationObj.getString("id"));
					hash.put("address_line_1",
							arrayElement.getString("address_line_1"));
					hash.put("city", arrayElement.getString("city"));
					hash.put("description",
							arrayElement.getString("description"));
					hash.put("distance", arrayElement.getString("distance"));
					hash.put("end_time", arrayElement.getString("end_time"));
					// hash.put("start_time",
					// arrayElement.getString("start_time"));
					hash.put("start_time",
							arrayElement.getString("start_time_local"));
					hash.put("event_url", arrayElement.getString("event_url"));
					hash.put("public_url", arrayElement.getString("public_url"));
					hash.put("id", arrayElement.getString("id"));
					hash.put("image_url", arrayElement.getString("image_url"));
					hash.put("latitude", arrayElement.getString("latitude"));
					hash.put("location", arrayElement.getString("location"));
					hash.put("longitude", arrayElement.getString("longitude"));
					hash.put("postalcode", arrayElement.getString("postalcode"));
					hash.put("registration_deadline",
							arrayElement.getString("registration_deadline"));
					hash.put("registration_url",
							arrayElement.getString("registration_url"));
					hash.put("state", arrayElement.getString("state"));
					hash.put("teaser", arrayElement.getString("teaser"));
					hash.put("title", arrayElement.getString("title"));
					hash.put("user_feedback",
							arrayElement.getString("user_feedback"));
					hash.put("user_like", arrayElement.getString("user_like"));
					hash.put("user_not_like",
							arrayElement.getString("user_not_like"));
					hash.put("user_registered",
							arrayElement.getString("user_registered"));
					hash.put("user_score", arrayElement.getString("user_score"));
					arrayToAdd.add(hash);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void setListView(boolean flag) {
		if (organizationEventList.size() == 0) {
			noItemFoundLinear.setVisibility(View.VISIBLE);
			organizationEventListView.setVisibility(View.GONE);
		} else {
			organizationEventListView.setVisibility(View.VISIBLE);
			if (flag) {
				Collections.sort(organizationEventList,
						new Comparator<HashMap<String, String>>() {
							@Override
							public int compare(HashMap<String, String> lhs,
									HashMap<String, String> rhs) {
								if (lhs.get("start_time").length() > 0
										&& rhs.get("start_time").length() > 0) {
									if (Integer.parseInt(lhs.get("start_time")) < Integer
											.parseInt(rhs.get("start_time"))) {
										return -1;
									}
								}
								return 0;

							}
						});
			} else {
				Collections.sort(organizationEventList,
						new Comparator<HashMap<String, String>>() {
							@Override
							public int compare(HashMap<String, String> lhs,
									HashMap<String, String> rhs) {
								if (lhs.get("distance").length() > 0
										&& rhs.get("distance").length() > 0
										&& !lhs.get("distance")
												.contains("null")
										&& !rhs.get("distance")
												.contains("null")) {
									if (Integer.parseInt(lhs.get("distance")) < Integer
											.parseInt(rhs.get("distance"))) {
										return -1;
									}
								}
								return 0;

							}
						});
			}

			organizationEventListView.setAdapter(searchResultAdapter);
			organizationEventListView.postInvalidate();
			searchResultAdapter.notifyDataSetChanged();
			noItemFoundLinear.setVisibility(View.GONE);
		}
	}

	// To get the Image of Organization
	private static final int BUFFER_IO_SIZE = 8000;

	private Bitmap loadImageFromUrl(final String url) {
		try {
			BufferedInputStream bis = new BufferedInputStream(
					new URL(url).openStream(), BUFFER_IO_SIZE);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			BufferedOutputStream bos = new BufferedOutputStream(baos,
					BUFFER_IO_SIZE);
			copy(bis, bos);
			bos.flush();
			bis.close();
			bm = BitmapFactory.decodeByteArray(baos.toByteArray(), 0,
					baos.size());
			baos.flush();

			return bm;
		} catch (IOException e) {
			// handle it properly
		}
		return null;
	}

	private void copy(final InputStream bis, final OutputStream baos)
			throws IOException {
		byte[] buf = new byte[256];
		int l;
		while ((l = bis.read(buf)) >= 0)
			baos.write(buf, 0, l);
	}

	public class AsynchOrganizationImage extends AsyncTask<Void, Void, Bitmap> {
		String organizationUrl;

		public AsynchOrganizationImage(String url) {
			this.organizationUrl = url;
		}

		@Override
		protected Bitmap doInBackground(Void... params) {
			return loadImageFromUrl(organizationUrl);
		}

		@Override
		protected void onPostExecute(Bitmap bm) {
			// TODO Auto-generated method stub
			super.onPostExecute(bm);
			// organizationIv.setImageBitmap(bm);
			// organizationEventIv.setImageBitmap(bm);
			bm = null;
		}

	}

	// ************** Class for pop-up window **********************
	/**
	 * The Class DemoPopupWindow.
	 */
	private class DemoPopupWindow extends BetterPopupWindow implements
			OnClickListener {

		/**
		 * Instantiates a new demo popup window.
		 *
		 * @param anchor
		 *            the anchor
		 * @param cnt
		 *            the cnt
		 */
		public DemoPopupWindow(View anchor, Context cnt) {
			super(anchor);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cellalert24.Views.BetterPopupWindow#onCreate()
		 */
		@Override
		protected void onCreate() {
			// inflate layout
			LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			ViewGroup root = (ViewGroup) inflater.inflate(
					R.layout.share_choose_popup, null);

			// setup button events
			for (int i = 0, icount = root.getChildCount(); i < icount; i++) {
				View v = root.getChildAt(i);

				if (v instanceof TableRow) {
					TableRow row = (TableRow) v;

					for (int j = 0, jcount = row.getChildCount(); j < jcount; j++) {
						View item = row.getChildAt(j);
						if (item instanceof Button) {
							Button b = (Button) item;
							b.setOnClickListener(this);
						}
					}
				}
			}

			this.setContentView(root);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			this.dismiss();
			Intent intent;
			switch (v.getId()) {
			case R.id.shareFacebookBtn:
				FacebookPost fb = new FacebookPost(OrganizationDetail_.this,
						ImageUrl, organizationNameTv.getText().toString(),
						organizationDetailsTv.getText().toString());
				break;

			case R.id.shareTwitterBtn:
				new TwitterDirectPost(OrganizationDetail_.this,
						webSiteURLString);
				break;

			case R.id.shareEmailBtn:
				String body = "<div style=\"margin:10px auto;padding:10px;border:2px solid #ccc; font-family:Arial;font-size:12px;\"><div style=\"margin-bottom: 10px; padding-bottom: 7px;\"><img src=\"http://www.grape-vine.com/images/logo.png\" alt=\"GrapeVine\" /></div><div style=\"background-color:#f6f6f6;padding:10px;moz-border:5px;border:1px solid #e5e5e5;\"><br /> Hi,<br /><br />I thought you might like this link I found on <a href=\"http://www.grape-vine.com\">GrapeVine</a>. <a href=\"%@\" target=\"_blank\">Click here</a> to check it out!<br /><br/>Or, find out what else they've heard you might like at <a href=\"http://www.grape-vine.com\">GrapeVine:</a> Your trusted source for recommended events in your community!<br /><br />Sincerely,<br /><br/><br/>GrapeVine is currently being offered in Rhode Island, Columbus, OH, and New York City. If you want more information about bringing GrapeVine to a city near you,please email us at <a href=\"mailto:info@grape-vine.com\">info@grape-vine.com</a></div></div>";
				Intent intent1 = new Intent(Intent.ACTION_SENDTO); // it's not
																	// ACTION_SEND
				intent1.setType("text/html");
				intent1.putExtra(Intent.EXTRA_SUBJECT,
						"Check out this organization from Grape-vine");
				intent1.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(body));
				intent1.setData(Uri.parse("mailto:")); // or just "mailto:" for
														// blank
				intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will
																	// make such
																	// that when
																	// user
																	// returns
																	// to your
																	// app, your
																	// app is
																	// displayed,
																	// instead
																	// of the
																	// email
																	// app.
				startActivity(intent1);
				break;

			case R.id.cancelBtn:
				dismiss();
				break;
			}
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (from.equalsIgnoreCase("SearchOrganization")) {
			Intent i = new Intent(this, SearchOrganization_.class);
			startActivity(i);
		} else if (from.equalsIgnoreCase("SearchEventDetail")) {
			Intent i = new Intent(this, EventDetail_.class);
			startActivity(i);
		} else if (from.equalsIgnoreCase("EventDetail")) {
			Intent i = new Intent(this, EventDetail_.class);
			startActivity(i);
		} else if (from.equalsIgnoreCase("MyEventDetail")) {
			Intent i = new Intent(this, EventDetail_.class);
			startActivity(i);
		}
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.btn_current:

			setListView(true);
			return;
		case R.id.btn_past:
			setListView(false);
			return;

		}

	}
}

package com.cuelogic.grapevine;
/* A class which is use to get Location Updates.
 * @author Cuelogic Technologies
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
public class LocationChangeHandler implements LocationListener {
	
	/** The Constant LOCATION_PREF_FILE. */
	private static final String LOCATION_PREF_FILE = "locationPref";
	
	/** The location manager. */
	private static LocationManager locationManager;
	
	/** The location. */
	private static Location location;
	
	/** The context. */
	private static Context context;
	
	/** The location change handler. */
	private static LocationChangeHandler locationChangeHandler;
	
	
	  
	/**
	 * Instantiates a new location change handler.
	 *
	 * @param context the context
	 */
	public LocationChangeHandler(Context context) {
		super();
		LocationChangeHandler.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); 
        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}
	
	/**
	 * Gets the single instance of LocationChangeHandler.
	 *
	 * @return single instance of LocationChangeHandler
	 */
	public static LocationChangeHandler getInstance()
	{
		return locationChangeHandler;
	}
	
	/**
	 * Sets the instance.
	 *
	 * @param locationChangeHandler the new instance
	 */
	public static void setInstance(
			LocationChangeHandler locationChangeHandler)
	{
		LocationChangeHandler.locationChangeHandler = locationChangeHandler;
	}


	/**
	 * Begin update.
	 */
	public void beginUpdate() {
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	}
	
	/**
	 * Stop updates.
	 */
	public void stopUpdates() {
		locationManager.removeUpdates(this);
	}

	public void onLocationChanged(Location location) {
		 updateWithNewLocation(location);
	}
	
	public void onProviderDisabled(String provider) {
		//Toast.makeText(context, provider + " is Disabled", Toast.LENGTH_SHORT).show();
	}
	
	public void onProviderEnabled(String provider) {
		//Toast.makeText(context, provider + " is Enable", Toast.LENGTH_SHORT).show();
	}    
	
	public void onStatusChanged(String provider, int status, Bundle extras) {
		//Toast.makeText(context, provider, Toast.LENGTH_SHORT).show();
	}    
	
	/**
	 * Update with new location.
	 *
	 * @param location the location
	 */
	private void updateWithNewLocation(Location location)    
	{
	   if (location != null) 
	   {
		   LocationChangeHandler.location = location;
	 	   saveLocationInPrefPreferences(location);
	   }
	     
	}

	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public static Location getLocation() {
		
		   if(location == null)   
			{
	          String provider = locationManager.getBestProvider(new Criteria(),true); 
	          if(provider != null)
	        	  location = locationManager.getLastKnownLocation( provider ); 
	          
	          if(location == null)
	          {
	        	  location = getLocationFromPrefPreferences();
	          }
			 }
		return location;
	}
	

	   
  /**
   * Save location in pref preferences.
   *
   * @param location the location
   */
  private void saveLocationInPrefPreferences(Location location)
	   {
	    	SharedPreferences sharedPreferences = context.getSharedPreferences(LocationChangeHandler.LOCATION_PREF_FILE, Context.MODE_PRIVATE);
	    	Editor editor = sharedPreferences.edit();
	    	editor.putString("lat", location.getLatitude() + ""); 
	    	editor.putString("lng", location.getLongitude() + "");
	    	editor.commit();
	   }
	  
   /**
    * Gets the location from pref preferences.
    *
    * @return the location from pref preferences
    */
   private static Location getLocationFromPrefPreferences()
	   {
	   SharedPreferences sharedPreferences = context.getSharedPreferences(LocationChangeHandler.LOCATION_PREF_FILE, Context.MODE_PRIVATE);
	    String s_lat  = sharedPreferences.getString("lat","0");
	    String s_lng  = sharedPreferences.getString("lng","0"); 
	    
	    if( !s_lat.equals("0") && !s_lng.equals("0") )
	    {
		    Location _location = new Location("");
		    _location.setLatitude(Double.parseDouble(s_lat));
		    _location.setLongitude(Double.parseDouble(s_lng));
		    return _location;
	    }
	    return null;
	   }
		
   
   
	
}
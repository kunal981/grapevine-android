/*
 * Copyright 2010 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.android;

// TODO: Auto-generated Javadoc
/**
 * Encapsulation of a Facebook Error: a Facebook request that could not be 
 * fulfilled.
 * 
 * @author ssoneff@facebook.com
 */
public class FacebookError extends Throwable {
  
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The m error code. */
    private int mErrorCode = 0;
    
    /** The m error type. */
    private String mErrorType;
    
    /**
     * Instantiates a new facebook error.
     *
     * @param message the message
     */
    public FacebookError(String message) {
        super(message);
    }

    /**
     * Instantiates a new facebook error.
     *
     * @param message the message
     * @param type the type
     * @param code the code
     */
    public FacebookError(String message, String type, int code) {
        super(message);
        mErrorType = type;
        mErrorCode = code;
    }
    
    /**
     * Gets the error code.
     *
     * @return the error code
     */
    public int getErrorCode() {
        return mErrorCode;
    }
    
    /**
     * Gets the error type.
     *
     * @return the error type
     */
    public String getErrorType() {
        return mErrorType;
    }
}

package com.grapevine.organization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.cuelogic.grapevine.R;
import com.cuelogic.grapevine.R.color;

public class OrganizationListAdapter_ extends BaseExpandableListAdapter {
	public final class OrganizationSectionManager extends
			SectionManagerInterface {

		public class Section {
			public int sectionId;
			public String sectionHeader;
			public Object sectionIdentifier;
			public ArrayList<HashMap<String, String>> socialContactsInSection = new ArrayList<HashMap<String, String>>();
		}

		private int sectionBy;
		private ArrayList<HashMap<String, String>> organizationArrayList;
		private HashMap<Integer, Section> position_section_map = new HashMap<Integer, Section>();

		public OrganizationSectionManager(
				ArrayList<HashMap<String, String>> organizationArrayList) {
			super();
			this.organizationArrayList = organizationArrayList;
			configure();
		}

		public boolean configure() {

			if (organizationArrayList != null
					&& organizationArrayList.size() != 0) {

				Collections.sort(organizationArrayList,
						new Comparator<HashMap<String, String>>() {
							@Override
							public int compare(HashMap<String, String> lhs,
									HashMap<String, String> rhs) {
								String LHS = lhs.get("value").toUpperCase();
								String RHS = rhs.get("value").toUpperCase();
								return LHS.compareToIgnoreCase(RHS);
							}
						});

				Section otherSection = null;
				Section section = null;

				if (!Character.isLetter(organizationArrayList.get(0)
						.get("value").charAt(0))) {
					otherSection = new Section();
					otherSection.socialContactsInSection
							.add(organizationArrayList.get(0));
					otherSection.sectionIdentifier = "#";
					otherSection.sectionHeader = "#";
				} else {
					section = new Section();
					section.socialContactsInSection.add(organizationArrayList
							.get(0));
					section.sectionId = position_section_map.size();
					section.sectionIdentifier = organizationArrayList.get(0)
							.get("value").charAt(0);
					section.sectionHeader = organizationArrayList.get(0)
							.get("value").charAt(0)
							+ "";
					position_section_map.put(section.sectionId, section);
				}

				for (int i = 1; i < organizationArrayList.size(); i++) {

					if (!Character.isLetter(organizationArrayList.get(i)
							.get("value").charAt(0))) {
						if (otherSection == null) {
							otherSection = new Section();
							otherSection.socialContactsInSection
									.add(organizationArrayList.get(i));
							otherSection.sectionIdentifier = "#";
							otherSection.sectionHeader = "#";
						} else
							otherSection.socialContactsInSection
									.add(organizationArrayList.get(i));

						continue;
					}

					if (organizationArrayList.get(i).get("value").charAt(0) != organizationArrayList
							.get(i - 1).get("value").charAt(0)) {

						boolean isNewSection = true;
						for (Iterator iterator = position_section_map.values()
								.iterator(); iterator.hasNext();) {
							Section isection = (Section) iterator.next();
							if (isection.sectionIdentifier
									.equals(organizationArrayList.get(i)
											.get("value").charAt(0))) {
								isection.socialContactsInSection
										.add(organizationArrayList.get(i));
								isNewSection = false;
								break;
							}
						}

						if (isNewSection) {
							section = new Section();
							section.socialContactsInSection
									.add(organizationArrayList.get(i));
							section.sectionId = position_section_map.size();
							section.sectionIdentifier = organizationArrayList
									.get(i).get("value").charAt(0);
							section.sectionHeader = organizationArrayList
									.get(i).get("value").charAt(0)
									+ "";
							position_section_map
									.put(section.sectionId, section);
						}
					} else {
						section.socialContactsInSection
								.add(organizationArrayList.get(i));
					}
				}

				if (otherSection != null) {
					otherSection.sectionId = position_section_map.size();
					position_section_map.put(otherSection.sectionId,
							otherSection);
				}
			}

			return true;

		}

		public int getTotalSections() {
			return position_section_map.size();
		}

		public int getElementCountInSection(int section) {
			return position_section_map.get(section).socialContactsInSection
					.size();
		}

		public Object getSection(int section) {
			return position_section_map.get(section);
		}

		public Object getElementOfSectionAtPosition(int section, int position) {
			return position_section_map.get(section).socialContactsInSection
					.get(position);
		}

		public int getSectionByIndexIdentifier(Object identifier) {

			int i = -1;
			for (Iterator iterator = position_section_map.values().iterator(); iterator
					.hasNext();) {
				i++;
				Section isection = (Section) iterator.next();

				String strIdentifier = isection.sectionIdentifier + "";
				String strToMatch = identifier + "";

				if (strIdentifier.equalsIgnoreCase(strToMatch))
					return i;

			}

			return -1;
		}

	}

	private OrganizationListAdapter_.OrganizationSectionManager sectionManagerAdapter;
	private Context context;

	public OrganizationListAdapter_(
			ArrayList<HashMap<String, String>> organizationArrayList,
			Context context) {
		super();
		this.context = context;
		this.sectionManagerAdapter = new OrganizationSectionManager(
				organizationArrayList);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {

		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {

		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, final ViewGroup parent) {
		final HashMap<String, String> organizationHashmap = (HashMap<String, String>) sectionManagerAdapter
				.getElementOfSectionAtPosition(groupPosition, childPosition);

		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewGroup vg = (ViewGroup) layoutInflater.inflate(
				R.layout.search_organization_list_item_, null);
		TextView textview = (TextView) vg.findViewById(R.id.organizationNameTV);

		textview.setText(organizationHashmap.get("value"));

		vg.setTag(organizationHashmap.get("id"));
		// vg.setTag(organizationHashmap);

		return vg;

	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return sectionManagerAdapter.getElementCountInSection(groupPosition);
	}

	@Override
	public OrganizationListAdapter_.OrganizationSectionManager.Section getGroup(
			int groupPosition) {

		return (OrganizationListAdapter_.OrganizationSectionManager.Section) sectionManagerAdapter
				.getSection(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return sectionManagerAdapter.getTotalSections();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TextView textView = (TextView) layoutInflater.inflate(
				R.layout.interest_communities_header_, null);
		textView.setText(getGroup(groupPosition).sectionHeader);
		textView.setPadding(0, 10, 10, 10);
		// TextView textView = new TextView(context);
		// textView.setText(getGroup(groupPosition).sectionHeader);
		// textView.setTextSize(18);
		// textView.setBackgroundColor(color.background_color);
		// // textView.setBackgroundColor(Color.LTGRAY);
		// textView.setTextColor(Color.WHITE);
		// textView.setGravity(Gravity.LEFT);
		// textView.setPadding(10, 10, 0, 10);

		((ExpandableListView) parent).expandGroup(groupPosition);

		return textView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public void notifyDataSetChanged() {
		// sectionManagerAdapter.configure();
		super.notifyDataSetChanged();
	}

	public SectionManagerInterface getSectionManagerAdapter() {
		return sectionManagerAdapter;
	}

}

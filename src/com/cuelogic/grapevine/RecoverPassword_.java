package com.cuelogic.grapevine;

import java.io.InputStream;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class RecoverPassword_ extends Activity implements OnClickListener {

	private EditText newPassword, confirmNewPassword;
	private Button submitButton;
	private ProgressDialog pd;
	BaseUrl bu;
	SocketConnection sc;
	private String ConfirmationCode = "";
	private String UserKey = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recover_password);
		bu = new BaseUrl();
		sc = new SocketConnection(this);
		Uri data = getIntent().getData();
		String scheme = data.getScheme(); // "http"
		String host = data.getHost(); // "www.grape-vine.com"
		List<String> params = data.getPathSegments();
		if (params.size() >= 2) {
			String first = params.get(0); // "reset"
			UserKey = params.get(1); // "998a0c3eedb7479d9f53abbebe283d41 - user key"
			ConfirmationCode = params.get(2); // "22E6F8" - confirmation_code
		}
		((com.grapevine.views.Header_) findViewById(R.id.header))
				.ConfigureHeaderPane(true, false);
		((TextView) findViewById(R.id.txt_headertitle))
				.setText("RECOVER PASSWORD");
		newPassword = (EditText) findViewById(R.id.newPasswordEdt);
		confirmNewPassword = (EditText) findViewById(R.id.confirmNewPasswordEdt);
		submitButton = (Button) findViewById(R.id.recoverPasswordSubmitButton);
		submitButton.setOnClickListener(this);
		findViewById(R.id.btBack).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btBack:
			Intent callLoginRegister = new Intent(RecoverPassword_.this,
					LoginRegister_.class);
			startActivity(callLoginRegister);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();
			break;

		case R.id.recoverPasswordSubmitButton:
			if (!newPassword.getText().toString()
					.equalsIgnoreCase(confirmNewPassword.getText().toString())) {
				Toast.makeText(RecoverPassword_.this, "Password must match",
						Toast.LENGTH_SHORT).show();
				break;
			} else {
				new AsyncPasswordRecovery().execute();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent callLoginRegister = new Intent(RecoverPassword_.this,
				LoginRegister_.class);
		startActivity(callLoginRegister);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}

	public class AsyncPasswordRecovery extends AsyncTask<Void, Void, String> {
		String s, Json_to_parse;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = ProgressDialog
					.show(RecoverPassword_.this, "", "Please Wait..");
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			InputStream ip;
			try {
				JSONObject eventUpdateJson = new JSONObject();
				eventUpdateJson.put("confirmation_code", ConfirmationCode);
				eventUpdateJson.put("password", newPassword.getText()
						.toString().trim());
				s = sc.SendPUTdata(
						BaseUrl.Development_Uri + bu.getMethodName("LOGIN")
								+ "/" + UserKey + "/recover" + bu.Api_key,
						eventUpdateJson);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return s;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
			System.out.println("The response is " + result);
			if (result.contains("Password changed successfully")) {
				Toast.makeText(RecoverPassword_.this,
						"Password changed successfully.", Toast.LENGTH_SHORT)
						.show();
				newPassword.setText("");
				confirmNewPassword.setText("");
			} else {
				Toast.makeText(RecoverPassword_.this,
						"Please try again later.", Toast.LENGTH_SHORT).show();
			}

		}
	}

}

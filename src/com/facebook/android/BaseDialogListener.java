package com.facebook.android;

import com.facebook.android.Facebook.DialogListener;

// TODO: Auto-generated Javadoc
/**
 * Skeleton base class for RequestListeners, providing default error
 * handling. Applications should handle these error conditions.
 *
 * @see BaseDialogEvent
 */
public abstract class BaseDialogListener implements DialogListener {

    /* (non-Javadoc)
     * @see com.facebook.android.Facebook.DialogListener#onFacebookError(com.facebook.android.FacebookError)
     */
    public void onFacebookError(FacebookError e) {
        e.printStackTrace();
    }

    /* (non-Javadoc)
     * @see com.facebook.android.Facebook.DialogListener#onError(com.facebook.android.DialogError)
     */
    public void onError(DialogError e) {
        e.printStackTrace();        
    }

    /* (non-Javadoc)
     * @see com.facebook.android.Facebook.DialogListener#onCancel()
     */
    public void onCancel() {        
    }
    
}

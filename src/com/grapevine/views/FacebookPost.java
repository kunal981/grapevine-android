package com.grapevine.views;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseDialogListener;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.SessionEvents;
import com.facebook.android.SessionEvents.AuthListener;
import com.facebook.android.SessionStore;
import com.facebook.android.Util;
import com.socketconnection.BaseUrl;

public class FacebookPost {
	// public static final String APP_ID = "132127003621561";
	public static final String APP_ID = BaseUrl.APP_ID;
	private String imageUrl, title, description;
	private TextView mText;
	Context context;
	SharedPreferences sharedPreferences;
	Thread loginSuccessThread, logoutSuccessThread;
	public static String firstName;
	public static String facebookID;
	public static String lastName;
	public static String emailID;
	public static String username;
	private Facebook mFacebook;
	private AsyncFacebookRunner mAsyncRunner;
	public int temp = 0;
	public Context ctx;

	public FacebookPost(Context ctx, String imageUrl, String title,
			String description) {
		mFacebook = new Facebook(APP_ID);
		mAsyncRunner = new AsyncFacebookRunner(mFacebook);
		this.ctx = ctx;
		this.imageUrl = imageUrl;
		this.title = title;
		this.description = description;
		SessionStore.restore(mFacebook, ctx);
		// to check if user is logged in
		SessionEvents.addAuthListener(new SampleAuthListener());
		// SessionEvents.addLogoutListener(new SampleLogoutListener());
		postOnWall("hi");
	}

	public void postOnWall(String msg) {

		Bundle params = new Bundle();
		params.putString("message", "https://www.grape-vine.com");
		params.putString("picture", imageUrl);
		params.putString("description", description);
		params.putString("name", title);
		params.putString("link", "https://www.grape-vine.com");
		mFacebook.dialog(ctx, "stream.publish", params,
				new SampleDialogListener());
	}

	public class SampleDialogListener extends BaseDialogListener {

		public void onComplete(Bundle values) {
			final String postId = values.getString("post_id");
			if (postId != null) {
				Log.d("Facebook-Example", "Dialog Success! post_id=" + postId);
				mAsyncRunner.request(postId, new WallPostRequestListener());
				Toast.makeText(ctx, "Posted on your wall successfully",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.d("Facebook-Example", "No wall post made");
			}
		}
	}

	// listener when wall is posted
	public class WallPostRequestListener extends BaseRequestListener {

		public void onComplete(final String response, final Object state) {
			Log.d("Facebook-Example", "Got response: " + response);

		}

		@Override
		public void onComplete(String response) {
			// TODO Auto-generated method stub

		}
	}

	public class SampleAuthListener implements AuthListener {

		public void onAuthSucceed() {

			// flag for reg

			mAsyncRunner.request("me", new SampleRequestListener());

		}

		public void onAuthFail(String error) {
			mText.setText("Login Failed: " + error);
		}
	}

	public class SampleRequestListener extends BaseRequestListener {

		public void onComplete(String response, Object state) {
			try {

				// process the response here: executed in background thread
				Log.d("Facebook-Example", "Response: " + response.toString());
				JSONObject json = Util.parseJson(response);
				facebookID = json.getString("id");
				firstName = json.getString("first_name");
				lastName = json.getString("last_name");
				emailID = json.getString("email");
				username = json.getString("name");

				// progressDialog.dismiss();
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}

			catch (Exception e) {
				String s = null;
			}
		}

		@Override
		public void onComplete(String response) {
			// TODO Auto-generated method stub

		}
	}

}

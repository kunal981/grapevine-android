package com.cuelogic.grapevine;

/* A class which is use to see privace policy, terms & conditions and about us screen.
 * @author Cuelogic Technologies
 */
import com.grapevine.util.DesignUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

public class PrivacyPolicy extends Activity implements OnClickListener {

	private WebView privacyPolicy;

	// private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.privacy_policy);
		((com.grapevine.views.Header_) findViewById(R.id.header))
				.ConfigureHeaderPane(false, true);

		// pd = ProgressDialog.show(PrivacyPolicy.this, "", "Please Wait..");
		DesignUtil.showProgressDialog(PrivacyPolicy.this);
		Intent i = getIntent();
		String from = i.getStringExtra("WEBVIEW");
		if (from.equals("privacy")) {
			((TextView) findViewById(R.id.txt_headertitle))
					.setText("PRIVACY POLICY");
		} else if (from.equals("terms")) {
			((TextView) findViewById(R.id.txt_headertitle))
					.setText("TERMS OF USE");
		} else if (from.equals("aboutus")) {
			((TextView) findViewById(R.id.txt_headertitle)).setText("ABOUT US");
		}

		privacyPolicy = (WebView) findViewById(R.id.privacypolicy_webview);
		privacyPolicy.getSettings().setJavaScriptEnabled(true);
		privacyPolicy.getSettings().setBuiltInZoomControls(true);
		privacyPolicy.setHorizontalScrollBarEnabled(false);
		privacyPolicy.getSettings().setUseWideViewPort(false);
		if (from.equalsIgnoreCase("privacy"))
			privacyPolicy
					.loadUrl("http://www.grape-vine.com/m/PrivacyPolicy.html");
		else if (from.equalsIgnoreCase("terms"))
			privacyPolicy
					.loadUrl("http://www.grape-vine.com/m/TermAndConditions.html");
		else if (from.equalsIgnoreCase("aboutus"))
			privacyPolicy.loadUrl("http://www.grape-vine.com/m/AboutUs.html");
		privacyPolicy.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				DesignUtil.hideProgressDialog();
			}
		});

		privacyPolicy.setInitialScale(50);

		privacyPolicy.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		((Button) findViewById(R.id.btBack))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intentMainActivity = new Intent(
								PrivacyPolicy.this, LoginRegister_.class);
						startActivity(intentMainActivity);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);
						finish();
					}
				});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intentMainActivity = new Intent(PrivacyPolicy.this,
				LoginRegister_.class);
		startActivity(intentMainActivity);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}

}

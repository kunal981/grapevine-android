package com.grapevine.myevent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.cuelogic.grapevine.R;
import com.grapevine.myprofile.MyProfile_;
import com.grapevine.recommended.Recommended_;
import com.grapevine.ui.SegmentedGroup;
import com.grapevine.ui.SponserImageView;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;

public class MyEvents_ extends Activity implements OnClickListener,
		RadioGroup.OnCheckedChangeListener {
	private ListView myEventsListView;
	private LayoutInflater mInflater;
	static MyEventsAdapter_ meeventsAdapter;
	ArrayList<HashMap<String, String>> myEventList;
	LinearLayout footerRel;
	LinearLayout noItemFoundLinear;
	LinearLayout myRegisteredEventLayout, myBookmarkedEventLayout,
			myBothEventLayout;
	RadioButton radioButton1, radioButton2, radioButton3;
	private ProgressDialog pd;
	private boolean forRegisteredEvent = true, forBookmarkedEvent = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BaseUrl.TAB3_STACK.add(this);
		setContentView(R.layout.my_events_);
		((com.grapevine.views.Header_) findViewById(R.id.header))
				.ConfigureHeaderPane(true, false);
		((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
				.ConfigureTabBarPane(false, false, true, false);
		((TextView) findViewById(R.id.txt_headertitle)).setText("MY EVENTS");
		((Button) findViewById(R.id.btLogout)).setOnClickListener(this);
		findViewById(R.id.tabMyEventsLinearLayout).setEnabled(false);
		mInflater = LayoutInflater.from(MyEvents_.this);
		View footerView = mInflater.inflate(R.layout.footer_, null);
		SponserImageView imageview = (SponserImageView) footerView
				.findViewById(R.id.id_img_sponser_1);
		imageview.setSponserImage(Integer.valueOf(BaseUrl.CommunityID));
		myEventsListView = (ListView) findViewById(R.id.myEventListview);
		myEventsListView.addFooterView(footerView);
		noItemFoundLinear = (LinearLayout) findViewById(R.id.NoSearchFoundLayout);
		noItemFoundLinear.setVisibility(View.GONE);
		myEventList = new ArrayList<HashMap<String, String>>();
		meeventsAdapter = new MyEventsAdapter_(this, myEventList);
		System.out.println("" + BaseUrl.bookmarkedEventId);
		System.out.println("" + BaseUrl.registeredEventId);
		SegmentedGroup segmented4 = (SegmentedGroup) findViewById(R.id.segmented);
		segmented4.setTintColor(getResources().getColor(
				R.color.radio_button_selected_color));
		radioButton1 = (RadioButton) findViewById(R.id.btn_registered);

		radioButton2 = (RadioButton) findViewById(R.id.btn_Bookmarked);
		radioButton3 = (RadioButton) findViewById(R.id.btn_both);

		segmented4.setOnCheckedChangeListener(this);
		new AsynchOrganizationEvent(forRegisteredEvent, forBookmarkedEvent)
				.execute();

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		System.out.println("Came here..onRestart");
		// new AsynchOrganizationEvent(forRegisteredEvent, forBookmarkedEvent)
		// .execute();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.btLogout:
			Intent intent = new Intent(MyEvents_.this, MyProfile_.class);
			startActivity(intent);
			finish();
			break;
		default:
			break;
		}
	}

	public class AsynchOrganizationEvent extends AsyncTask<Void, Void, String> {
		public AsynchOrganizationEvent(boolean flag1, boolean flag2) {
			forRegisteredEvent = flag1;
			forBookmarkedEvent = flag2;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// pd = ProgressDialog.show(MyEvents_.this, "", "Please Wait..");
			DesignUtil.showProgressDialog(MyEvents_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (BaseUrl.RecommendedEvents != null)
				if (BaseUrl.RecommendedEvents.length() > 0) {
					PutEventinHashmap(myEventList, BaseUrl.RecommendedEvents);
				}
			if (BaseUrl.FeaturedEvents != null)
				if (BaseUrl.FeaturedEvents.length() > 0) {
					PutEventinHashmap(myEventList, BaseUrl.FeaturedEvents);
				}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd.dismiss();
			DesignUtil.hideProgressDialog();
			HashSet<HashMap<String, String>> hs = new HashSet<HashMap<String, String>>();
			hs.addAll(myEventList);
			myEventList.clear();
			myEventList.addAll(hs);
			setListView(forRegisteredEvent, forBookmarkedEvent);
		}

	}

	public void PutEventinHashmap(
			ArrayList<HashMap<String, String>> arrayToAdd, JSONArray jsonarray) {
		for (int i = 0; i < jsonarray.length(); i++) {
			try {
				JSONObject arrayElement = jsonarray.getJSONObject(i);
				if (BaseUrl.bookmarkedEventId.contains(arrayElement
						.getString("id"))
						| BaseUrl.registeredEventId.contains(arrayElement
								.getString("id"))) {
					JSONObject orgnazationObj = arrayElement
							.getJSONObject("organization");
					HashMap<String, String> hash = new HashMap<String, String>();
					hash.put("orgnization", orgnazationObj.getString("name"));
					hash.put("orgnizationid", orgnazationObj.getString("id"));
					hash.put("address_line_1",
							arrayElement.getString("address_line_1"));
					hash.put("city", arrayElement.getString("city"));
					hash.put("description",
							arrayElement.getString("description"));
					hash.put("distance", arrayElement.getString("distance"));
					hash.put("end_time", arrayElement.getString("end_time"));
					hash.put("start_time",
							arrayElement.getString("start_time_local"));
					// hash.put("start_time",
					// arrayElement.getString("start_time"));
					hash.put("event_url", arrayElement.getString("event_url"));
					hash.put("public_url", arrayElement.getString("public_url"));
					hash.put("id", arrayElement.getString("id"));
					hash.put("image_url", arrayElement.getString("image_url"));
					hash.put("latitude", arrayElement.getString("latitude"));
					hash.put("location", arrayElement.getString("location"));
					hash.put("longitude", arrayElement.getString("longitude"));
					hash.put("postalcode", arrayElement.getString("postalcode"));
					hash.put("registration_deadline",
							arrayElement.getString("registration_deadline"));
					hash.put("registration_url",
							arrayElement.getString("registration_url"));
					hash.put("state", arrayElement.getString("state"));
					hash.put("teaser", arrayElement.getString("teaser"));
					hash.put("title", arrayElement.getString("title"));
					hash.put("user_feedback",
							arrayElement.getString("user_feedback"));
					hash.put("user_like", arrayElement.getString("user_like"));
					hash.put("user_not_like",
							arrayElement.getString("user_not_like"));
					hash.put("user_registered",
							arrayElement.getString("user_registered"));
					hash.put("user_score", arrayElement.getString("user_score"));
					if (arrayElement.getString("user_like").equalsIgnoreCase(
							"1")) {
						boolean isFound = false;
						for (int k = 0; k < BaseUrl.likedEventList.size(); k++) {
							HashMap<String, String> hm = BaseUrl.likedEventList
									.get(k);
							if (hm.get("id").equalsIgnoreCase(
									arrayElement.getString("id")))
								isFound = true;
						}
						if (!isFound)
							BaseUrl.likedEventList.add(hash);
					}
					arrayToAdd.add(hash);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void setListView(boolean registered, boolean bookmarked) {
		if (myEventList.size() == 0) {
			// footerRel.setVisibility(View.VISIBLE);
			noItemFoundLinear.setVisibility(View.VISIBLE);
			myEventsListView.setVisibility(View.GONE);
		} else {
			if (registered & bookmarked) {
				myEventList.clear();
				myEventList.addAll(BaseUrl.bookmarkedEventList);
				myEventList.addAll(BaseUrl.registeredEventList);
				HashSet<HashMap<String, String>> hs = new HashSet<HashMap<String, String>>();
				hs.addAll(myEventList);
				myEventList.clear();
				myEventList.addAll(hs);
				meeventsAdapter = new MyEventsAdapter_(this, myEventList);
				myEventsListView.setVisibility(View.VISIBLE);
				myEventsListView.setAdapter(meeventsAdapter);
				myEventsListView.postInvalidate();
				meeventsAdapter.notifyDataSetChanged();
				// footerRel.setVisibility(View.GONE);
				noItemFoundLinear.setVisibility(View.GONE);
			}
			if (!registered & bookmarked) {
				ArrayList<HashMap<String, String>> myBookmarkedEvents = new ArrayList<HashMap<String, String>>();
				// for (int i = 0; i < myEventList.size(); i++) {
				// if (BaseUrl.bookmarkedEventId.contains(myEventList.get(i)
				// .get("id")))
				// myBookmarkedEvents.add(myEventList.get(i));
				// }
				HashSet<HashMap<String, String>> hs = new HashSet<HashMap<String, String>>();
				hs.addAll(BaseUrl.bookmarkedEventList);
				BaseUrl.bookmarkedEventList.clear();
				BaseUrl.bookmarkedEventList.addAll(hs);
				for (int i = 0; i < BaseUrl.bookmarkedEventList.size(); i++) {
					myBookmarkedEvents.add(BaseUrl.bookmarkedEventList.get(i));
				}
				if (myBookmarkedEvents.size() == 0) {
					// footerRel.setVisibility(View.VISIBLE);
					noItemFoundLinear.setVisibility(View.VISIBLE);
					myEventsListView.setVisibility(View.GONE);
				} else {

					meeventsAdapter = new MyEventsAdapter_(this,
							myBookmarkedEvents);
					myEventsListView.setVisibility(View.VISIBLE);
					myEventsListView.setAdapter(meeventsAdapter);
					myEventsListView.postInvalidate();
					meeventsAdapter.notifyDataSetChanged();
					// footerRel.setVisibility(View.GONE);
					noItemFoundLinear.setVisibility(View.GONE);
				}
			}
			if (registered & !bookmarked) {
				ArrayList<HashMap<String, String>> myRegisteredEvents = new ArrayList<HashMap<String, String>>();
				// for(int i =0;i<myEventList.size();i++){
				// if(SplashScreen.registeredEventId.contains(myEventList.get(i).get("id")))
				// myRegisteredEvents.add(myEventList.get(i));
				// }
				HashSet<HashMap<String, String>> hs = new HashSet<HashMap<String, String>>();
				hs.addAll(BaseUrl.registeredEventList);
				BaseUrl.registeredEventList.clear();
				BaseUrl.registeredEventList.addAll(hs);
				for (int i = 0; i < BaseUrl.registeredEventList.size(); i++) {
					myRegisteredEvents.add(BaseUrl.registeredEventList.get(i));
				}
				if (myRegisteredEvents.size() == 0) {
					// footerRel.setVisibility(View.VISIBLE);
					noItemFoundLinear.setVisibility(View.VISIBLE);
					myEventsListView.setVisibility(View.GONE);
				} else {
					meeventsAdapter = new MyEventsAdapter_(this,
							myRegisteredEvents);
					myEventsListView.setVisibility(View.VISIBLE);
					myEventsListView.setAdapter(meeventsAdapter);
					myEventsListView.postInvalidate();
					meeventsAdapter.notifyDataSetChanged();
					// footerRel.setVisibility(View.GONE);
					noItemFoundLinear.setVisibility(View.GONE);
				}
			}

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (BaseUrl.TAB2_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB2_STACK.size(); i++) {
				Activity act = BaseUrl.TAB2_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB1_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB1_STACK.size(); i++) {
				Activity act = BaseUrl.TAB1_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB3_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB3_STACK.size(); i++) {
				Activity act = BaseUrl.TAB3_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB4_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB4_STACK.size(); i++) {
				Activity act = BaseUrl.TAB4_STACK.get(i);
				act.finish();
			}
		}
		BaseUrl.TAB3_LAST_ACTIVITY = this.getClass();
		Intent i = new Intent(this, Recommended_.class);
		startActivity(i);
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		finish();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		switch (checkedId) {

		case R.id.btn_Bookmarked:
			forRegisteredEvent = false;
			forBookmarkedEvent = true;
			setListView(forRegisteredEvent, forBookmarkedEvent);
			break;
		case R.id.btn_registered:
			forRegisteredEvent = true;
			forBookmarkedEvent = false;
			setListView(forRegisteredEvent, forBookmarkedEvent);
			break;
		case R.id.btn_both:

			forRegisteredEvent = true;
			forBookmarkedEvent = true;
			setListView(forRegisteredEvent, forBookmarkedEvent);
			break;
		}

	}
}

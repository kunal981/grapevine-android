package com.cuelogic.grapevine.util;

public class AppConstant {

	public static class Config {
		public static final boolean DEVELOPER_MODE = false;

		public static final String KEY_PREF_SPONSER_ID = "com.cuelogic.grapevine.ID.SPONSER";

	}
}

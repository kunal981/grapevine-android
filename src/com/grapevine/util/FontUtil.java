package com.grapevine.util;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Typeface;

public class FontUtil {

	private static final Map<String, Typeface> typefaces = new HashMap<String, Typeface>();

	public static Typeface getTypeface(Context ctx, String fontName) {
		Typeface typeface = typefaces.get(fontName);
		if (typeface == null) {
			typeface = Typeface.createFromAsset(ctx.getAssets(), fontName);
			typefaces.put(fontName, typeface);
		}
		return typeface;
	}

}

package com.grapevine.interest;

/* A screen which shows user a list of Interests and Communities to select.
 * @author Cuelogic Technologies
 */
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.cuelogic.grapevine.R;
import com.grapevine.recommended.Recommended_;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class Intersest_ extends Activity implements OnClickListener {

	ListView interestsCommunitiesLv;
	ArrayList<HashMap<String, String>> interestList, communitiesList;
	InterestAdapter_ interestAdapter, communitiesAdapter;
	String InterstsToParse, CommunitiesToParse;
	private ProgressDialog pd;
	SocketConnection sc;
	BaseUrl bu;
	private LayoutInflater mInflater;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.interest_communities_);
		interestsCommunitiesLv = (ListView) findViewById(R.id.InterestCommunitiesListview);

		interestList = new ArrayList<HashMap<String, String>>();
		communitiesList = new ArrayList<HashMap<String, String>>();
		interestAdapter = new InterestAdapter_(this, interestList, "INTERESTS");
		communitiesAdapter = new InterestAdapter_(this, communitiesList,
				"COMMUNITIES");
		Intent intent = getIntent();
		sc = new SocketConnection(this);
		bu = new BaseUrl();

		if (intent.getExtras() != null) {

			InterstsToParse = intent.getStringExtra("allInterest");
			CommunitiesToParse = intent.getStringExtra("allCommunities");
			new AsynchInterest().execute();

			findViewById(R.id.btContinue).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new AsynchUpdateUser().execute();
						}
					});
		}
	}

	@Override
	public void onClick(View v) {

	}

	/*
	 * A thread which runs in background to parse all the Interest and Communities.
	 */
	public class AsynchInterest extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = ProgressDialog.show(Intersest_.this, "", "Please Wait..");
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				JSONArray arrayOfInterests = new JSONArray(InterstsToParse);
				for (int i = 0; i < arrayOfInterests.length(); i++) {
					JSONObject obj = arrayOfInterests.getJSONObject(i);
					HashMap<String, String> hm = new HashMap<String, String>();
					hm.put("ID", obj.getString("id"));
					hm.put("Value", obj.getString("value"));
					interestList.add(hm);
				}
				JSONArray arrayOfCommunities = new JSONArray(CommunitiesToParse);
				for (int i = 0; i < arrayOfCommunities.length(); i++) {
					JSONObject obj = arrayOfCommunities.getJSONObject(i);
					HashMap<String, String> hm = new HashMap<String, String>();
					hm.put("ID", obj.getString("id"));
					hm.put("Value", obj.getString("value"));
					communitiesList.add(hm);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
			InterestAdapterHeader_ adapter = new InterestAdapterHeader_(
					Intersest_.this, "INTERESTS", "COMMUNITIES");
			adapter.addSection("COMMUNITIES", communitiesAdapter);
			adapter.addSection("INTERESTS", interestAdapter);
			interestsCommunitiesLv.setAdapter(adapter);
		}

	}

	/*
	 * A service is called to save the checked Interests and Communities.
	 */
	public class AsynchUpdateUser extends AsyncTask<Void, Void, String> {
		String s;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = ProgressDialog.show(Intersest_.this, "", "Please Wait..");
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			try {
				JSONObject updateInterestJson = new JSONObject();
				JSONArray arrayInterest = new JSONArray();
				// for(int i
				// =0;i<InterestAdapter_.selected_INTEREST.size();i++){
				// arrayInterest.put(InterestAdapter_.selected_INTEREST.get(i));
				// }
				for (int i = 0; i < BaseUrl.checkedINTEREST.size(); i++) {
					arrayInterest.put(BaseUrl.checkedINTEREST.get(i));
				}
				updateInterestJson.put("interests", arrayInterest);
				JSONArray arrayCommunity = new JSONArray();
				// for(int i
				// =0;i<InterestAdapter_.selected_COMMUNITIES.size();i++){
				// arrayCommunity.put(InterestAdapter_.selected_COMMUNITIES.get(i));
				// }
				for (int i = 0; i < BaseUrl.checkedCOMMUNITIES.size(); i++) {
					arrayCommunity.put(BaseUrl.checkedCOMMUNITIES.get(i));
				}
				updateInterestJson.put("communities", arrayCommunity);
				System.out.println("" + updateInterestJson);
				s = sc.SendPUTdata(
						bu.Development_Uri
								+ bu.getMethodName("UPDATE_INTEREST_COMMUNITIES")
								+ bu.USER_KEY, updateInterestJson);
			} catch (JSONException e) {
			}
			return s;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
			if (s != null) {
				if (s.contains("success")) {
					Toast.makeText(Intersest_.this, "Updated successfully",
							Toast.LENGTH_SHORT).show();
					Intent i = new Intent(getApplicationContext(),
							Recommended_.class);
					startActivity(i);
					finish();
				}
			} else {
				Toast.makeText(Intersest_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
		}

	}
}

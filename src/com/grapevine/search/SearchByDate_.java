package com.grapevine.search;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cuelogic.grapevine.LocationChangeHandler;
import com.cuelogic.grapevine.R;
import com.grapevine.myprofile.MyProfile_;
import com.grapevine.ui.SegmentedGroup;
import com.grapevine.ui.SponserImageView;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class SearchByDate_ extends Activity implements OnClickListener,
		RadioGroup.OnCheckedChangeListener {

	// private ToggleButton soonestClosetToggle;
	private ListView searchByDateEventsListView;
	private LayoutInflater mInflater;
	static SearchResultAdapter_ searchResultAdapter;
	ArrayList<HashMap<String, String>> searchByDateEventList;
	// LinearLayout footerRel;
	LinearLayout noItemFoundLinear;

	SocketConnection sc;
	BaseUrl bu;
	String latitude, longitude;
	private Button startDate, endDate;
	DateFormat fmtDateAndTime = DateFormat.getDateTimeInstance();
	static Calendar myCalendarStartDate, myCalendarEndDate;// =
	RadioButton radioButton1, radioButton2;
	SegmentedGroup segmented4; // Calendar.getInstance();
	SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy");
	private static long startDateTimestamp, endDateTimestamp;
	private boolean wait1, wait2 = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_by_date_);
		BaseUrl.TAB2_STACK.add(this);
		((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
				.ConfigureTabBarPane(false, true, false, false);
		((TextView) findViewById(R.id.txt_headertitle))
				.setText("EVENTS BY DATE");
		((Button) findViewById(R.id.btLogout)).setOnClickListener(this);
		mInflater = LayoutInflater.from(SearchByDate_.this);
		View footerView = mInflater.inflate(R.layout.footer_, null);
		SponserImageView imageview = (SponserImageView) footerView
				.findViewById(R.id.id_img_sponser_1);
		imageview.setSponserImage(Integer.valueOf(BaseUrl.CommunityID));

		segmented4 = (SegmentedGroup) findViewById(R.id.segmented_button);
		// segmented4.setTintColor(getResources().getColor(
		// R.color.radio_button_selected_color));
		segmented4.setOnCheckedChangeListener(this);
		radioButton1 = (RadioButton) findViewById(R.id.btn_nearest);

		radioButton2 = (RadioButton) findViewById(R.id.btn_soonest);

		searchByDateEventsListView = (ListView) findViewById(R.id.searchByDateEventListview);

		searchByDateEventsListView.addFooterView(footerView);
		startDate = (Button) findViewById(R.id.startDate);
		startDate.setOnClickListener(this);
		endDate = (Button) findViewById(R.id.endDate);
		endDate.setOnClickListener(this);

		// footerRel = (LinearLayout) findViewById(R.id.footerLinear);
		noItemFoundLinear = (LinearLayout) findViewById(R.id.NoSearchFoundLayout);
		findViewById(R.id.btBack).setOnClickListener(this);
		// footerRel.setVisibility(View.GONE);
		noItemFoundLinear.setVisibility(View.GONE);
		searchByDateEventList = new ArrayList<HashMap<String, String>>();
		searchResultAdapter = new SearchResultAdapter_(this,
				searchByDateEventList);
		sc = new SocketConnection(this);
		bu = new BaseUrl();
		turnGPSOn();
		LocationChangeHandler.setInstance(new LocationChangeHandler(
				getApplicationContext()));
		LocationChangeHandler.getInstance().beginUpdate();
		myCalendarStartDate = Calendar.getInstance();
		myCalendarEndDate = Calendar.getInstance();

		Date date1 = new Date();
		myCalendarStartDate.setTime(date1);
		startDateTimestamp = System.currentTimeMillis();
		// startDate.setText(sdfDate.format(myCalendarStartDate.getTime()));

		Long tsLong = System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000);
		Date date2 = new Date(tsLong);
		myCalendarEndDate.setTime(date2);
		endDateTimestamp = tsLong;
		// endDate.setText(sdfDate.format(myCalendarEndDate.getTime()));
		findViewById(R.id.tabSearchLinearLayout).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent startSearchScreen = new Intent(
								SearchByDate_.this, SearchScreen_.class);
						startActivity(startSearchScreen);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);
						finish();
					}
				});
		new AsynchDateEvents().execute();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.startDate:
			new DatePickerDialog(this, d,
					myCalendarStartDate.get(myCalendarStartDate.YEAR),
					myCalendarStartDate.get(myCalendarStartDate.MONTH),
					myCalendarStartDate.get(Calendar.DAY_OF_MONTH)).show();
			break;
		case R.id.endDate:
			new DatePickerDialog(this, e,
					myCalendarEndDate.get(myCalendarEndDate.YEAR),
					myCalendarEndDate.get(myCalendarEndDate.MONTH),
					myCalendarEndDate.get(Calendar.DAY_OF_MONTH)).show();
			break;

		case R.id.btBack:
			Intent i = new Intent(this, SearchScreen_.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();
			break;
		case R.id.btLogout:
			Intent intent = new Intent(SearchByDate_.this, MyProfile_.class);
			startActivity(intent);
			finish();
			break;

		default:
			break;
		}
	}

	DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			myCalendarStartDate.set(Calendar.YEAR, year);
			myCalendarStartDate.set(Calendar.MONTH, monthOfYear);
			myCalendarStartDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			startDateTimestamp = myCalendarStartDate.getTimeInMillis();
			if (startDateTimestamp > endDateTimestamp) {
				Toast.makeText(SearchByDate_.this,
						"Start date must be less than End date",
						Toast.LENGTH_SHORT).show();
				startDateTimestamp = System.currentTimeMillis();
			} else {
				startDate
						.setText(sdfDate.format(myCalendarStartDate.getTime()));
				System.out.println("Called from start date");
				if (wait1) {
					new AsynchDateEvents().execute();
					wait1 = false;
				}

			}
		}

	};

	DatePickerDialog.OnDateSetListener e = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			myCalendarEndDate.set(Calendar.YEAR, year);
			myCalendarEndDate.set(Calendar.MONTH, monthOfYear);
			myCalendarEndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			endDateTimestamp = myCalendarEndDate.getTimeInMillis();
			if (endDateTimestamp < startDateTimestamp) {
				Toast.makeText(SearchByDate_.this,
						"End date can not be less than Start date",
						Toast.LENGTH_SHORT).show();
			} else {
				endDate.setText(sdfDate.format(myCalendarEndDate.getTime()));
				System.out.println("Called from end date");
				if (wait2) {
					new AsynchDateEvents().execute();
					wait2 = false;
				}

			}
		}
	};

	/*
	 * A function used to turn-on device GPS if it is off.
	 */
	public void turnGPSOn() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", true);
		this.sendBroadcast(intent);
		String provider = Settings.Secure.getString(this.getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) { // if gps is disabled

			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			this.sendBroadcast(poke);

		}
	}

	/*
	 * A function used to turn-off device GPS.
	 */
	public void turnGPSOff() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", false);
		this.sendBroadcast(intent);
	}

	// A service is called to get all the events between given dates

	public class AsynchDateEvents extends AsyncTask<Void, Void, String> {
		String Json_to_parse;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// proDialog = ProgressDialog.show(SearchByDate_.this, "",
			// "Please Wait..");
			DesignUtil.showProgressDialog(SearchByDate_.this);
			System.out.println("Im here again");

		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				if (LocationChangeHandler.getLocation() != null) {
					latitude = LocationChangeHandler.getLocation()
							.getLatitude() + "";
					longitude = LocationChangeHandler.getLocation()
							.getLongitude() + "";
				} else {
					latitude = 35.74184050952353 + "";
					longitude = -118.8128372137487 + "";
				}
				InputStream ip = sc.getHttpStream(BaseUrl.Development_Uri
						+ bu.getMethodName("RECOMMENDED") + bu.USER_KEY
						+ bu.getMethodName("RECOMMENDED_EVENTS") + bu.Api_key
						+ "&type=all&lat=" + latitude + "&lon=" + longitude
						+ "&filter_community_id=" + bu.CommunityID
						+ "&filter_start_date=" + (startDateTimestamp / 1000)
						+ "" + "&filter_end_date=" + (endDateTimestamp / 1000)
						+ "");
				Json_to_parse = sc.convertStreamToString(ip);
				parseJson(Json_to_parse);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// return Json_to_parse;
			return Json_to_parse;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			DesignUtil.hideProgressDialog();

			LocationChangeHandler.getInstance().stopUpdates();
			turnGPSOff();
			if (result != null)
				setListView(true);
			else
				Toast.makeText(SearchByDate_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
		}

	}

	public void parseJson(String jsonString) {
		try {
			JSONObject recommendedJsonObject = new JSONObject(jsonString);
			// jsonFeatured = (JSONObject) jsonObject.get("sponsored");
			// JSONObject jsonRecommended = (JSONObject)
			// jsonObject.get("recommended");
			JSONArray arrayallEvents = recommendedJsonObject
					.getJSONArray("all");
			if (arrayallEvents.length() > 0) {
				searchByDateEventList.clear();
				PutEventinHashmap(searchByDateEventList, arrayallEvents);
			}

		} catch (JSONException e) {
			DesignUtil.hideProgressDialog();
			searchByDateEventList.clear();
		} catch (Exception e) {
			DesignUtil.hideProgressDialog();
			searchByDateEventList.clear();
		}
	}

	public void PutEventinHashmap(
			ArrayList<HashMap<String, String>> arrayToAdd, JSONArray jsonarray) {

		for (int i = 0; i < jsonarray.length(); i++) {
			try {
				JSONObject arrayElement = jsonarray.getJSONObject(i);
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("address_line_1",
						arrayElement.getString("address_line_1"));
				hash.put("city", arrayElement.getString("city"));
				hash.put("description", arrayElement.getString("description"));
				hash.put("distance", arrayElement.getString("distance"));
				hash.put("end_time", arrayElement.getString("end_time"));
				// hash.put("start_time", arrayElement.getString("start_time"));
				hash.put("start_time",
						arrayElement.getString("start_time_local"));
				hash.put("event_url", arrayElement.getString("event_url"));
				hash.put("public_url", arrayElement.getString("public_url"));
				hash.put("id", arrayElement.getString("id"));
				hash.put("image_url", arrayElement.getString("image_url"));
				hash.put("latitude", arrayElement.getString("latitude"));
				hash.put("location", arrayElement.getString("location"));
				hash.put("longitude", arrayElement.getString("longitude"));
				hash.put("postalcode", arrayElement.getString("postalcode"));
				hash.put("registration_deadline",
						arrayElement.getString("registration_deadline"));
				hash.put("registration_url",
						arrayElement.getString("registration_url"));
				hash.put("state", arrayElement.getString("state"));
				hash.put("teaser", arrayElement.getString("teaser"));
				hash.put("title", arrayElement.getString("title"));
				hash.put("user_feedback",
						arrayElement.getString("user_feedback"));
				hash.put("user_like", arrayElement.getString("user_like"));
				hash.put("user_not_like",
						arrayElement.getString("user_not_like"));
				hash.put("user_registered",
						arrayElement.getString("user_registered"));
				hash.put("user_score", arrayElement.getString("user_score"));
				JSONObject orgnazationObj = arrayElement
						.getJSONObject("organization");
				hash.put("orgnization", orgnazationObj.getString("name"));
				arrayToAdd.add(hash);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void setListView(boolean flag) {
		if (searchByDateEventList.size() == 0) {
			// footerRel.setVisibility(View.VISIBLE);
			noItemFoundLinear.setVisibility(View.VISIBLE);
			searchByDateEventsListView.setVisibility(View.GONE);
		} else {
			wait1 = true;
			wait2 = true;
			searchByDateEventsListView.setVisibility(View.VISIBLE);
			if (flag) {
				Collections.sort(searchByDateEventList,
						new Comparator<HashMap<String, String>>() {
							@Override
							public int compare(HashMap<String, String> lhs,
									HashMap<String, String> rhs) {
								if (lhs.get("start_time").length() > 0
										&& rhs.get("start_time").length() > 0) {
									if (Integer.parseInt(lhs.get("start_time")) < Integer
											.parseInt(rhs.get("start_time"))) {
										return -1;
									}
								}
								return 0;

							}
						});
			} else {
				Collections.sort(searchByDateEventList,
						new Comparator<HashMap<String, String>>() {
							@Override
							public int compare(HashMap<String, String> lhs,
									HashMap<String, String> rhs) {
								if (lhs.get("distance").length() > 0
										&& rhs.get("distance").length() > 0
										&& !lhs.get("distance")
												.contains("null")
										&& !rhs.get("distance")
												.contains("null")) {
									if (Integer.parseInt(lhs.get("distance")) < Integer
											.parseInt(rhs.get("distance"))) {
										return -1;
									}
								}
								return 0;

							}
						});
			}
			searchByDateEventsListView.setAdapter(searchResultAdapter);
			searchByDateEventsListView.postInvalidate();
			searchResultAdapter.notifyDataSetChanged();
			// footerRel.setVisibility(View.GONE);
			noItemFoundLinear.setVisibility(View.GONE);
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent i = new Intent(this, SearchScreen_.class);
		startActivity(i);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		switch (checkedId) {
		case R.id.btn_nearest:

			setListView(true);

			break;
		case R.id.btn_soonest:

			setListView(false);

			break;
		}
	}
}

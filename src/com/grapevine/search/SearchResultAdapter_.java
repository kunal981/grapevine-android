package com.grapevine.search;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cuelogic.grapevine.R;
import com.grapevine.recommended.AsynchUpdateAction;
import com.grapevine.recommended.EventDetail_;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class SearchResultAdapter_ extends BaseAdapter {
	Context context;
	ViewHolder holder;
	private LayoutInflater mInflater;
	String listTitle;
	public ArrayList<HashMap<String, String>> List = new ArrayList<HashMap<String, String>>();
	SocketConnection sc;
	BaseUrl bu;
	public static ArrayList<String> selected_eventThumbsUp;
	public static ArrayList<String> selected_eventThumbsDown;
	DisplayImageOptions options;

	public SearchResultAdapter_(Context context,
			ArrayList<HashMap<String, String>> homeList) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
		this.List = homeList;
		selected_eventThumbsUp = new ArrayList<String>();
		selected_eventThumbsDown = new ArrayList<String>();
		sc = new SocketConnection(this.context);
		bu = new BaseUrl();
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.no_image_icon)
				.showImageOnFail(R.drawable.no_image_icon).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(1)).build();

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return List.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return List.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View vx, ViewGroup parent) {

		ViewGroup convertView = (ViewGroup) mInflater
				.inflate(
						com.cuelogic.grapevine.R.layout.my_recommended_screen_listitem_,
						null);
		holder = new ViewHolder();
		holder.tvTitle = (TextView) convertView
				.findViewById(R.id.recommendedTitleTV);
		holder.imageEvent = (ImageView) convertView
				.findViewById(R.id.img_event_id);
		holder.tvDesc = (TextView) convertView
				.findViewById(R.id.txt_description);
		holder.tvHost = (TextView) convertView.findViewById(R.id.tv_host);
		holder.tvEndTime = (TextView) convertView
				.findViewById(R.id.recommendedEndTimeTV);

		holder.thumbUp = (Button) convertView.findViewById(R.id.btn_like);
		holder.thumbDown = (Button) convertView.findViewById(R.id.btn_cancel);
		holder.layoutListItem = (RelativeLayout) convertView
				.findViewById(R.id.id_list_row);

		// add elements to al, including duplicates
		holder.tvTitle.setText(List.get(position).get("title"));
		String address = "";
		if (!List.get(position).get("location").equals("")
				&& !List.get(position).get("city").equals("")
				&& !List.get(position).get("state").equals("")) {

			address = List.get(position).get("location") + ", "
					+ List.get(position).get("city") + ", "
					+ List.get(position).get("state") + " "
					+ List.get(position).get("postalcode");
		} else if (!List.get(position).get("city").equals("")
				&& !List.get(position).get("state").equals("")) {
			address = List.get(position).get("city") + ", "
					+ List.get(position).get("state") + " "
					+ List.get(position).get("postalcode");
		} else if (!List.get(position).get("state").equals("")) {
			address = List.get(position).get("state") + " "
					+ List.get(position).get("postalcode");
		}
		if (!address.equals("")) {
			holder.tvDesc.setText(address.trim());
		} else {
			holder.tvDesc.setVisibility(View.GONE);
		}
		holder.tvHost.setText(List.get(position).get("orgnization"));
		String imgUrlString = List.get(position).get("image_url");
		ImageLoader.getInstance().displayImage(imgUrlString, holder.imageEvent,
				options);

		if (List.get(position).get("end_time").length() == 0) {
			String DateTime = getDate(List.get(position).get("end_time"));
			holder.tvEndTime.setText(DateTime);
		} else {
			String DateTime = getDate(List.get(position).get("start_time"));
			holder.tvEndTime.setText(DateTime);
		}
		if (List.get(position).get("user_like").equalsIgnoreCase("1")) {
			selected_eventThumbsUp.add(List.get(position).get("id"));
			holder.thumbUp.setSelected(true);
		}
		if (List.get(position).get("user_not_like").equalsIgnoreCase("1")) {
			selected_eventThumbsDown.add(List.get(position).get("id"));
			holder.thumbDown.setSelected(true);
		}

		removeDuplicates();

		holder.thumbUp.setTag(List.get(position).get("id"));
		holder.thumbDown.setTag(List.get(position).get("id"));

		holder.thumbUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				System.out.println("The position is " + position);
				if (v.isSelected()) {
					v.setSelected(false);
					selected_eventThumbsUp.remove(List.get(position).get("id"));
					List.get(position).put("user_like", "0");
					BaseUrl.likedEventList.remove(List.get(position));
					new AsynchUpdateAction(List.get(position).get("id"),
							"unlike", context).execute();
				} else {
					v.setSelected(true);
					List.get(position).put("user_like", "1");
					selected_eventThumbsUp.add(List.get(position).get("id"));
					BaseUrl.likedEventList.add(List.get(position));
					new AsynchUpdateAction(List.get(position).get("id"),
							"like", context).execute();

					if (selected_eventThumbsDown.contains(List.get(position)
							.get("id"))) {
						ViewGroup viewParent = (ViewGroup) v.getParent();
						viewParent.findViewById(R.id.btn_cancel).setSelected(
								false);
						List.get(position).put("user_not_like", "0");
						selected_eventThumbsDown.remove(List.get(position).get(
								"id"));
						notifyDataSetChanged();
						// ((Activity)(context)).searchResultAdapter.notifyDataSetChanged();

					}
				}

			}

		});

		holder.thumbDown.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (v.isSelected()) {
					v.setSelected(false);
					List.get(position).put("user_not_like", "0");
					selected_eventThumbsDown.remove(List.get(position)
							.get("id"));
					new AsynchUpdateAction(List.get(position).get("id"),
							"unnotlike", context).execute();
				} else {
					v.setSelected(true);
					List.get(position).put("user_not_like", "1");
					selected_eventThumbsDown.add(List.get(position).get("id"));
					new AsynchUpdateAction(List.get(position).get("id"),
							"notlike", context).execute();

					if (selected_eventThumbsUp.contains(List.get(position).get(
							"id"))) {
						ViewGroup viewParent = (ViewGroup) v.getParent();
						viewParent.findViewById(R.id.btn_cancel).setSelected(
								false);
						List.get(position).put("user_like", "0");
						selected_eventThumbsUp.remove(List.get(position).get(
								"id"));
						BaseUrl.likedEventList.remove(List.get(position));
						notifyDataSetChanged();
						// SearchResult.searchResultAdapter.notifyDataSetChanged();
					}

				}

			}
		});

		holder.layoutListItem.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				BaseUrl.TAB2_LAST_ACTIVITY = context.getClass();
				Intent intent = new Intent(context, EventDetail_.class);
				HashMap<String, String> p = List.get(position);
				intent.putExtra("EventDetail", p);
				intent.putExtra("From", BaseUrl.TAB2_LAST_ACTIVITY + "");
				context.startActivity(intent);
				// ((Activity)context).finish();
				((Activity) context).overridePendingTransition(
						R.anim.slide_in_right, R.anim.slide_out_left);
			}
		});

		return convertView;

	}

	class ViewHolder {
		TextView tvTitle, tvEndTime, tvDesc, tvHost;
		RelativeLayout layoutListItem;
		Button thumbUp, thumbDown, btnCancel;
		ImageView imageEvent;
	}

	private String getDate(String timeStampStr) {

		try {
			SimpleDateFormat writeFormat = new SimpleDateFormat(
					"MMM dd, yyyy hh:mm aaa");
			Date netDate = (new Date(Long.parseLong(timeStampStr) * 1000));
			return writeFormat.format(netDate);
		} catch (Exception ex) {
			return null;
		}
	}

	private String getTimeStamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		return tsLong.toString();
	}

	public void removeDuplicates() {
		HashSet<String> hs = new HashSet<String>();
		hs.addAll(selected_eventThumbsUp);
		selected_eventThumbsUp.clear();
		selected_eventThumbsUp.addAll(hs);
		HashSet<String> hs2 = new HashSet<String>();
		hs2.addAll(selected_eventThumbsDown);
		selected_eventThumbsDown.clear();
		selected_eventThumbsDown.addAll(hs2);
	}
}

package com.grapevine.organization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.cuelogic.grapevine.R;
import com.grapevine.myprofile.MyProfile_;
import com.grapevine.organization.SectionListIndexerScroller_.SectionIndexChangeInterface;
import com.grapevine.search.SearchScreen_;
import com.grapevine.ui.MySpinnerAdapter;
import com.grapevine.ui.SponserImageView;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class SearchOrganization_ extends Activity implements OnClickListener,
		OnChildClickListener {

	private Spinner interestSpinner;
	// To store Id - Value of all Interest.We need the IDs when we sort
	// organization based on Interest.
	private HashMap<String, String> hashmapAllInterest = new HashMap<String, String>();
	// Arraylist that will bind to the spinner.
	private ArrayList<String> interestArray;
	// Hashmap and arraylist for Storing organization to show in Listview
	private ArrayList<HashMap<String, String>> organizationArrayList;
	private BaseExpandableListAdapter baseExpandableListAdapter;
	private ExpandableListView expandableListView;
	BaseUrl bu;
	private LayoutInflater mInflater;
	private EditText filterOrgs;
	private ArrayList<HashMap<String, String>> array_sort;
	private ArrayList<HashMap<String, String>> array_sort_spinner;

	SocketConnection sc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		BaseUrl.TAB2_STACK.add(this);

		setContentView(R.layout.search_organization_screen_);
		bu = new BaseUrl();
		sc = new SocketConnection(this);
		((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
				.ConfigureTabBarPane(false, true, false, false);
		((TextView) findViewById(R.id.txt_headertitle))
				.setText("EVENTS BY ORGANIZER");
		((Button) findViewById(R.id.btLogout)).setOnClickListener(this);
		findViewById(R.id.btBack).setOnClickListener(this);
		expandableListView = (ExpandableListView) findViewById(R.id.organizaitListview);
		expandableListView.setFastScrollEnabled(false);
		expandableListView.setOnChildClickListener(this);
		mInflater = LayoutInflater.from(SearchOrganization_.this);
		View footerView = mInflater.inflate(R.layout.footer_, null);
		SponserImageView imageview = (SponserImageView) footerView
				.findViewById(R.id.id_img_sponser_1);
		imageview.setSponserImage(Integer.valueOf(bu.CommunityID));
		footerView.setPadding(0, 20, 0, 0);
		expandableListView.addFooterView(footerView);
		interestSpinner = (Spinner) (findViewById(R.id.interestSpinner));

		new AsynchLoadData().execute();

		// initFingerScrolling();

		array_sort_spinner = new ArrayList<HashMap<String, String>>();
		interestSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				array_sort_spinner.clear();
				if (interestSpinner.getSelectedItem().toString()
						.equalsIgnoreCase("All Interests")) {
					baseExpandableListAdapter = new OrganizationListAdapter_(
							organizationArrayList, SearchOrganization_.this);
					expandableListView.setAdapter(baseExpandableListAdapter);
					baseExpandableListAdapter.notifyDataSetChanged();

				} else {

					Iterator<Map.Entry<String, String>> iter = hashmapAllInterest
							.entrySet().iterator();
					while (iter.hasNext()) {
						Map.Entry<String, String> entry = iter.next();
						if (entry.getValue().equalsIgnoreCase(
								interestSpinner.getSelectedItem().toString())) {
							String key_you_look_for = entry.getKey();
							// Toast.makeText(SearchOrganization.this,
							// "The id of Interest is  "+ key_you_look_for,
							// Toast.LENGTH_SHORT).show();
							for (int i = 0; i < organizationArrayList.size(); i++) {
								String interestOrganization = "";
								interestOrganization = organizationArrayList
										.get(i).get("interests");
								if (interestOrganization.length() > 2) {
									String StringtoPass = interestOrganization
											.substring(1, interestOrganization
													.length() - 1);
									if (!StringtoPass.equals("")
											| !StringtoPass
													.equalsIgnoreCase(null)) {
										ArrayList<String> arrayList = new ArrayList<String>();
										Collections.addAll(arrayList,
												StringtoPass.split(","));
										for (int j = 0; j < arrayList.size(); j++) {
											if (arrayList.get(j).equals(
													key_you_look_for)) {
												array_sort_spinner
														.add(organizationArrayList
																.get(i));
											}
											// break;
										}
									}
								}
							}
							baseExpandableListAdapter = new OrganizationListAdapter_(
									array_sort_spinner,
									SearchOrganization_.this);
							expandableListView
									.setAdapter(baseExpandableListAdapter);
							baseExpandableListAdapter.notifyDataSetChanged();

						}
					}
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		// Filter the list of Organization on text change in Edittext
		array_sort = new ArrayList<HashMap<String, String>>();
		filterOrgs = (EditText) findViewById(R.id.searchOrganizationEditText);
		filterOrgs.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				int length = filterOrgs.getText().length();

				if (length > 0) {
					array_sort.clear();
					// for the first time array_sort_spinner will be of size 0
					// untill we change by selecting a value from spinner
					if (array_sort_spinner.size() > 0) {
						for (int i = 0; i < array_sort_spinner.size(); i++) {
							String orgValue = array_sort_spinner.get(i)
									.get("value").toLowerCase();
							if (orgValue.contains(filterOrgs.getText()
									.toString().toLowerCase())) {
								array_sort.add(array_sort_spinner.get(i));
								baseExpandableListAdapter = new OrganizationListAdapter_(
										array_sort, SearchOrganization_.this);
								expandableListView
										.setAdapter(baseExpandableListAdapter);
								baseExpandableListAdapter
										.notifyDataSetChanged();

							}
						}
					} else {
						for (int i = 0; i < organizationArrayList.size(); i++) {
							String orgValue = organizationArrayList.get(i)
									.get("value").toLowerCase();
							if (orgValue.contains(filterOrgs.getText()
									.toString().toLowerCase())) {
								array_sort.add(organizationArrayList.get(i));
								baseExpandableListAdapter = new OrganizationListAdapter_(
										array_sort, SearchOrganization_.this);
								expandableListView
										.setAdapter(baseExpandableListAdapter);
								baseExpandableListAdapter
										.notifyDataSetChanged();

							}
						}
					}

				} else {
					if (array_sort_spinner.size() > 0)
						baseExpandableListAdapter = new OrganizationListAdapter_(
								array_sort_spinner, SearchOrganization_.this);
					else
						baseExpandableListAdapter = new OrganizationListAdapter_(
								organizationArrayList, SearchOrganization_.this);
					expandableListView.setAdapter(baseExpandableListAdapter);
					baseExpandableListAdapter.notifyDataSetChanged();

				}
			}
		});
		findViewById(R.id.tabSearchLinearLayout).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent startSearchScreen = new Intent(
								SearchOrganization_.this, SearchScreen_.class);
						startActivity(startSearchScreen);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);
						finish();
					}
				});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btBack:
			Intent i = new Intent(this, SearchScreen_.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();
			break;
		case R.id.btLogout:
			Intent intent = new Intent(SearchOrganization_.this,
					MyProfile_.class);
			startActivity(intent);
			finish();
			break;
		default:
			break;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		{

			if (v.getTag() != null) {
				// @SuppressWarnings("unused")
				// HashMap<String, String> organizationHashmap =
				// (HashMap<String, String>) v.getTag();
				String OrganizationName = "";
				System.out.println("Organization " + v.getTag());
				for (int i = 0; i < organizationArrayList.size(); i++) {
					HashMap<String, String> hm = organizationArrayList.get(i);
					if (hm.get("id").equalsIgnoreCase(v.getTag() + "")) {
						OrganizationName = hm.get("value");
						break;
					}
				}

				Intent callOrganizationDetail = new Intent(
						SearchOrganization_.this, OrganizationDetail_.class);
				callOrganizationDetail.putExtra("organizationId", v.getTag()
						+ "");
				callOrganizationDetail.putExtra("from", "SearchOrganization");
				callOrganizationDetail.putExtra("organizationName",
						OrganizationName);
				BaseUrl.TAB2_STACK.add(this);
				startActivity(callOrganizationDetail);
				overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
			return false;
		}
	}

	// private void initFingerScrolling() {
	// ((SectionListIndexerScroller)
	// findViewById(R.id.sectionListIndexerScroller))
	// .setSectionIndexChangeInterface(new SectionIndexChangeInterface() {
	// @Override
	// public boolean onSectionIndexChange(
	// SectionListIndexerScroller sectionListIndexerScroller,
	// int newIndex) {
	//
	// int sectionIndex = ((OrganizationListAdapter_) baseExpandableListAdapter)
	// .getSectionManagerAdapter()
	// .getSectionByIndexIdentifier(
	// sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex));
	//
	// Log.e("iden",
	// sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex) + "");
	// Log.e("iden pos", sectionIndex + "");
	// if ((sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex) + "")
	// .equalsIgnoreCase("R")) {
	// expandableListView.setSelectedGroup(16);
	// return false;
	// }
	// if ((sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex) + "")
	// .equalsIgnoreCase("S")) {
	// expandableListView.setSelectedGroup(17);
	// return false;
	// }
	// if ((sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex) + "")
	// .equalsIgnoreCase("T")) {
	// expandableListView.setSelectedGroup(18);
	// return false;
	// }
	// if ((sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex) + "")
	// .equalsIgnoreCase("U")) {
	// expandableListView.setSelectedGroup(19);
	// return false;
	// }
	// if ((sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex) + "")
	// .equalsIgnoreCase("W")) {
	// expandableListView.setSelectedGroup(20);
	// return false;
	// }
	// if ((sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex) + "")
	// .equalsIgnoreCase("Y")) {
	// expandableListView.setSelectedGroup(21);
	// return false;
	// }
	// if ((sectionListIndexerScroller
	// .getIdentifierOfSection(newIndex) + "")
	// .equalsIgnoreCase("#")) {
	// expandableListView.setSelectedGroup(22);
	// return false;
	// }
	// if (sectionIndex != -1)
	// expandableListView.setSelectedGroup(sectionIndex);
	//
	// return false;
	// }
	// });
	// }

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent i = new Intent(this, SearchScreen_.class);
		startActivity(i);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}

	public class AsynchLoadData extends AsyncTask<Void, Void, String> {
		private ArrayList<String> interestArraySorted;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			DesignUtil.showProgressDialog(SearchOrganization_.this);
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			// Get the Json array of Interest
			interestArray = new ArrayList<String>();
			interestArraySorted = new ArrayList<String>();
			interestArray.add("All Interests");
			for (int i = 0; i < bu.AllInterestArray.length(); i++) {
				try {
					JSONObject jobjInterest = bu.AllInterestArray
							.getJSONObject(i);
					// System.out.println(""+ jobjInterest);
					hashmapAllInterest.put(jobjInterest.getString("id"),
							jobjInterest.getString("value"));
					interestArraySorted.add(jobjInterest.getString("value"));

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Collections.sort(interestArraySorted);
			interestArray.addAll(interestArraySorted);
			// Get the Json array of Organizations

			organizationArrayList = new ArrayList<HashMap<String, String>>();
			for (int i = 0; i < bu.AllOrganizationArray.length(); i++) {
				try {
					JSONObject jobjOrganization = bu.AllOrganizationArray
							.getJSONObject(i);
					// System.out.println("here is details of each Organization "+
					// jobjOrganization);
					HashMap<String, String> organizationHashMap = new HashMap<String, String>();
					organizationHashMap.put("id",
							jobjOrganization.getString("id"));
					String organizationTitle = jobjOrganization.getString(
							"value").toString();
					char[] stringArray = organizationTitle.toCharArray();
					stringArray[0] = Character.toUpperCase(stringArray[0]);
					organizationTitle = new String(stringArray);

					organizationHashMap.put("value", organizationTitle);
					organizationHashMap.put("interests",
							jobjOrganization.getString("interests"));
					organizationArrayList.add(organizationHashMap);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// System.out.println("Total organizations are "+
			// organizationArrayList.size());

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			DesignUtil.hideProgressDialog();
			baseExpandableListAdapter = new OrganizationListAdapter_(
					organizationArrayList, SearchOrganization_.this);
			expandableListView.setAdapter(baseExpandableListAdapter);

			MySpinnerAdapter spinnerArrayAdapter = new MySpinnerAdapter(
					SearchOrganization_.this, R.layout.spinner_layout,
					interestArray, "PtSansWebRegular.TTF");
			spinnerArrayAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The
																								// drop
																								// down
																								// vieww
			interestSpinner.setAdapter(spinnerArrayAdapter);
		}

	}

}

package com.facebook.android;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.util.Log;

import com.facebook.android.AsyncFacebookRunner.RequestListener;

// TODO: Auto-generated Javadoc
/**
 * Skeleton base class for RequestListeners, providing default error
 * handling. Applications should handle these error conditions.
 *
 * @see BaseRequestEvent
 */
public abstract class BaseRequestListener implements RequestListener {

    /* (non-Javadoc)
     * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onFacebookError(com.facebook.android.FacebookError)
     */
    public void onFacebookError(FacebookError e) {
        Log.e("Facebook", e.getMessage());
        e.printStackTrace();
    }

    /* (non-Javadoc)
     * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onFileNotFoundException(java.io.FileNotFoundException)
     */
    public void onFileNotFoundException(FileNotFoundException e) {
        Log.e("Facebook", e.getMessage());
        e.printStackTrace();
    }

    /* (non-Javadoc)
     * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onIOException(java.io.IOException)
     */
    public void onIOException(IOException e) {
        Log.e("Facebook", e.getMessage());
        e.printStackTrace();
    }

    /* (non-Javadoc)
     * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onMalformedURLException(java.net.MalformedURLException)
     */
    public void onMalformedURLException(MalformedURLException e) {
        Log.e("Facebook", e.getMessage());
        e.printStackTrace();
    }
    
}

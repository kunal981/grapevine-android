package com.twitter.android;

import java.io.File;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cuelogic.grapevine.R;
import com.twitter.android.TwitterApp.TwDialogListener;

@SuppressLint("HandlerLeak")
public class TwitterDirectPost {

	Context context;
	String review;
	String message;
	private TwitterApp mTwitter;
	private static final String twitter_consumer_key = "OodQpFldpIcrtQFHOhS1g";
	private static final String twitter_secret_key = "9Cmb7vdHW774tF3sTkaM2BmJeA9d5XYvSbOcMWYDxg";
	public File f;
	public Dialog dialog;
	public String url_to_attach;

	public TwitterDirectPost(Context cont, String event_url) {

		this.context = cont;
		this.url_to_attach = event_url;
		// this.message=msge;

		/*
		 * review = "Can you help me guess this logo?" +
		 * "If you like guessing NFL logos and more, " +
		 * "then LOGOmania: Visual Quizzes is the game for you. " ;
		 */

		mTwitter = new TwitterApp(cont, twitter_consumer_key,
				twitter_secret_key);
		mTwitter.setListener(mTwLoginDialogListener);
		// System.out.println("The length of message is " +
		// this.message.length());

		if (mTwitter.hasAccessToken())
			showTweetDialog();
		// postToTwitter(message);
		else
			mTwitter.authorize();
	}

	void postToTwitter(final String review, final String url_to_attach) {
		new Thread() {
			public void run() {
				int what = 0;
				try {
					TwitterSession twitterSession = new TwitterSession(context);
					AccessToken accessToken = twitterSession.getAccessToken();
					Configuration conf = new ConfigurationBuilder()
							.setOAuthConsumerKey(twitter_consumer_key)
							.setOAuthConsumerSecret(twitter_secret_key)
							.setOAuthAccessToken(accessToken.getToken())
							.setOAuthAccessTokenSecret(
									accessToken.getTokenSecret()).build();
					Twitter t = new TwitterFactory(conf).getInstance();

					// Bitmap b =
					// CacheStore.getInstance().isAvailableOnSDcard(url);
					Bitmap b = null;
					try {
						if (b != null) {
						} else
							t.updateStatus(review + " " + url_to_attach);
					} catch (TwitterException e) {
						e.printStackTrace();
					}
					Log.d("*********", "Updating status++++++++");
				} catch (Exception e) {
					what = 1;
				}
				mHandler.sendMessage(mHandler.obtainMessage(what));
			}
		}.start();
	}

	public void uploadPic(File file, String message, Twitter twitter)
			throws Exception {
		try {
			StatusUpdate status = new StatusUpdate(message);
			status.setMedia(file);
			twitter.updateStatus(status);
		} catch (TwitterException e) {
			Log.d("TAG", "Pic Upload error" + e.getErrorMessage());
			throw e;
		}
	}

	/**
	 * This example shows how to connect to Twitter, display authorization
	 * dialog and save user's token and username
	 */

	private final TwDialogListener mTwLoginDialogListener = new TwDialogListener() {
		public void onComplete(String value) {
			showTweetDialog();
			// postToTwitter(message);
		}

		public void onError(String value) {

		}
	};

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			// f.delete();
			String text = (msg.what == 0) ? "Tweet posted successfully"
					: "Post to Twitter failed";
			mTwitter.resetAccessToken();
			Toast.makeText(context, "Tweet posted successfully",
					Toast.LENGTH_SHORT).show();
			dialog.dismiss();
		}
	};

	public void showTweetDialog() {
		dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.tweet_dialog);
		dialog.setTitle("");
		final Button postTweet = (Button) dialog.findViewById(R.id.sendBtn);
		final TextView counterTv = (TextView) dialog
				.findViewById(R.id.counterTV);
		final EditText tweetMsg = (EditText) dialog
				.findViewById(R.id.tweetEdit);
		tweetMsg.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				int counterToShow = 118 - tweetMsg.getText().toString().trim()
						.length();
				counterTv.setText(counterToShow + "");
				if (counterToShow < 0) {
					postTweet.setEnabled(false);
				} else {
					postTweet.setEnabled(true);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
			}

		});
		// set the custom dialog components - text, image and button
		Button cancelTweet = (Button) dialog.findViewById(R.id.cancelBtn);
		// if button is clicked, close the custom dialog
		cancelTweet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		// if button is clicked, close the custom dialog
		postTweet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				postToTwitter(tweetMsg.getText().toString(), url_to_attach);
			}
		});

		dialog.show();
	}
}
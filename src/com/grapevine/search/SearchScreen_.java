package com.grapevine.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.cuelogic.grapevine.R;
import com.grapevine.organization.SearchOrganization_;
import com.grapevine.recommended.Recommended_;
import com.socketconnection.BaseUrl;

public class SearchScreen_ extends Activity implements OnClickListener {

	private EditText searchText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		BaseUrl.TAB2_STACK.add(this);
		setContentView(R.layout.search_screen_);
		findViewById(R.id.viewRecommendationLinearLayout).setOnClickListener(
				this);
		findViewById(R.id.neighborhoodEventsLinearLayout).setOnClickListener(
				this);
		findViewById(R.id.searchOrganizationLinearLayout).setOnClickListener(
				this);
		findViewById(R.id.searchByDateLinearLayout).setOnClickListener(this);
		((com.grapevine.views.Header_) findViewById(R.id.header))
				.ConfigureHeaderPane(false, false);
		((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
				.ConfigureTabBarPane(false, true, false, false);
		findViewById(R.id.tabSearchLinearLayout).setEnabled(false);
		((TextView) findViewById(R.id.txt_headertitle))
				.setText("SEARCH EVENTS");
		searchText = (EditText) findViewById(R.id.searchAllOpportitiesEditText);
		searchText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
				if ((arg2 != null && (arg2.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (arg1 == EditorInfo.IME_ACTION_DONE)) {
					// Toast.makeText(getApplicationContext(), "Call API",
					// Toast.LENGTH_SHORT).show();
					Intent i = new Intent(SearchScreen_.this,
							SearchResult_.class);
					i.putExtra("filterTerm", searchText.getText().toString()
							.trim());
					startActivity(i);
					overridePendingTransition(R.anim.slide_in_right,
							R.anim.slide_out_left);
					finish();
					// Toast.makeText(getApplicationContext(),
					// "Under development", Toast.LENGTH_SHORT).show();
				}
				return false;
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.viewRecommendationLinearLayout:
			BaseUrl.TAB2_LAST_ACTIVITY = this.getClass();
			System.out.println("The activity name stored is "
					+ BaseUrl.TAB2_LAST_ACTIVITY);
			Intent startViewRecommendScreen = new Intent(this,
					ViewRecommendation_.class);
			startActivity(startViewRecommendScreen);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
			// Toast.makeText(getApplicationContext(), "Under development..",
			// Toast.LENGTH_SHORT).show();
			break;
		case R.id.neighborhoodEventsLinearLayout:
			BaseUrl.TAB2_LAST_ACTIVITY = this.getClass();
			System.out.println("The activity name stored is "
					+ BaseUrl.TAB2_LAST_ACTIVITY);
			Intent startViewNeighborhoodEvent = new Intent(this,
					NeighborhoodEvent_.class);
			BaseUrl.TAB2_STACK.add(this);
			startActivity(startViewNeighborhoodEvent);

			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
			// Toast.makeText(getApplicationContext(), "Under development",
			// Toast.LENGTH_SHORT).show();

			break;
		case R.id.searchOrganizationLinearLayout:
			BaseUrl.TAB2_LAST_ACTIVITY = this.getClass();
			System.out.println("The activity name stored is "
					+ BaseUrl.TAB2_LAST_ACTIVITY);
			Intent startSearchOrganizationScreen = new Intent(this,
					SearchOrganization_.class);
			BaseUrl.TAB2_STACK.add(this);
			startActivity(startSearchOrganizationScreen);

			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
			// Toast.makeText(getApplicationContext(), "Under development",
			// Toast.LENGTH_SHORT).show();

			break;

		case R.id.searchByDateLinearLayout:
			BaseUrl.TAB2_LAST_ACTIVITY = this.getClass();
			System.out.println("The activity name stored is "
					+ BaseUrl.TAB2_LAST_ACTIVITY);
			Intent startSearchbyDateScreen = new Intent(this,
					SearchByDate_.class);
			BaseUrl.TAB2_STACK.add(this);
			startActivity(startSearchbyDateScreen);

			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
			// Toast.makeText(getApplicationContext(), "Under development",
			// Toast.LENGTH_SHORT).show();

			break;
		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (BaseUrl.TAB2_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB2_STACK.size(); i++) {
				Activity act = BaseUrl.TAB2_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB1_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB1_STACK.size(); i++) {
				Activity act = BaseUrl.TAB1_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB3_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB3_STACK.size(); i++) {
				Activity act = BaseUrl.TAB3_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB4_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB4_STACK.size(); i++) {
				Activity act = BaseUrl.TAB4_STACK.get(i);
				act.finish();
			}
		}
		BaseUrl.TAB2_LAST_ACTIVITY = this.getClass();
		Intent i = new Intent(this, Recommended_.class);
		startActivity(i);
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		finish();
	}
}

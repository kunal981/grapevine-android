package com.grapevine.recommended;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.cuelogic.grapevine.R;
import com.grapevine.search.SearchByDate_;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;

public class RegisterForEvent_ extends Activity implements OnClickListener {

	private Button detailsBtn;
	private WebView registerEventWebview;

	private String fromScreen, registrationUrl;
	HashMap<String, String> eventDetailhashMap;

	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.register_event_);

		DesignUtil.showProgressDialog(RegisterForEvent_.this);
		Intent receivedIntent = getIntent();
		fromScreen = receivedIntent.getStringExtra("from");

		if (fromScreen.equalsIgnoreCase("recommended")) {
			((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
					.ConfigureTabBarPane(true, false, false, false);
		}
		registrationUrl = receivedIntent.getStringExtra("url");
		eventDetailhashMap = (HashMap<String, String>) receivedIntent
				.getSerializableExtra("EventDetail");
		registerEventWebview = (WebView) findViewById(R.id.registerEventWebview);

		registerEventWebview.getSettings().setJavaScriptEnabled(true);
		registerEventWebview.getSettings().setBuiltInZoomControls(true);
		registerEventWebview.getSettings().setLoadWithOverviewMode(true);
		registerEventWebview.getSettings().setUseWideViewPort(true);
		if (registrationUrl.equalsIgnoreCase("null")) {

			DesignUtil.hideProgressDialog();
			AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
			builder1.setTitle("No Registration page");
			builder1.setMessage("This event does not have any registration page.");
			builder1.setCancelable(true);
			builder1.setNeutralButton(android.R.string.ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
							finish();
						}
					});

			AlertDialog alert11 = builder1.create();
			alert11.requestWindowFeature(Window.FEATURE_NO_TITLE);
			alert11.show();

		} else {
			String url = null;
			// if (registrationUrl.contains("www.grape-vine.com")) {
			// url = String.valueOf(eventDetailhashMap.get("registration_url")
			// + "&UserKey=" + BaseUrl.USER_KEY);
			// } else {
			// }
			// Log.e("url", url);

			url = registrationUrl;
			registerEventWebview.loadUrl(url);
			registerEventWebview.setWebViewClient(new WebViewClient() {
				@Override
				public void onPageFinished(WebView view, String url) {
					DesignUtil.hideProgressDialog();
				}
			});
			registerEventWebview.setInitialScale(50);
			registerEventWebview
					.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		}

		((Button) findViewById(R.id.btBack)).setOnClickListener(this);
		// findViewById(R.id.tabRecommendedLinearLayout).setOnClickListener(
		// new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// Intent startRecommendedScreen = new Intent(
		// RegisterForEvent_.this, Recommended_.class);
		// startRecommendedScreen
		// .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
		// | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		// startActivity(startRecommendedScreen);
		// overridePendingTransition(R.anim.slide_in_left,
		// R.anim.slide_out_right);
		// finish();
		//
		// }
		// });
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btBack:
			backButtonHandle();
			break;

		default:
			break;
		}

	}

	public void backButtonHandle() {
		if (fromScreen.equalsIgnoreCase("recommended")) {
			Intent backToEventDetails = new Intent(RegisterForEvent_.this,
					EventDetail_.class);
			backToEventDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			backToEventDetails.putExtra("EventDetail", eventDetailhashMap);
			backToEventDetails.putExtra("IsRegistered", "check");
			backToEventDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(backToEventDetails);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();
		} else if (fromScreen.contains("com.grapevine")) {
			// else if(fromScreen.equalsIgnoreCase("search")){
			Intent backToEventDetails = new Intent(RegisterForEvent_.this,
					EventDetail_.class);
			backToEventDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			backToEventDetails.putExtra("EventDetail", eventDetailhashMap);
			backToEventDetails.putExtra("IsRegistered", "check");
			backToEventDetails.putExtra("From", fromScreen);
			startActivity(backToEventDetails);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();
		} else if (fromScreen.equalsIgnoreCase("myevent")) {
			Intent backToEventDetails = new Intent(RegisterForEvent_.this,
					EventDetail_.class);
			backToEventDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			backToEventDetails.putExtra("EventDetail", eventDetailhashMap);
			backToEventDetails.putExtra("IsRegistered", "check");
			startActivity(backToEventDetails);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();
		}
	}

	@Override
	public void onBackPressed() {
		backButtonHandle();
		// super.onBackPressed();
	}

}

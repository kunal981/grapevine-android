package com.cuelogic.grapevine;

/* A class which is use to see Login/Register screen.
 * @author Cuelogic Technologies
 */
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.grapevine.interest.Intersest_;
import com.grapevine.recommended.Recommended_;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class RegisterActivity extends Activity implements OnClickListener {
	SocketConnection sc;
	BaseUrl bu;
	private EditText email, password, confirmPassword, age, zipCode;
	private Button register;
	private TextView errorTextview;
	private ProgressDialog pd;
	private SharedPreferences shared;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		email = (EditText) findViewById(R.id.emailEdit);
		password = (EditText) findViewById(R.id.passwordEdit);
		confirmPassword = (EditText) findViewById(R.id.confirmpasswordEdit);
		age = (EditText) findViewById(R.id.ageEdit);
		zipCode = (EditText) findViewById(R.id.zipcodeEdit);
		register = (Button) findViewById(R.id.registerBtn);
		errorTextview = (TextView) findViewById(R.id.ErrorMessage);
		register.setOnClickListener(this);
		sc = new SocketConnection(this);
		bu = new BaseUrl();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.registerBtn:
			boolean isValid = true;
			if (!checkValidEmail(email.getText().toString())) {
				errorTextview.setVisibility(View.VISIBLE);
				errorTextview.setText("Invalid Email address");
				isValid = false;
			}
			if (password.getText().length() < 8) {
				errorTextview.setVisibility(View.VISIBLE);
				errorTextview.setText("Password must be at least 8 characters");
				isValid = false;
			}
			if (!password.getText().toString()
					.equalsIgnoreCase(confirmPassword.getText().toString())) {
				errorTextview.setVisibility(View.VISIBLE);
				errorTextview.setText("Passwords must match");
				isValid = false;
			}
			if (age.getText().length() < 1 && age.getText().length() > 3) {
				errorTextview.setVisibility(View.VISIBLE);
				errorTextview.setText("Age must be entered");
				isValid = false;
			}
			if (zipCode.getText().length() < 5) {
				errorTextview.setVisibility(View.VISIBLE);
				errorTextview.setText("Zip Code must be 5 numbers long");
				isValid = false;
			}
			// To hide the key-board.
			if (isValid) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(zipCode.getWindowToken(), 0);
				// Call to Registration service.
				new AsynchRegister().execute();
				break;
			}
		default:
			break;
		}
	}

	/*
	 * An Email validation function which checks the entered email is a valid
	 * email or not. returns true if valid , false if invalid
	 */
	protected boolean checkValidEmail(String email) {
		boolean isValid = false;
		// String PATTERN =
		// "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(email);
		isValid = matcher.matches();
		return isValid;
	}

	/*
	 * A user Login API is called.
	 */

	/*
	 * A register user API called.
	 */
	public class AsynchRegister extends AsyncTask<Void, Void, String> {
		String s;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// pd = ProgressDialog
			// .show(RegisterActivity.this, "", "Please Wait..");
			DesignUtil.showProgressDialog(RegisterActivity.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				JSONObject registerJson = new JSONObject();
				try {
					registerJson.put("type", "new");
					registerJson.put("password", password.getText().toString()
							.trim());
					registerJson
							.put("email", email.getText().toString().trim());
					registerJson.put("age", age.getText().toString().trim());
					registerJson.put("offer_postalcode", zipCode.getText()
							.toString().trim());
					s = sc.sendJSONPost(
							BaseUrl.Development_Uri
									+ bu.getMethodName("REGISTER")
									+ BaseUrl.Api_key, registerJson);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return s;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			DesignUtil.hideProgressDialog();
			if (s != null) {
				try {
					JSONObject registerRespone = new JSONObject(result);
					bu.UserInfo = registerRespone;
					bu.USER_KEY = registerRespone.getString("user_key");
					Editor editor = getSharedPreferences(bu.KEY,
							Context.MODE_PRIVATE).edit();
					editor.putString("USER_KEY",
							registerRespone.getString("user_key"));
					editor.commit();
					int KEY_STATUS = registerRespone.getInt("key_status");
					int is_new = registerRespone.getInt("is_new");
					String InterestALL = registerRespone
							.getString("interests_all");
					String CommunitiesALL = registerRespone
							.getString("target_communities_all");
					// Put all the Communities,Neighborhoods and Selected
					// community Location at one place.
					bu.parentCommunityArray = new JSONArray();
					if (!(registerRespone.getString("communities_all")
							.equals("null"))) {
						bu.parentCommunityArray = registerRespone
								.getJSONArray("communities_all");
					}
					bu.neighborhoodCommunityAllArray = new JSONArray();
					if (!(registerRespone.getString("neighborhoods_all")
							.equals("null"))) {
						bu.neighborhoodCommunityAllArray = registerRespone
								.getJSONArray("neighborhoods_all");
					}
					bu.CommunityID = registerRespone.getString("community_id");
					bu.AllInterestArray = new JSONArray();
					if (!(registerRespone.getString("interests_all")
							.equals("null")))
						bu.AllInterestArray = registerRespone
								.getJSONArray("interests_all");
					bu.AllOrganizationArray = new JSONArray();
					if (!(registerRespone.getString("organizations_all")
							.equals("null")))
						bu.AllOrganizationArray = registerRespone
								.getJSONArray("organizations_all");
					bu.AllCommunitiesArray = new JSONArray();
					if (!(registerRespone.getString("target_communities_all")
							.equals("null")))
						bu.AllCommunitiesArray = registerRespone
								.getJSONArray("target_communities_all");
					bu.checkedINTEREST = new ArrayList<String>();
					if (!registerRespone.isNull("interests")) {
						JSONArray userSelectedInterest = registerRespone
								.getJSONArray("interests");
						if (userSelectedInterest.length() > 0) {
							for (int i = 0; i < userSelectedInterest.length(); i++) {
								bu.checkedINTEREST.add(userSelectedInterest
										.getString(i));
							}
						}
					}
					bu.checkedCOMMUNITIES = new ArrayList<String>();
					if (!registerRespone.isNull("target_communities")) {
						JSONArray userSelectedCommunities = registerRespone
								.getJSONArray("target_communities");
						if (userSelectedCommunities.length() > 0) {
							for (int i = 0; i < userSelectedCommunities
									.length(); i++) {
								bu.checkedCOMMUNITIES
										.add(userSelectedCommunities
												.getString(i));
							}
						}
					}

					errorTextview.setVisibility(View.GONE);
					email.setText("");
					password.setText("");
					confirmPassword.setText("");
					age.setText("");
					zipCode.setText("");
					if (KEY_STATUS == 1) {
						Intent call_loginRegister = new Intent(
								RegisterActivity.this, Intersest_.class);
						call_loginRegister.putExtra("allInterest",
								bu.AllInterestArray.toString());
						call_loginRegister.putExtra("allCommunities",
								bu.AllCommunitiesArray.toString());
						startActivity(call_loginRegister);
						finish();
					} else {
						Intent call_loginRegister = new Intent(
								RegisterActivity.this,
								ThanksRegistrationScreen_.class);
						startActivity(call_loginRegister);
						overridePendingTransition(R.anim.slide_in_right,
								R.anim.slide_out_left);
						finish();
					}

					// if (InterestALL.equalsIgnoreCase("null")
					// && CommunitiesALL.equalsIgnoreCase("null")) {
					// Intent call_loginRegister = new Intent(
					// RegisterActivity.this,
					// ThanksRegistrationScreen_.class);
					// startActivity(call_loginRegister);
					// overridePendingTransition(R.anim.slide_in_right,
					// R.anim.slide_out_left);
					// finish();
					// // Toast.makeText(LoginRegister.this,
					// // "Registaration done", Toast.LENGTH_SHORT).show();
					//
					// } else {
					// // Intent call_loginRegister = new Intent(
					// // RegisterActivity.this,
					// // ThanksRegistrationScreen_.class);
					// // startActivity(call_loginRegister);
					// // overridePendingTransition(R.anim.slide_in_right,
					// // R.anim.slide_out_left);
					// // finish();
					// Intent call_loginRegister = new Intent(
					// RegisterActivity.this, Intersest_.class);
					// call_loginRegister.putExtra("allInterest",
					// bu.AllInterestArray.toString());
					// call_loginRegister.putExtra("allCommunities",
					// bu.AllCommunitiesArray.toString());
					// startActivity(call_loginRegister);
					// finish();
					// }

				} catch (JSONException e) {
					e.printStackTrace();
					return;

				}
			} else {
				Toast.makeText(RegisterActivity.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
			System.out.println("" + result);
		}

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent i = new Intent(RegisterActivity.this, LoginRegister_.class);
		startActivity(i);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}
}

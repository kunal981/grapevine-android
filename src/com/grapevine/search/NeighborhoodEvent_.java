package com.grapevine.search;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cuelogic.grapevine.LocationChangeHandler;
import com.cuelogic.grapevine.R;
import com.grapevine.myprofile.MyProfile_;
import com.grapevine.ui.MySpinnerAdapter;
import com.grapevine.ui.SegmentedGroup;
import com.grapevine.ui.SponserImageView;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class NeighborhoodEvent_ extends Activity implements OnClickListener,
		RadioGroup.OnCheckedChangeListener {

	private Spinner neighborhoodSpinner;
	// To store Id - Value of all neighborhoods.We need tosend ID when we call
	// service.
	private HashMap<String, String> hashmapAllNeighborhood = new HashMap<String, String>();
	// Arraylist that will bind to the spinner.
	private ArrayList<String> neighbourhoods;
	private ListView neighborhoodEventsListView;
	private LayoutInflater mInflater;
	static SearchResultAdapter_ searchResultAdapter;
	ArrayList<HashMap<String, String>> neighborhoodEventList;
	// LinearLayout footerRel;
	LinearLayout noItemFoundLinear;

	SocketConnection sc;
	BaseUrl bu;
	String latitude, longitude;

	RadioButton radiobtn1, radiobtn2;
	SegmentedGroup segmentbutton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		BaseUrl.TAB2_STACK.add(this);
		setContentView(R.layout.neighborhood_events_);
		((com.grapevine.views.Header_) findViewById(R.id.header))
				.ConfigureHeaderPane(true, true);
		((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
				.ConfigureTabBarPane(false, true, false, false);
		((TextView) findViewById(R.id.txt_headertitle))
				.setText("EVENTS BY NEIGHBORHOOD");
		((Button) findViewById(R.id.btLogout)).setOnClickListener(this);
		mInflater = LayoutInflater.from(NeighborhoodEvent_.this);
		View footerView = mInflater.inflate(R.layout.footer_, null);
		SponserImageView imageview = (SponserImageView) footerView
				.findViewById(R.id.id_img_sponser_1);
		imageview.setSponserImage(Integer.valueOf(BaseUrl.CommunityID));

		neighborhoodSpinner = (Spinner) findViewById(R.id.neighborhoodSpinner);
		neighborhoodEventsListView = (ListView) findViewById(R.id.neighborhoodEventListview);

		segmentbutton = (SegmentedGroup) findViewById(R.id.segmentedbutton);
		segmentbutton.setTintColor(getResources().getColor(
				R.color.radio_button_selected_color));
		radiobtn1 = (RadioButton) findViewById(R.id.btnnearest);
		radiobtn2 = (RadioButton) findViewById(R.id.btnsoonest);
		segmentbutton.setOnCheckedChangeListener(this);
		neighborhoodEventsListView.addFooterView(footerView);
		// footerRel = (LinearLayout) findViewById(R.id.footerLinear);
		noItemFoundLinear = (LinearLayout) findViewById(R.id.NoSearchFoundLayout);
		findViewById(R.id.btBack).setOnClickListener(this);
		// footerRel.setVisibility(View.GONE);
		noItemFoundLinear.setVisibility(View.GONE);
		neighborhoodEventList = new ArrayList<HashMap<String, String>>();
		searchResultAdapter = new SearchResultAdapter_(this,
				neighborhoodEventList);
		sc = new SocketConnection(this);
		bu = new BaseUrl();
		turnGPSOn();
		LocationChangeHandler.setInstance(new LocationChangeHandler(
				getApplicationContext()));
		LocationChangeHandler.getInstance().beginUpdate();
		if (LocationChangeHandler.getLocation() != null) {
			latitude = LocationChangeHandler.getLocation().getLatitude() + "";
			longitude = LocationChangeHandler.getLocation().getLongitude() + "";
		} else {
			latitude = 35.74184050952353 + "";
			longitude = -118.8128372137487 + "";
		}
		// Default items are added here for spinner to show.
		neighbourhoods = new ArrayList<String>();
		neighbourhoods.add("Current Location");
		neighbourhoods.add("All Neighborhoods");
		for (int i = 0; i < bu.neighborhoodCommunityAllArray.length(); i++) {
			try {
				JSONObject jobjNeighborhood = bu.neighborhoodCommunityAllArray
						.getJSONObject(i);
				System.out.println("" + jobjNeighborhood);
				if (jobjNeighborhood.getString("parent_community")
						.equalsIgnoreCase(bu.CommunityID)) {
					hashmapAllNeighborhood.put(
							jobjNeighborhood.getString("id"),
							jobjNeighborhood.getString("value"));
					neighbourhoods.add(jobjNeighborhood.getString("value"));
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		MySpinnerAdapter spinnerArrayAdapter = new MySpinnerAdapter(this,
				R.layout.spinner_layout, neighbourhoods, "PtSansWebRegular.TTF");

		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The
																							// drop
																							// down
																							// vieww
		neighborhoodSpinner.setAdapter(spinnerArrayAdapter);
		neighborhoodSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// Toast.makeText(NeighborhoodEvent.this,
						// ""+neighborhoodSpinner.getSelectedItem(),
						// Toast.LENGTH_SHORT).show();
						if (neighborhoodSpinner.getSelectedItem().toString()
								.equalsIgnoreCase("Current Location")) {
							// Toast.makeText(NeighborhoodEvent.this,
							// "Send lat -lon", Toast.LENGTH_SHORT).show();
							new AsynchNeighborhoodList(bu.Development_Uri
									+ bu.getMethodName("RECOMMENDED")
									+ bu.USER_KEY
									+ bu.getMethodName("RECOMMENDED_EVENTS")
									+ bu.Api_key + "&type=nearby&lat="
									+ latitude + "&lon=" + longitude
									+ "&radius_meters=10000").execute();
						} else if (neighborhoodSpinner.getSelectedItem()
								.toString()
								.equalsIgnoreCase("All Neighborhoods")) {
							System.out.println("The community id is "
									+ bu.CommunityID);
							new AsynchNeighborhoodList(bu.Development_Uri
									+ bu.getMethodName("RECOMMENDED")
									+ bu.USER_KEY
									+ bu.getMethodName("RECOMMENDED_EVENTS")
									+ bu.Api_key + "&type=all&lat=" + latitude
									+ "&lon=" + longitude
									+ "&filter_community_id=" + bu.CommunityID)
									.execute();
						} else {

							Iterator<Map.Entry<String, String>> iter = hashmapAllNeighborhood
									.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, String> entry = iter.next();
								if (entry.getValue().equalsIgnoreCase(
										neighborhoodSpinner.getSelectedItem()
												.toString())) {
									String key_you_look_for = entry.getKey();
									// Toast.makeText(NeighborhoodEvent.this,
									// "The id to send is "+ key_you_look_for,
									// Toast.LENGTH_SHORT).show();
									new AsynchNeighborhoodList(
											bu.Development_Uri
													+ bu.getMethodName("RECOMMENDED")
													+ bu.USER_KEY
													+ bu.getMethodName("RECOMMENDED_EVENTS")
													+ bu.Api_key
													+ "&type=all&lat="
													+ latitude
													+ "&lon="
													+ longitude
													+ "&filter_neighborhood_id="
													+ key_you_look_for)
											.execute();
								}
							}
						}

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});
		findViewById(R.id.tabSearchLinearLayout).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent startSearchScreen = new Intent(
								NeighborhoodEvent_.this, SearchScreen_.class);
						startActivity(startSearchScreen);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);
						finish();
					}
				});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btBack:
			Intent i = new Intent(this, SearchScreen_.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			finish();
			break;
		case R.id.btLogout:
			Intent intent = new Intent(NeighborhoodEvent_.this,
					MyProfile_.class);
			startActivity(intent);
			finish();
			break;

		// case R.id.btn_nearest:
		// if (radiobtn1.isChecked()) {
		//
		// setListView(true);
		// } else {
		// setListView(false);
		// }
		// break;
		default:
			break;
		}

	}

	/*
	 * A function used to turn-on device GPS if it is off.
	 */
	public void turnGPSOn() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", true);
		this.sendBroadcast(intent);
		String provider = Settings.Secure.getString(this.getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) { // if gps is disabled

			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			this.sendBroadcast(poke);

		}
	}

	/*
	 * A function used to turn-off device GPS.
	 */
	public void turnGPSOff() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", false);
		this.sendBroadcast(intent);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		LocationChangeHandler.getInstance().stopUpdates();
		turnGPSOff();
	}

	// A service is called to get Neighborhood Events for Search tab.
	public class AsynchNeighborhoodList extends AsyncTask<Void, Void, String> {
		String s, Json_to_parse;
		String UrlString;

		public AsynchNeighborhoodList(String url) {
			this.UrlString = url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// pd = ProgressDialog.show(NeighborhoodEvent_.this, "",
			// "Please Wait..");
			DesignUtil.showProgressDialog(NeighborhoodEvent_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {

				InputStream ip = sc.getHttpStream(this.UrlString);
				Json_to_parse = sc.convertStreamToString(ip);
				parseJson(Json_to_parse);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// return Json_to_parse;
			return Json_to_parse;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd.dismiss();
			DesignUtil.hideProgressDialog();
			LocationChangeHandler.getInstance().stopUpdates();
			turnGPSOff();
			if (result != null)
				setListView(true);
			else
				Toast.makeText(NeighborhoodEvent_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			setListView(true);
		}

	}

	public void parseJson(String jsonString) {
		try {
			JSONObject recommendedJsonObject = new JSONObject(jsonString);
			// jsonFeatured = (JSONObject) jsonObject.get("sponsored");
			// JSONObject jsonRecommended = (JSONObject)
			// jsonObject.get("recommended");
			JSONArray arrayallEvents = recommendedJsonObject
					.getJSONArray("all");
			if (arrayallEvents.length() > 0) {
				neighborhoodEventList.clear();
				PutEventinHashmap(neighborhoodEventList, arrayallEvents);
			}

		} catch (JSONException e) {
		} catch (Exception e) {
		}
	}

	public void PutEventinHashmap(
			ArrayList<HashMap<String, String>> arrayToAdd, JSONArray jsonarray) {

		for (int i = 0; i < jsonarray.length(); i++) {
			try {
				JSONObject arrayElement = jsonarray.getJSONObject(i);
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("address_line_1",
						arrayElement.getString("address_line_1"));
				hash.put("city", arrayElement.getString("city"));
				hash.put("description", arrayElement.getString("description"));
				hash.put("distance", arrayElement.getString("distance"));
				hash.put("end_time", arrayElement.getString("end_time"));
				hash.put("start_time",
						arrayElement.getString("start_time_local"));
				// hash.put("start_time", arrayElement.getString("start_time"));
				hash.put("event_url", arrayElement.getString("event_url"));
				hash.put("public_url", arrayElement.getString("public_url"));
				hash.put("id", arrayElement.getString("id"));
				hash.put("image_url", arrayElement.getString("image_url"));
				hash.put("latitude", arrayElement.getString("latitude"));
				hash.put("location", arrayElement.getString("location"));
				hash.put("longitude", arrayElement.getString("longitude"));
				hash.put("postalcode", arrayElement.getString("postalcode"));
				hash.put("registration_deadline",
						arrayElement.getString("registration_deadline"));
				hash.put("registration_url",
						arrayElement.getString("registration_url"));
				hash.put("state", arrayElement.getString("state"));
				hash.put("teaser", arrayElement.getString("teaser"));
				hash.put("title", arrayElement.getString("title"));
				hash.put("user_feedback",
						arrayElement.getString("user_feedback"));
				hash.put("user_like", arrayElement.getString("user_like"));
				hash.put("user_not_like",
						arrayElement.getString("user_not_like"));
				hash.put("user_registered",
						arrayElement.getString("user_registered"));
				hash.put("user_score", arrayElement.getString("user_score"));
				JSONObject orgnazationObj = arrayElement
						.getJSONObject("organization");
				hash.put("orgnization", orgnazationObj.getString("name"));
				arrayToAdd.add(hash);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void setListView(boolean flag) {
		if (neighborhoodEventList.size() == 0) {
			// footerRel.setVisibility(View.VISIBLE);
			noItemFoundLinear.setVisibility(View.VISIBLE);
			neighborhoodEventsListView.setVisibility(View.GONE);
		} else {
			neighborhoodEventsListView.setVisibility(View.VISIBLE);
			if (flag) {
				Collections.sort(neighborhoodEventList,
						new Comparator<HashMap<String, String>>() {
							@Override
							public int compare(HashMap<String, String> lhs,
									HashMap<String, String> rhs) {
								if (lhs.get("start_time").length() > 0
										&& rhs.get("start_time").length() > 0) {
									if (Integer.parseInt(lhs.get("start_time")) < Integer
											.parseInt(rhs.get("start_time"))) {
										return -1;
									}
								}
								return 0;

							}
						});
			} else {
				Collections.sort(neighborhoodEventList,
						new Comparator<HashMap<String, String>>() {
							@Override
							public int compare(HashMap<String, String> lhs,
									HashMap<String, String> rhs) {
								if (lhs.get("distance").length() > 0
										&& rhs.get("distance").length() > 0
										&& !lhs.get("distance")
												.contains("null")
										&& !rhs.get("distance")
												.contains("null")) {
									if (Integer.parseInt(lhs.get("distance")) < Integer
											.parseInt(rhs.get("distance"))) {
										return -1;
									}
								}
								return 0;

							}
						});
			}
			neighborhoodEventsListView.setAdapter(searchResultAdapter);
			neighborhoodEventsListView.postInvalidate();
			searchResultAdapter.notifyDataSetChanged();
			// footerRel.setVisibility(View.GONE);
			noItemFoundLinear.setVisibility(View.GONE);
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent i = new Intent(this, SearchScreen_.class);
		startActivity(i);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		switch (checkedId) {
		case R.id.btnnearest:

			setListView(true);
			break;

		case R.id.btnsoonest:
			setListView(false);
			break;
		default:
			break;
		}
	}
}

package com.socketconnection;

/* A class which is use to maintain all Common data such as urls,methos names in url,user_key.
 * @author Cuelogic Technologies
 */
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

public class BaseUrl {

	public static String Development_Uri = "https://www.grape-vine.com/apiv1";// "http://jjcdev.repeatsys.com/apiv1";////"http://jjcdev.repeatsys.com/apiv1"
	// public static String Development_Uri =
	// "http://jjcdev.repeatsys.com/apiv1";//
	// //"http://jjcdev.repeatsys.com/apiv1"
	public static String Api_key = "?api_key=b1a3f4f6e13641dd921aa49b8b21816c";
	// public static final String APP_ID = "132127003621561";//
	// "5079f16a01ed85595f000004"
	// public static final String APP_ID = "967485719942456";//
	// "5079f16a01ed85595f000004" from brstdev17@gmail.com
	public static final String APP_ID = "807651385936915";// from realse
	public static final String KEY = "GrapevineData";
	public static String USER_KEY;
	public static HashMap<String, String> methodName;
	public static JSONArray neighborhoodCommunityAllArray;
	public static JSONArray parentCommunityArray;
	public static String CommunityID = "27";
	public static JSONArray AllInterestArray;
	public static JSONArray AllOrganizationArray;
	public static JSONArray RecommendedEvents;
	public static JSONArray FeaturedEvents;
	public static JSONArray AllCommunitiesArray;
	public static JSONArray AllEventsArray;
	// public ArrayList<Integer> bookmarkedEventId = new ArrayList<Integer>();
	/*
	 * Created class objects to maintain last opened class/activity from each
	 * tab. So, whenever user changes the tab form one to another,last tabs
	 * position class will be shown.
	 */
	public static Class TAB1_LAST_ACTIVITY = com.grapevine.recommended.Recommended_.class;
	public static Class TAB2_LAST_ACTIVITY = com.grapevine.search.SearchScreen_.class;
	public static Class TAB3_LAST_ACTIVITY = com.grapevine.myevent.MyEvents_.class;
	public static Class TAB4_LAST_ACTIVITY = com.grapevine.myprofile.MyProfile_.class;
	// This arraylist will be used to delete the all activities from perticular
	// tab, when user clicks on root tab
	public static ArrayList<Activity> TAB1_STACK = new ArrayList<Activity>();
	public static ArrayList<Activity> TAB2_STACK = new ArrayList<Activity>();
	public static ArrayList<Activity> TAB3_STACK = new ArrayList<Activity>();
	public static ArrayList<Activity> TAB4_STACK = new ArrayList<Activity>();
	// To maintain the List of selected Interest,Communities previously
	public static ArrayList<String> checkedINTEREST;
	public static ArrayList<String> checkedCOMMUNITIES;

	// Create a static variables to store the user profile information
	public static JSONObject UserInfo;

	/*
	 * We need to maintain all the bookmarked,registered events right from user
	 * logs in. Also we will keep the total number of events like and disliked
	 * by user in entire app.
	 */
	public static ArrayList<String> bookmarkedEventId = new ArrayList<String>();
	public static ArrayList<String> registeredEventId = new ArrayList<String>();
	public static ArrayList<HashMap<String, String>> bookmarkedEventList = new ArrayList<HashMap<String, String>>();
	public static ArrayList<HashMap<String, String>> registeredEventList = new ArrayList<HashMap<String, String>>();
	public static ArrayList<HashMap<String, String>> likedEventList = new ArrayList<HashMap<String, String>>();

	// public static ArrayList<HashMap<String, String>> allEventList = new
	// ArrayList<HashMap<String,String>>();

	public BaseUrl() {
		methodName = new HashMap<String, String>();
		methodName.put("EVENTS", "/events");
		methodName.put("LOGINFB", "/users");
		methodName.put("LOGIN", "/users");
		methodName.put("REGISTER", "/users");
		methodName.put("UPDATE_INTEREST_COMMUNITIES", "/users/");
		methodName.put("RECOMMENDED", "/users/");
		methodName.put("RECOMMENDED_EVENTS", "/events");
		methodName.put("ORGANIZATIONS", "/organizations/");
	}

	public String getMethodName(String method) {
		String methodNameinUrl = methodName.get(method);
		return methodNameinUrl;
	}

}

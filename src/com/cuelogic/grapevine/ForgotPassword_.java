package com.cuelogic.grapevine;

/* A class which is use to see Forgot Password screen.
 * @author Cuelogic Technologies
 */
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.grapevine.recommended.RegisterForEvent_;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class ForgotPassword_ extends Activity implements OnClickListener {

	SocketConnection sc;
	BaseUrl bu;
	private TextView forgotPasswordMessageTV;
	private EditText emailForgotPassword;
	private Button submitForgotPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.forgot_password_);
		forgotPasswordMessageTV = (TextView) findViewById(R.id.forgotpasswordMessageTV);
		emailForgotPassword = (EditText) findViewById(R.id.forgotEmailEdt);
		submitForgotPassword = (Button) findViewById(R.id.forgotPasswordSubmitButton);
		submitForgotPassword.setOnClickListener(this);
		sc = new SocketConnection(this);
		bu = new BaseUrl();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.forgotPasswordSubmitButton:
			new AsynchForgotPasswod().execute();
			break;

		default:
			break;
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent callLoginRegister = new Intent(ForgotPassword_.this,
				LoginRegister_.class);
		startActivity(callLoginRegister);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		finish();
	}

	/*
	 * A thread which send a forgot password request to server. 
	 */
	public class AsynchForgotPasswod extends AsyncTask<Void, Void, String> {
		String s;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			DesignUtil.showProgressDialog(ForgotPassword_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			JSONObject forgotPasswordJson = new JSONObject();
			try {
				forgotPasswordJson.put("link_prefix",
						"http://www.grape-vine.com/");
				s = sc.sendJSONPost(
						BaseUrl.Development_Uri
								+ bu.getMethodName("LOGIN")
								+ "/"
								+ emailForgotPassword.getText().toString()
										.trim() + "/recover" + BaseUrl.Api_key,
						forgotPasswordJson);
				return s;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return s;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			DesignUtil.hideProgressDialog();
			if (s != null) {
				if (s.contains("success")) {
					emailForgotPassword.setVisibility(View.INVISIBLE);
					submitForgotPassword.setVisibility(View.INVISIBLE);
					forgotPasswordMessageTV.setText(R.string.forgot_password2);

				} else {
					Toast.makeText(ForgotPassword_.this,
							"Invalid Email, Please Retry", Toast.LENGTH_SHORT)
							.show();
					emailForgotPassword.setText("");
				}
			} else {
				Toast.makeText(ForgotPassword_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

}

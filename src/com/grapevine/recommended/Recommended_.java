package com.grapevine.recommended;

/* A screen which shows First Tab selected with Featured and My Recommendations event list.
 * @author Cuelogic Technologies
 */
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cuelogic.grapevine.LocationChangeHandler;
import com.cuelogic.grapevine.R;
import com.grapevine.myprofile.MyProfile_;
import com.grapevine.ui.SponserImageView;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class Recommended_ extends Activity implements OnClickListener {

	ListView recommendations;
	ArrayList<HashMap<String, String>> featuredList, myRecommendationList;
	static RecommendedAdapter_ featuredAdapter, myRecommendaionAdapter;
	private ProgressDialog pd;
	SocketConnection sc;
	BaseUrl bu;
	private LayoutInflater mInflater;
	LinearLayout noItemFoundLinear;
	public static String COMMUNITY_ID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.my_recommended_screen_);
		BaseUrl.TAB1_STACK.add(this);
		((com.grapevine.views.Header_) findViewById(R.id.header))
				.ConfigureHeaderPane(true, false);
		((com.grapevine.views.TabBar_) findViewById(R.id.tabbar))
				.ConfigureTabBarPane(true, false, false, false);
		((TextView) findViewById(R.id.txt_headertitle))
				.setText("EXPLORE EVENTS");
		((Button) findViewById(R.id.btLogout))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						Intent intent = new Intent(Recommended_.this,
								MyProfile_.class);
						startActivity(intent);

					}
				});
		findViewById(R.id.tabRecommendedLinearLayout).setEnabled(false);
		bu = new BaseUrl();
		COMMUNITY_ID = bu.CommunityID;

		mInflater = LayoutInflater.from(Recommended_.this);
		View footerView = mInflater.inflate(R.layout.footer_, null);
		SponserImageView imageview = (SponserImageView) footerView
				.findViewById(R.id.id_img_sponser_1);
		imageview.setSponserImage(Integer.valueOf(COMMUNITY_ID));

		recommendations = (ListView) findViewById(R.id.recommendedListview);
		recommendations.addFooterView(footerView);
		noItemFoundLinear = (LinearLayout) findViewById(R.id.NoItemFoundLayout);
		noItemFoundLinear.setVisibility(View.GONE);
		featuredList = new ArrayList<HashMap<String, String>>();
		myRecommendationList = new ArrayList<HashMap<String, String>>();
		featuredAdapter = new RecommendedAdapter_(this, featuredList,
				"FEATURED");
		myRecommendaionAdapter = new RecommendedAdapter_(this,
				myRecommendationList, "MY RECOMMENDATIONS");
		sc = new SocketConnection(this);
		turnGPSOn();
		LocationChangeHandler.setInstance(new LocationChangeHandler(
				getApplicationContext()));
		LocationChangeHandler.getInstance().beginUpdate();
		new AsynchRecommendList().execute();
		new AsyncGetUserRegisteredEvents().execute();
	}

	// @Override
	// protected void onRestart() {
	// // TODO Auto-generated method stub
	// super.onRestart();
	// ("Came in onrestart of Recommended");
	// new AsynchRecommendList().execute();
	// }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		}
	}

	// A service is called to get Events for Recommended tab.
	public class AsynchRecommendList extends AsyncTask<Void, Void, String> {
		String s, lat, lon, Json_to_parse;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// pd = ProgressDialog.show(Recommended_.this, "", "Please Wait..");
			DesignUtil.showProgressDialog(Recommended_.this);
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				if (LocationChangeHandler.getLocation() != null) {
					lat = LocationChangeHandler.getLocation().getLatitude()
							+ "";
					lon = LocationChangeHandler.getLocation().getLongitude()
							+ "";
				} else {
					lat = 35.74184050952353 + "";
					lon = -118.8128372137487 + "";
				}
				InputStream ip = sc.getHttpStream(BaseUrl.Development_Uri
						+ bu.getMethodName("RECOMMENDED") + bu.USER_KEY
						+ bu.getMethodName("RECOMMENDED_EVENTS") + bu.Api_key
						+ "&type=sponsored,friends,recommended&lat=" + lat
						+ "&lon=" + lon + "&filter_community_id="
						+ bu.CommunityID);
				Json_to_parse = sc.convertStreamToString(ip);
				parseJson(Json_to_parse);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// return Json_to_parse;
			return Json_to_parse;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pd.dismiss();
			DesignUtil.hideProgressDialog();
			LocationChangeHandler.getInstance().stopUpdates();
			turnGPSOff();
			if (result != null) {
				RecommendedAdapterHeader_ adapter = new RecommendedAdapterHeader_(
						Recommended_.this, "FEATURED EVENTS",
						"MY RECOMMENDATIONS EVENTS");
				if (featuredList.size() > 0)
					adapter.addSection("FEATURED EVENTS", featuredAdapter);
				if (myRecommendationList.size() > 0)
					adapter.addSection("RECOMMENDED EVENTS",
							myRecommendaionAdapter);
				recommendations.setAdapter(adapter);
				recommendations.postInvalidate();
				featuredAdapter.notifyDataSetChanged();
				myRecommendaionAdapter.notifyDataSetChanged();
				adapter.notifyDataSetChanged();
				// recommendations.notifyAll();

				if (featuredList.size() == 0
						&& myRecommendationList.size() == 0) {
					noItemFoundLinear.setVisibility(View.VISIBLE);
					recommendations.setVisibility(View.GONE);
				}
			} else {
				Toast.makeText(Recommended_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	/*
	 * A function used to turn-on device GPS if it is off.
	 */
	public void turnGPSOn() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", true);
		this.sendBroadcast(intent);
		String provider = Settings.Secure.getString(this.getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (!provider.contains("gps")) { // if gps is disabled

			final Intent poke = new Intent();
			poke.setClassName("com.android.settings",
					"com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			this.sendBroadcast(poke);

		}
	}

	/*
	 * A function used to turn-off device GPS.
	 */
	public void turnGPSOff() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.putExtra("enabled", false);
		this.sendBroadcast(intent);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		LocationChangeHandler.getInstance().stopUpdates();
		turnGPSOff();
	}

	public void parseJson(String jsonString) {
		try {
			JSONObject recommendedJsonObject = new JSONObject(jsonString);
			// jsonFeatured = (JSONObject) jsonObject.get("sponsored");
			// JSONObject jsonRecommended = (JSONObject)
			// jsonObject.get("recommended");
			JSONArray arrayRecommnended = recommendedJsonObject
					.getJSONArray("recommended");
			bu.RecommendedEvents = new JSONArray();
			bu.RecommendedEvents = arrayRecommnended;
			JSONArray arraySponsered = recommendedJsonObject
					.getJSONArray("sponsored");
			bu.FeaturedEvents = new JSONArray();
			bu.FeaturedEvents = arraySponsered;
			if (arraySponsered.length() > 0) {
				PutEventinHashmap(featuredList, arraySponsered);
			}
			if (arrayRecommnended.length() > 0) {
				PutEventinHashmap(myRecommendationList, arrayRecommnended);
			}

		} catch (JSONException e) {
		} catch (Exception e) {
		}
	}

	public void PutEventinHashmap(
			ArrayList<HashMap<String, String>> arrayToAdd, JSONArray jsonarray) {

		for (int i = 0; i < jsonarray.length(); i++) {
			try {
				JSONObject arrayElement = jsonarray.getJSONObject(i);
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("address_line_1",
						arrayElement.getString("address_line_1"));
				hash.put("city", arrayElement.getString("city"));
				hash.put("description", arrayElement.getString("description"));
				hash.put("distance", arrayElement.getString("distance"));
				hash.put("end_time", arrayElement.getString("end_time"));
				// hash.put("start_time", arrayElement.getString("start_time"));
				hash.put("start_time",
						arrayElement.getString("start_time_local"));
				hash.put("event_url", arrayElement.getString("event_url"));
				String eventType = arrayElement.getString("eventtype") != null ? arrayElement
						.getString("eventtype") : "";
				hash.put("eventtype", eventType);
				hash.put("public_url", arrayElement.getString("public_url"));
				hash.put("id", arrayElement.getString("id"));
				hash.put("image_url", arrayElement.getString("image_url"));
				hash.put("latitude", arrayElement.getString("latitude"));
				hash.put("location", arrayElement.getString("location"));
				hash.put("longitude", arrayElement.getString("longitude"));
				hash.put("postalcode", arrayElement.getString("postalcode"));
				hash.put("registration_deadline",
						arrayElement.getString("registration_deadline"));
				hash.put("registration_url",
						arrayElement.getString("registration_url"));
				hash.put("state", arrayElement.getString("state"));
				hash.put("teaser", arrayElement.getString("teaser"));
				hash.put("title", arrayElement.getString("title"));
				hash.put("user_feedback",
						arrayElement.getString("user_feedback"));
				hash.put("user_like", arrayElement.getString("user_like"));
				hash.put("user_not_like",
						arrayElement.getString("user_not_like"));
				hash.put("user_registered",
						arrayElement.getString("user_registered"));

				hash.put("user_score", arrayElement.getString("user_score"));
				JSONObject orgnazationObj = arrayElement
						.getJSONObject("organization");
				hash.put("orgnization", orgnazationObj.getString("name"));
				hash.put("orgnizationid", orgnazationObj.getString("id"));

				if (arrayElement.getString("user_like").equalsIgnoreCase("1")) {
					boolean isFound = false;
					for (int k = 0; k < BaseUrl.likedEventList.size(); k++) {
						HashMap<String, String> hm = BaseUrl.likedEventList
								.get(k);
						if (hm.get("id").equalsIgnoreCase(
								arrayElement.getString("id")))
							isFound = true;
					}
					if (!isFound)
						BaseUrl.likedEventList.add(hash);
				}
				if (arrayElement.getString("user_registered").equalsIgnoreCase(
						"1")) {
					BaseUrl.registeredEventId.add(arrayElement.getString("id"));
					boolean isFound = false;
					for (int k = 0; k < BaseUrl.registeredEventList.size(); k++) {
						HashMap<String, String> hm = BaseUrl.registeredEventList
								.get(k);
						if (hm.get("id").equalsIgnoreCase(
								arrayElement.getString("id")))
							isFound = true;
					}
					if (!isFound)
						BaseUrl.registeredEventList.add(hash);
				}
				arrayToAdd.add(hash);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (BaseUrl.TAB2_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB2_STACK.size(); i++) {
				Activity act = BaseUrl.TAB2_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB1_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB1_STACK.size(); i++) {
				Activity act = BaseUrl.TAB1_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB3_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB3_STACK.size(); i++) {
				Activity act = BaseUrl.TAB3_STACK.get(i);
				act.finish();
			}
		}
		if (BaseUrl.TAB4_STACK.size() > 0) {
			for (int i = 0; i < BaseUrl.TAB4_STACK.size(); i++) {
				Activity act = BaseUrl.TAB4_STACK.get(i);
				act.finish();
			}
		}
		BaseUrl.TAB1_LAST_ACTIVITY = com.grapevine.recommended.Recommended_.class;
		BaseUrl.TAB2_LAST_ACTIVITY = com.grapevine.search.SearchScreen_.class;
		BaseUrl.TAB3_LAST_ACTIVITY = com.grapevine.myevent.MyEvents_.class;
		BaseUrl.TAB4_LAST_ACTIVITY = com.grapevine.myprofile.MyProfile_.class;
		finish();
	}

	public class AsyncGetUserRegisteredEvents extends
			AsyncTask<Void, Void, String> {
		String s, Json_to_parse;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... arg0) {
			InputStream ip;
			try {
				ip = sc.getHttpStream(BaseUrl.Development_Uri + "/users/"
						+ bu.USER_KEY + bu.getMethodName("EVENTS") + bu.Api_key
						+ "&type=registered&sort=start_date");
				Json_to_parse = sc.convertStreamToString(ip);
				parseJsonforRegisteredEvents(Json_to_parse);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return Json_to_parse;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result != null) {

			} else {
				Toast.makeText(Recommended_.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void parseJsonforRegisteredEvents(String jsonString) {
		try {
			JSONObject recommendedJsonObject = new JSONObject(jsonString);
			JSONArray arrayRegistered = recommendedJsonObject
					.getJSONArray("registered");
			if (arrayRegistered.length() > 0)
				PutRegisteredEventinHashmap(BaseUrl.registeredEventList,
						arrayRegistered);

		} catch (JSONException e) {
		} catch (Exception e) {
		}
	}

	public void PutRegisteredEventinHashmap(
			ArrayList<HashMap<String, String>> arrayToAdd, JSONArray jsonarray) {

		for (int i = 0; i < jsonarray.length(); i++) {
			try {
				JSONObject arrayElement = jsonarray.getJSONObject(i);
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("address_line_1",
						arrayElement.getString("address_line_1"));
				hash.put("city", arrayElement.getString("city"));
				hash.put("description", arrayElement.getString("description"));
				hash.put("distance", arrayElement.getString("distance"));
				hash.put("end_time", arrayElement.getString("end_time"));
				hash.put("start_time", arrayElement.getString("start_time"));
				hash.put("event_url", arrayElement.getString("event_url"));
				hash.put("public_url", arrayElement.getString("public_url"));
				hash.put("id", arrayElement.getString("id"));
				hash.put("image_url", arrayElement.getString("image_url"));
				hash.put("latitude", arrayElement.getString("latitude"));
				hash.put("location", arrayElement.getString("location"));
				hash.put("longitude", arrayElement.getString("longitude"));
				hash.put("postalcode", arrayElement.getString("postalcode"));
				hash.put("registration_deadline",
						arrayElement.getString("registration_deadline"));
				hash.put("registration_url",
						arrayElement.getString("registration_url"));
				hash.put("state", arrayElement.getString("state"));
				hash.put("teaser", arrayElement.getString("teaser"));
				hash.put("title", arrayElement.getString("title"));
				hash.put("user_feedback",
						arrayElement.getString("user_feedback"));
				hash.put("user_like", arrayElement.getString("user_like"));
				hash.put("user_not_like",
						arrayElement.getString("user_not_like"));
				hash.put("user_registered",
						arrayElement.getString("user_registered"));
				hash.put("user_score", arrayElement.getString("user_score"));
				JSONObject orgnazationObj = arrayElement
						.getJSONObject("organization");
				hash.put("orgnization", orgnazationObj.getString("name"));
				hash.put("orgnizationid", orgnazationObj.getString("id"));
				arrayToAdd.add(hash);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		if (!COMMUNITY_ID.equalsIgnoreCase(bu.CommunityID)) {
			COMMUNITY_ID = bu.CommunityID;
			featuredList = new ArrayList<HashMap<String, String>>();
			myRecommendationList = new ArrayList<HashMap<String, String>>();
			new AsynchRecommendList().execute();
		}
	}
}

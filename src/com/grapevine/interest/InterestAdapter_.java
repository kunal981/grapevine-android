package com.grapevine.interest;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.cuelogic.grapevine.R;
import com.grapevine.util.FontUtil;
import com.socketconnection.BaseUrl;

public class InterestAdapter_ extends BaseAdapter {

	Context context;
	ViewHolder holder;
	private LayoutInflater mInflater;
	String listTitle;
	ArrayList<HashMap<String, String>> List = new ArrayList<HashMap<String, String>>();
	public static ArrayList<String> selected_INTEREST;
	public static ArrayList<String> selected_COMMUNITIES;

	public static HashMap<String, Boolean> selected_chkbox_interest;
	public static HashMap<String, Boolean> selected_chkbox_communities;

	public InterestAdapter_(Context context,
			ArrayList<HashMap<String, String>> homeList, String title) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
		this.List = homeList;
		this.listTitle = title;
		selected_chkbox_interest = new HashMap<String, Boolean>();
		selected_chkbox_communities = new HashMap<String, Boolean>();
		selected_INTEREST = new ArrayList<String>();
		selected_COMMUNITIES = new ArrayList<String>();
		System.out.println("The previous selected Interest are "
				+ BaseUrl.checkedINTEREST);
		System.out.println("The previous selected Communities are "
				+ BaseUrl.checkedCOMMUNITIES);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return List.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return List.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// if (convertView == null)
		// {
		convertView = mInflater.inflate(
				com.cuelogic.grapevine.R.layout.interest_communities_adapter_,
				null);
		holder = new ViewHolder();
		holder.cb_item = (CheckBox) convertView.findViewById(R.id.check);
		holder.cb_item.setTypeface(FontUtil.getTypeface(context,
				"PtSansWebRegular.TTF"));
		holder.tv = (TextView) convertView.findViewById(R.id.itemID);
		convertView.setTag(holder);
		// }
		// else
		// holder = (ViewHolder) convertView.getTag();

		holder.cb_item.setText(List.get(position).get("Value"));
		holder.tv.setText(List.get(position).get("ID"));

		if (listTitle.contains("INTERESTS")) {
			selected_chkbox_interest.put(List.get(position).get("ID"), false);
			// if(selected_INTEREST.contains(List.get(position).get("ID"))){
			// selected_chkbox_interest.put(List.get(position).get("ID"), true);
			// holder.cb_item.setChecked(true);
			// }
			if (BaseUrl.checkedINTEREST != null)
				if (BaseUrl.checkedINTEREST.contains(List.get(position).get(
						"ID"))) {
					selected_chkbox_interest.put(List.get(position).get("ID"),
							true);
					holder.cb_item.setChecked(true);
				}
		}
		if (listTitle.contains("COMMUNITIES")) {
			selected_chkbox_communities
					.put(List.get(position).get("ID"), false);
			// if(selected_COMMUNITIES.contains(List.get(position).get("ID"))){
			// selected_chkbox_communities.put(List.get(position).get("ID"),
			// true);
			// holder.cb_item.setChecked(true);
			// }
			if (BaseUrl.checkedCOMMUNITIES != null)
				if (BaseUrl.checkedCOMMUNITIES.contains(List.get(position).get(
						"ID"))) {
					selected_chkbox_communities.put(List.get(position)
							.get("ID"), true);
					holder.cb_item.setChecked(true);
				}
		}

		holder.cb_item
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (listTitle.contains("INTERESTS")) {
							if (isChecked) {
								selected_chkbox_interest.remove(List.get(
										position).get("ID"));
								selected_chkbox_interest.put(List.get(position)
										.get("ID"), true);
								// selected_INTEREST.add(List.get(position).get("ID"));
								BaseUrl.checkedINTEREST.add(List.get(position)
										.get("ID"));
							} else {
								selected_chkbox_interest.remove(List.get(
										position).get("ID"));
								selected_chkbox_interest.put(List.get(position)
										.get("ID"), false);
								// selected_INTEREST.remove(List.get(position).get("ID"));
								BaseUrl.checkedINTEREST.remove(List.get(
										position).get("ID"));
							}
						}
						if (listTitle.contains("COMMUNITIES")) {
							if (isChecked) {
								selected_chkbox_communities.remove(List.get(
										position).get("ID"));
								selected_chkbox_communities.put(
										List.get(position).get("ID"), true);
								// selected_COMMUNITIES.add(List.get(position).get("ID"));
								BaseUrl.checkedCOMMUNITIES.add(List.get(
										position).get("ID"));
							} else {
								selected_chkbox_communities.remove(List.get(
										position).get("ID"));
								selected_chkbox_communities.put(
										List.get(position).get("ID"), false);
								// selected_COMMUNITIES.remove(List.get(position).get("ID"));
								BaseUrl.checkedCOMMUNITIES.remove(List.get(
										position).get("ID"));
							}
						}

					}
				});

		return convertView;
	}

	class ViewHolder {
		CheckBox cb_item;
		TextView tv;
	}
}

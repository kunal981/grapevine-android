/*
 * Copyright 2010 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cuelogic.grapevine.R;
import com.facebook.android.Facebook.DialogListener;


// TODO: Auto-generated Javadoc
/**
 * The Class FbDialog.
 */
public class FbDialog extends Dialog {

    /** The Constant FB_BLUE. */
    static final int FB_BLUE = 0xFF6D84B4;
    
    /** The Constant DIMENSIONS_LANDSCAPE. */
    static final float[] DIMENSIONS_LANDSCAPE = {460, 260};
    
    /** The Constant DIMENSIONS_PORTRAIT. */
    static final float[] DIMENSIONS_PORTRAIT = {280, 420};
    
    /** The Constant FILL. */
    static final LayoutParams FILL = 
        new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 
                         ViewGroup.LayoutParams.FILL_PARENT);
    
    /** The Constant MARGIN. */
    static final int MARGIN = 4;
    
    /** The Constant PADDING. */
    static final int PADDING = 2;
    
    /** The Constant DISPLAY_STRING. */
    static final String DISPLAY_STRING = "display=touch";
    
    /** The Constant FB_ICON. */
    static final String FB_ICON = "icon.png";
    
    /** The m url. */
    private String mUrl;
    
    /** The m listener. */
    private DialogListener mListener;
    
    /** The m spinner. */
    private ProgressDialog mSpinner;
    
    /** The m web view. */
    private WebView mWebView;
    
    /** The m content. */
    private LinearLayout mContent;
    
    /** The m title. */
    private TextView mTitle;
    
    /** The tag. */
    String TAG = "FbDialog";
    
    /** The _context. */
    private Context _context;
   
    /**
     * Instantiates a new fb dialog.
     *
     * @param context the context
     * @param url the url
     * @param listener the listener
     */
    public FbDialog(Context context, String url, DialogListener listener) {
        super(context);
        _context = context;
        mUrl = url;
        mListener = listener;
    }

    /* (non-Javadoc)
     * @see android.app.Dialog#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSpinner = new ProgressDialog(_context);
        mSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mSpinner.setMessage("Loading...");
        
        mContent = new LinearLayout(getContext());
        mContent.setOrientation(LinearLayout.VERTICAL);
        setUpTitle();
        setUpWebView();
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        final float scale = getContext().getResources().getDisplayMetrics().density;
        float[] dimensions =
            (display.getWidth() < display.getHeight())
                    ? DIMENSIONS_PORTRAIT : DIMENSIONS_LANDSCAPE;
        addContentView(mContent, new FrameLayout.LayoutParams(
                (int) (dimensions[0] * scale + 0.5f),
                (int) (dimensions[1] * scale + 0.5f)));
    }

 
    /**
     * Sets the up title.
     */
    private void setUpTitle() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Drawable icon = getContext().getResources().getDrawable(R.drawable.ic_facebook);
        mTitle = new TextView(getContext());
        mTitle.setText("Facebook");
        mTitle.setTextColor(Color.WHITE);
        mTitle.setTypeface(Typeface.DEFAULT_BOLD);
        mTitle.setBackgroundColor(FB_BLUE);
        mTitle.setPadding(MARGIN + PADDING, MARGIN, MARGIN, MARGIN);
        mTitle.setCompoundDrawablePadding(MARGIN + PADDING);
        mTitle.setCompoundDrawablesWithIntrinsicBounds(
                icon, null, null, null);
        mContent.addView(mTitle);
    }
    
    /**
     * Sets the up web view.
     */
    private void setUpWebView() {
        mWebView = new WebView(getContext());
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(new FbDialog.FbWebViewClient());
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(mUrl);
        mWebView.setLayoutParams(FILL);
        mContent.addView(mWebView);
    }

    /* (non-Javadoc)
     * @see android.app.Dialog#onBackPressed()
     */
    @Override
    public void onBackPressed() {
    	
    	super.onBackPressed();
    //	onegift.isShareOptionOpen = 0;
    }
    
    /**
     * The Class FbWebViewClient.
     */
    private class FbWebViewClient extends WebViewClient {

        /* (non-Javadoc)
         * @see android.webkit.WebViewClient#shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String)
         */
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            
        	Log.d("Facebook-WebView", "Redirect URL: " + url);
           
        	if (url.startsWith(Facebook.REDIRECT_URI)) {
            	
            	//****url at the time of login
            	//shouldOverrideUrlLoading Url:---fbconnect://success#access_token=107941852557749%7Cd5049ab0a38dd45929ee54a3.3-100000729968985%7CEyWuCAeGzOtCwPqEV-iiNLSg1bg&expires_in=0
            	//*****This is when wall post successfully
            	//shouldOverrideUrlLoading Url:---fbconnect://success?post_id=100001344896401_143510419060143

            	Log.d(TAG, "shouldOverrideUrlLoading Url:---"+url);
            	CharSequence cs = "access_token";
                
            	//CharSequence cs1 = "post_id";
            	
            	int index,nindex;
            	Bundle values = null ;
            	
            	if(url.contains(cs)){
            		 
            		index = url.indexOf("access_token")+"access_token".length()+1;
                	nindex = url.indexOf("&");
                	String token = url.substring(index, nindex);
                	values = new Bundle();
                	values.putString("access_token", token);
            	}
            	
            	else{
            		//This is must if already Loged in
            		
            		  values = Util.parseUrl(url);
            	}
            	
                String error = values.getString("error_reason");
                if (error == null) {
                    mListener.onComplete(values);
                } else {
                    mListener.onFacebookError(new FacebookError(error));
                }
                FbDialog.this.dismiss();
                return true;
            } else if (url.startsWith(Facebook.CANCEL_URI)) {
                mListener.onCancel();
                FbDialog.this.dismiss();
                return true;
            } else if (url.contains(DISPLAY_STRING)) {
                return false;
            }
            // launch non-dialog URLs in a full browser
            //getContext().startActivity(
              //      new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            mWebView.loadUrl(url);
            return true;
        }

     
        
        /* (non-Javadoc)
         * @see android.webkit.WebViewClient#onReceivedError(android.webkit.WebView, int, java.lang.String, java.lang.String)
         */
        @Override
        public void onReceivedError(WebView view, int errorCode,
                String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            mListener.onError(
                    new DialogError(description, errorCode, failingUrl));
            FbDialog.this.dismiss();
         //   onegift.isShareOptionOpen = 0;
        }

        /* (non-Javadoc)
         * @see android.webkit.WebViewClient#onPageStarted(android.webkit.WebView, java.lang.String, android.graphics.Bitmap)
         */
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.d("Facebook-WebView", "Webview loading URL: " + url);
            super.onPageStarted(view, url, favicon);
            mSpinner.show();
        }

        /* (non-Javadoc)
         * @see android.webkit.WebViewClient#onPageFinished(android.webkit.WebView, java.lang.String)
         */
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            String title = mWebView.getTitle();
            if (title != null && title.length() > 0) {
                mTitle.setText(title);
            }
            mSpinner.dismiss();
        }   
        
    }
    
}

package com.grapevine.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;

import com.cuelogic.grapevine.R;
import com.grapevine.util.FontUtil;

public class MyButton extends Button {

	private final static int PT_SANS_WEB_BOLD = 1;
	private final static int PT_SANS_WEB_REGULAR = 0;

	public MyButton(Context context) {
		super(context);
		if (isInEditMode()) {
			return;
		}
	}

	public MyButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (isInEditMode()) {
			return;
		}
		parseAttributes(context, attrs);
	}

	public MyButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		if (isInEditMode()) {
			return;
		}
		parseAttributes(context, attrs);
	}

	private void parseAttributes(Context context, AttributeSet attrs) {
		TypedArray values = context.obtainStyledAttributes(attrs,
				R.styleable.CustomFont);

		// The value 0 is a default, but shouldn't ever be used since the attr
		// is an enum
		int typeface = values.getInt(R.styleable.CustomFont_typeface, 0);

		switch (typeface) {
		case PT_SANS_WEB_BOLD:
			setTypeface(FontUtil.getTypeface(context, "PtSansWebBold.TTF"));
			break;
		case PT_SANS_WEB_REGULAR:
			setTypeface(FontUtil.getTypeface(context, "PtSansWebRegular.TTF"));
			break;
		}

		values.recycle();
	}

}

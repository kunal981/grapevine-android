package com.cuelogic.grapevine;

/* A splash screen.
 * @author Cuelogic Technologies
 */

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cuelogic.grapevine.util.AppConstant.Config;
import com.grapevine.recommended.Recommended_;
import com.grapevine.ui.SponserImageView;
import com.grapevine.util.DesignUtil;
import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

public class SplashScreen extends Activity {

	/** The handler. */
	// private Handler handler = new Handler();
	// private ProgressBar pb;
	BaseUrl bu;
	SocketConnection sc;
	private RelativeLayout sponserImage;
	private SponserImageView imageView;
	SharedPreferences preferences;
	int sponserId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.splashscreen_);
		preferences = getSharedPreferences("GrapevineData",
				Context.MODE_PRIVATE);

		bu = new BaseUrl();
		sc = new SocketConnection(this);

		DesignUtil.showProgressDialog(SplashScreen.this);
		LocationChangeHandler.setInstance(new LocationChangeHandler(
				getApplicationContext()));
		LocationChangeHandler.getInstance().beginUpdate();
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				new AsynchTogetLocation().execute();

			}
		}, 1000);
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStop() {
		// handler.removeCallbacks(waitThread);
		super.onStop();
	}

	public class AsynchTogetLocation extends AsyncTask<Void, Void, String> {
		String lat, lon;

		@Override
		protected String doInBackground(Void... params) {
			if (LocationChangeHandler.getLocation() != null) {
				lat = LocationChangeHandler.getLocation().getLatitude() + "";
				lon = LocationChangeHandler.getLocation().getLongitude() + "";
			} else {
				lat = "35.74184050952353";
				lon = "-118.8128372137487";
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// if (Double.parseDouble(lon) > -74.3
			// && Double.parseDouble(lon) < -73.7
			// && Double.parseDouble(lat) > 40.5
			// && Double.parseDouble(lat) < 40.8)
			// sponserImage.setVisibility(View.VISIBLE);
			// else
			// sponserImage.setVisibility(View.GONE);
			// Checks in the shared preference whether the user is already
			// log-in or not.

			String userkey = preferences.getString("USER_KEY", "");
			if (userkey.equalsIgnoreCase("")) {
				DesignUtil.hideProgressDialog();
				Intent i = new Intent(SplashScreen.this, LoginRegister_.class);
				startActivity(i);
				finish();
			} else {
				bu.USER_KEY = userkey;
				new AsynchGetUserProfile().execute();
			}
			// Intent i = new Intent(SplashScreen.this, LoginRegister_.class);
			// startActivity(i);
			// finish();
		}

	}

	public class AsynchGetUserProfile extends AsyncTask<Void, Void, String> {

		String s, Json_to_parse, lat, lon;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				if (LocationChangeHandler.getLocation() != null) {
					lat = LocationChangeHandler.getLocation().getLatitude()
							+ "";
					lon = LocationChangeHandler.getLocation().getLongitude()
							+ "";
				} else {
					lat = "35.74184050952353";
					lon = "-118.8128372137487";
				}
				InputStream ip = sc.getHttpStream(BaseUrl.Development_Uri
						+ bu.getMethodName("LOGIN") + "/" + bu.USER_KEY
						+ bu.Api_key + "&lat=" + lat + "&lon=" + lon);
				// Json_to_parse = sc.getResponse(BaseUrl.Development_Uri
				// + bu.getMethodName("LOGIN") + "/" + bu.USER_KEY
				// + bu.Api_key + "&lat=" + lat + "&lon=" + lon);
				Json_to_parse = sc.convertStreamToString(ip);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return Json_to_parse;
		}

		@SuppressWarnings("static-access")
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			DesignUtil.hideProgressDialog();
			LocationChangeHandler.getInstance().stopUpdates();

			if (Json_to_parse != null) {
				try {
					JSONObject userProfileRespone = new JSONObject(result);
					bu.UserInfo = userProfileRespone;
					JSONArray bookmarkedEventArray = userProfileRespone
							.getJSONArray("event_bookmarks");
					if (bookmarkedEventArray.length() > 0) {
						for (int i = 0; i < bookmarkedEventArray.length(); i++) {
							BaseUrl.bookmarkedEventId.add(bookmarkedEventArray
									.getString(i));
						}
					}
					// Put all the Communities,Neighborhoods and Selected
					// community Location at one place.
					bu.parentCommunityArray = new JSONArray();
					bu.parentCommunityArray = userProfileRespone
							.getJSONArray("communities_all");
					bu.neighborhoodCommunityAllArray = new JSONArray();
					bu.neighborhoodCommunityAllArray = userProfileRespone
							.getJSONArray("neighborhoods_all");
					bu.CommunityID = userProfileRespone
							.getString("community_id");
					bu.AllInterestArray = new JSONArray();
					bu.AllInterestArray = userProfileRespone
							.getJSONArray("interests_all");
					bu.AllOrganizationArray = new JSONArray();
					bu.AllOrganizationArray = userProfileRespone
							.getJSONArray("organizations_all");
					bu.AllCommunitiesArray = new JSONArray();
					bu.AllCommunitiesArray = userProfileRespone
							.getJSONArray("target_communities_all");
					// This will take all previously selected Interest and
					// communities
					bu.checkedINTEREST = new ArrayList<String>();
					JSONArray userSelectedInterest = userProfileRespone
							.getJSONArray("interests");
					if (userSelectedInterest.length() > 0) {
						for (int i = 0; i < userSelectedInterest.length(); i++) {
							bu.checkedINTEREST.add(userSelectedInterest
									.getString(i));
						}
					}
					bu.checkedCOMMUNITIES = new ArrayList<String>();
					JSONArray userSelectedCommunities = userProfileRespone
							.getJSONArray("target_communities");
					if (userSelectedCommunities.length() > 0) {
						for (int i = 0; i < userSelectedCommunities.length(); i++) {
							bu.checkedCOMMUNITIES.add(userSelectedCommunities
									.getString(i));
						}
					}
					sponserId = Integer.valueOf(bu.CommunityID);
					preferences.edit()
							.putInt(Config.KEY_PREF_SPONSER_ID, sponserId)
							.commit();
					Intent i = new Intent(getApplicationContext(),
							Recommended_.class);
					startActivity(i);
					overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
					finish();

				} catch (JSONException e) {
					e.printStackTrace();

					Intent i = new Intent(SplashScreen.this,
							LoginRegister_.class);
					startActivity(i);
					finish();

				}
			} else {
				Toast.makeText(SplashScreen.this,
						"Please check your Internet Connection",
						Toast.LENGTH_SHORT).show();
				// finish();
				Intent i = new Intent(SplashScreen.this, LoginRegister_.class);
				startActivity(i);
				finish();
				// new AsynchGetUserProfile().execute();
			}
			System.out.println("" + result);
		}

	}

}
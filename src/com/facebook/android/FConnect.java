/*
 * Copyright 2010 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.android;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.cuelogic.grapevine.R;
import com.facebook.android.SessionEvents.AuthListener;
import com.facebook.android.SessionEvents.LogoutListener;

// TODO: Auto-generated Javadoc
/**
 * The Class FConnect.
 */
public class FConnect extends Activity {

	// Your Facebook Application ID must be set before running this example
	// See http://www.facebook.com/developers/createapp.php
	/** The Constant APP_ID. */
	public static final String APP_ID = "132127003621561";// Grape-365698503496748,cell-132127003621561

	/** The Constant PERMISSIONS. */
	@SuppressWarnings("unused")
	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "read_stream", "offline_access" };

	/** The m text. */
	private TextView mText;

	/** The m request button. */
	private Button mRequestButton;

	/** The m post button. */
	private Button mPostButton;

	/** The m delete button. */
	private Button mDeleteButton;

	/** The m facebook. */
	private Facebook mFacebook;

	/** The m async runner. */
	private AsyncFacebookRunner mAsyncRunner;

	/**
	 * Called when the activity is first created.
	 *
	 * @param savedInstanceState
	 *            the saved instance state
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (APP_ID == null) {
			Util.showAlert(this, "Warning", "Facebook Applicaton ID must be "
					+ "specified before running this example: see Example.java");
		}

		setContentView(R.layout.fconnect);

		mText = (TextView) FConnect.this.findViewById(R.id.txt);
		// mRequestButton = (Button) findViewById(R.id.requestButton);
		// mPostButton = (Button) findViewById(R.id.postButton);
		// mDeleteButton = (Button) findViewById(R.id.deletePostButton);

		mFacebook = new Facebook();
		mAsyncRunner = new AsyncFacebookRunner(mFacebook);

		SessionStore.restore(mFacebook, this);
		SessionEvents.addAuthListener(new SampleAuthListener());
		SessionEvents.addLogoutListener(new SampleLogoutListener());

		mRequestButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mAsyncRunner.request("me", new SampleRequestListener());
				mAsyncRunner
						.request("me/friends", new FriendsRequestListener());
			}
		});
		mRequestButton.setVisibility(mFacebook.isSessionValid() ? View.VISIBLE
				: View.INVISIBLE);

		mPostButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mFacebook.dialog(FConnect.this, "stream.publish",
						new SampleDialogListener());
			}
		});
		mPostButton.setVisibility(mFacebook.isSessionValid() ? View.VISIBLE
				: View.INVISIBLE);
	}

	public void SendPostonWall(Context ctx) {
		mFacebook = new Facebook();
		mAsyncRunner = new AsyncFacebookRunner(mFacebook);

		mFacebook.dialog(getApplicationContext(), "stream.publish",
				new SampleDialogListener());
	}

	/**
	 * The listener interface for receiving sampleAuth events. The class that is
	 * interested in processing a sampleAuth event implements this interface,
	 * and the object created with that class is registered with a component
	 * using the component's <code>addSampleAuthListener<code> method. When
	 * the sampleAuth event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SampleAuthEvent
	 */
	public class SampleAuthListener implements AuthListener {

		/* (non-Javadoc)
		 * @see com.facebook.android.SessionEvents.AuthListener#onAuthSucceed()
		 */
		public void onAuthSucceed() {
			mText.setText("You have logged in! ");
			mRequestButton.setVisibility(View.VISIBLE);
			mPostButton.setVisibility(View.VISIBLE);
		}

		/* (non-Javadoc)
		 * @see com.facebook.android.SessionEvents.AuthListener#onAuthFail(java.lang.String)
		 */
		public void onAuthFail(String error) {
			mText.setText("Login Failed: " + error);
		}
	}

	/**
	 * The listener interface for receiving sampleLogout events. The class that
	 * is interested in processing a sampleLogout event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addSampleLogoutListener<code> method. When
	 * the sampleLogout event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SampleLogoutEvent
	 */
	public class SampleLogoutListener implements LogoutListener {

		/* (non-Javadoc)
		 * @see com.facebook.android.SessionEvents.LogoutListener#onLogoutBegin()
		 */
		public void onLogoutBegin() {
			// mText.setText("Logging out...");
		}

		/* (non-Javadoc)
		 * @see com.facebook.android.SessionEvents.LogoutListener#onLogoutFinish()
		 */
		public void onLogoutFinish() {
			mText.setText("You have logged out! ");
			mRequestButton.setVisibility(View.INVISIBLE);
			mPostButton.setVisibility(View.INVISIBLE);
		}
	}

	/**
	 * The listener interface for receiving friendsRequest events. The class
	 * that is interested in processing a friendsRequest event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addFriendsRequestListener<code> method. When
	 * the friendsRequest event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see FriendsRequestEvent
	 */
	public class FriendsRequestListener extends BaseRequestListener {

		/* (non-Javadoc)
		 * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onComplete(java.lang.String)
		 */
		@Override
		public void onComplete(String response) {

			try {
				JSONObject json = Util.parseJson(response);
				JSONArray ja = json.getJSONArray("data");
				String friends = "";
				for (int i = 0; i < ja.length(); ++i)
					friends = friends + ja.getJSONObject(i).getString("name")
							+ ",";
				friends = friends.substring(0, friends.length() - 1);
				// Intent flist = new Intent(FConnect.this,
				// InviteFriends.class);
				// flist.putExtra("friends", friends);
				// startActivity(flist);
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}
		}
	}

	/**
	 * The listener interface for receiving sampleRequest events. The class that
	 * is interested in processing a sampleRequest event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addSampleRequestListener<code> method. When
	 * the sampleRequest event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SampleRequestEvent
	 */
	public class SampleRequestListener extends BaseRequestListener {

		/* (non-Javadoc)
		 * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onComplete(java.lang.String)
		 */
		public void onComplete(final String response) {
			try {
				// process the response here: executed in background thread
				Log.d("Facebook-Example", "Response: " + response.toString());
				JSONObject json = Util.parseJson(response);
				final String name = json.getString("name");

				// then post the processed result back to the UI thread
				// if we do not do this, an runtime exception will be generated
				// e.g. "CalledFromWrongThreadException: Only the original
				// thread that created a view hierarchy can touch its views."
				FConnect.this.runOnUiThread(new Runnable() {
					public void run() {
						mText.setText("Hello there, " + name + "!");
					}
				});
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}
		}
	}

	/**
	 * The listener interface for receiving wallPostRequest events. The class
	 * that is interested in processing a wallPostRequest event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addWallPostRequestListener<code> method. When
	 * the wallPostRequest event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see WallPostRequestEvent
	 */
	public class WallPostRequestListener extends BaseRequestListener {

		/* (non-Javadoc)
		 * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onComplete(java.lang.String)
		 */
		public void onComplete(final String response) {
			Log.d("Facebook-Example", "Got response: " + response);
			String message = "<empty>";
			try {
				JSONObject json = Util.parseJson(response);
				message = json.getString("message");
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}
			final String text = "Your Wall Post: " + message;
			FConnect.this.runOnUiThread(new Runnable() {
				public void run() {
					mText.setText(text);
				}
			});
		}
	}

	/**
	 * The listener interface for receiving wallPostDelete events. The class
	 * that is interested in processing a wallPostDelete event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addWallPostDeleteListener<code> method. When
	 * the wallPostDelete event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see WallPostDeleteEvent
	 */
	public class WallPostDeleteListener extends BaseRequestListener {

		/* (non-Javadoc)
		 * @see com.facebook.android.AsyncFacebookRunner.RequestListener#onComplete(java.lang.String)
		 */
		public void onComplete(final String response) {
			if (response.equals("true")) {
				Log.d("Facebook-Example", "Successfully deleted wall post");
				FConnect.this.runOnUiThread(new Runnable() {
					public void run() {
						mDeleteButton.setVisibility(View.INVISIBLE);
						mText.setText("Deleted Wall Post");
					}
				});
			} else {
				Log.d("Facebook-Example", "Could not delete wall post");
			}
		}
	}

	/**
	 * The listener interface for receiving sampleDialog events. The class that
	 * is interested in processing a sampleDialog event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>addSampleDialogListener<code> method. When
	 * the sampleDialog event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SampleDialogEvent
	 */
	public class SampleDialogListener extends BaseDialogListener {

		/* (non-Javadoc)
		 * @see com.facebook.android.Facebook.DialogListener#onComplete(android.os.Bundle)
		 */
		public void onComplete(Bundle values) {
			final String postId = values.getString("post_id");
			if (postId != null) {
				Log.d("Facebook-Example", "Dialog Success! post_id=" + postId);
				mAsyncRunner.request(postId, new WallPostRequestListener());
				mDeleteButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						mAsyncRunner.request(postId, new Bundle(), "DELETE",
								new WallPostDeleteListener());
					}
				});
				mDeleteButton.setVisibility(View.VISIBLE);
			} else {
				Log.d("Facebook-Example", "No wall post made");
			}
		}

	}
}
package com.cuelogic.grapevine;

import com.grapevine.myevent.MyEvents_;
import com.grapevine.myprofile.MyProfile_;
import com.grapevine.recommended.Recommended_;
import com.grapevine.search.SearchScreen_;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HomeScreen extends Activity implements OnClickListener {

	Button btn_settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homescreen);

		findViewById(R.id.eventsLayout).setOnClickListener(this);
		findViewById(R.id.searchEventsLayout).setOnClickListener(this);
		findViewById(R.id.myEventsLayout).setOnClickListener(this);
		findViewById(R.id.myProfileLayout).setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.eventsLayout:
			Intent i = new Intent(this, Recommended_.class);
			startActivity(i);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
			break;
		case R.id.searchEventsLayout:
			Intent iSearch = new Intent(this, SearchScreen_.class);
			startActivity(iSearch);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
			break;
		case R.id.myEventsLayout:
			Intent iMyevent = new Intent(this, MyEvents_.class);
			startActivity(iMyevent);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
			break;
		case R.id.myProfileLayout:
			Intent iMyprofile = new Intent(this, MyProfile_.class);
			startActivity(iMyprofile);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
			break;

		default:
			break;
		}

	}

}

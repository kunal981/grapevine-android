package com.grapevine.recommended;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.socketconnection.BaseUrl;
import com.socketconnection.SocketConnection;

/*
 * A serivce is called to update events Action.
 */
public class AsynchUpdateAction extends AsyncTask<Void, Void, String>{
	String eventID,action;
	SocketConnection sc;
	BaseUrl bu;
	Context context;
	public AsynchUpdateAction(String eventID,String action,Context context) {
		// TODO Auto-generated constructor stub
		this.eventID = eventID;
		this.action = action;
		this.context = context;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		sc= new SocketConnection(this.context);
        bu = new BaseUrl();
	}
	@Override
	protected String doInBackground(Void... params) {
		
		JSONObject eventUpdateJson = new JSONObject();
		try {
			eventUpdateJson.put("action", action);
			eventUpdateJson.put("timestamp", getTimeStamp());
			String s = sc.SendPUTdata(bu.Development_Uri+bu.getMethodName("RECOMMENDED")+bu.USER_KEY+bu.getMethodName("RECOMMENDED_EVENTS")+"/"+eventID+bu.Api_key,eventUpdateJson);
			return s;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch(Exception e){}
		return null;
	}
	@Override
	protected void onPostExecute(String result) {

		super.onPostExecute(result);
		System.out.println("Response is " + result);
		if(result!=null){
			
		}else{
			Toast.makeText(context, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
		}
		
	}
	private String getTimeStamp(){
		Long tsLong = System.currentTimeMillis()/1000;
		return tsLong.toString();
	}
}
